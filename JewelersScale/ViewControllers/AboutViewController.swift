//
//  AboutViewController.swift
//  JewelersScale
//
//  Created by Virendra on 29/12/18.
//  Copyright © 2018 MacOS High. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController,UITextViewDelegate {

    @IBOutlet weak var txtView_Text: UITextView!
    @IBOutlet weak var txtView_privcy: UITextView!
    @IBOutlet weak var textViewNewPrivay: UITextView!
    
    var value = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        if value == "1"{
            txtView_privcy.isHidden = true
            textViewNewPrivay.isHidden = true
            txtView_Text.isHidden = false
            txtView_Text.text = "All the Essential Tools for Jewelry Production Efficiency.  Be one step ahead!\nYou don’t need to be a PRO,\nJewelers’ Scale will save you time and money by these user friendly features:\n• Weight & Casting Cost Calculator\n-Enter the volume of your CAD design to calculate the weight and the approximate cost of your piece in major precious metals (silver, 10K, 14K, 18K, Platinum)\n-Easily transfer your casting cost to “Jeweler’s Ledger”\n• Scrap Gold Calculator\n- Calculate the value of your scrap gold in current market price or in desired price in various gold karats\n- Adjust your own percentage offs for each precious metals and calculate results accordingly\n• Weight Units Converter\n-Convert between weight units\n• Casting Calculations\n-Find out how much your material (wax, silver, 10K, 14K etc.) will weigh in different materials\n-See casting costs/dwt of major precious metals and see total casting cost of desired metal based on current market price\n• Precious Metals Current Market Price\n-See current market price for desired precious metals\n• Tips for Common Setbacks for Designers\n-Avoid possible setbacks with these important designing tips\n• Jeweler’s Ledger\n-Find out the total cost of your piece with this cost itemization form\n-Save and email the generated costing form\n• STL File Viewer & Sharing\n-View your CAD designs in 3D at every angle\n-Find out your designs’ automatically calculated volume\n-With one-click, forward your designs’ volume and calculate your “Weight and Casting Cost”\n-Take a screenshot or record a video\n-Share your design with others via email or messaging platforms\nThis unique app will be your #1 go to app!!!"
        }else if value == "2"{
            textViewNewPrivay.delegate = self
            txtView_privcy.isHidden = true
            textViewNewPrivay.isHidden = false
            txtView_Text.isHidden = true
            
        }else{
            txtView_privcy.isHidden = false
            txtView_privcy.delegate = self
            txtView_Text.isHidden = true
            textViewNewPrivay.isHidden = true
        }
        
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print("url")
        UIApplication.shared.open(URL, options: [:])
        return false
    }
    
    
    @IBAction func Back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
