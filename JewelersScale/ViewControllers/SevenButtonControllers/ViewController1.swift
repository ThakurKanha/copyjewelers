
//  ViewController.swift
//  swiftStlViewer
//  Created by user on 23/07/18.
//  Copyright © 2018 Rajat. All rights reserved.

import UIKit
import GLKit
import SceneKit
import SwiftyDropbox
import Alamofire
import MobileCoreServices
import ReplayKit
import SwiftHSVColorPicker
import Photos
import ARKit
class ViewController1: UIViewController,UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate,RPPreviewViewControllerDelegate {
    
    @IBOutlet var roundviewcolor: roundView!
    @IBOutlet var mySceneView: SCNView!
    @IBOutlet var labelIsRecordingOnOff: UILabel!
    @IBOutlet weak var lbl_valume: UILabel!
    @IBOutlet weak var color_picker: SwiftHSVColorPicker!
    @IBOutlet weak var colorview: UIView!
    @IBOutlet weak var btnBluecolor: UIButton!
    @IBOutlet weak var btnWhitecolor: UIButton!
    @IBOutlet weak var Image_uiview: UIView!
    @IBOutlet weak var share_image: UIImageView!
    @IBOutlet weak var color_view: UIView!
    @IBOutlet var vwSample: UIView!
    @IBOutlet weak var unmuted_view: UIView!
    @IBOutlet weak var muted_view: UIView!
    @IBOutlet weak var muted_image: UIImageView!
    @IBOutlet weak var bllink_view: UIView!
    @IBOutlet weak var Btnimage_cancel: UIButton!
    @IBOutlet weak var Btnshare_image: UIButton!
    @IBOutlet weak var Btnsave_image: UIButton!
    @IBOutlet weak var priview_image: UIImageView!
    @IBOutlet weak var Priview_cancelbtn: UIButton!
    @IBOutlet weak var btn_camra: UIButton!
    @IBOutlet weak var zoom_btn: UIButton!

    
    var Gold = ""
    var Silver = ""
    var Platinum = ""
    var DateTime = ""
    var alpha = 0.0
    var colorSelection:Int = 0
    var intTempValueForSource:Int = 0
    var arrForDataToPassOnTable = NSMutableArray()
    var strNameOfSTLfile = String()
    let recorder = RPScreenRecorder.shared()
    var userdefult = UserDefaults()
    var ambientLightNode = SCNNode()
    var volume : String = "0.00"
    private var isRecording = false
    var buttonString:String!
    var screenshotImage:UIImage!
    var newTempUrlFromiCloud = URL(string:"https://www.apple.com")
    var PickedColor = UIColor()
    var colorTimer: Timer!
    var theFileName:String = ""
    var stltempurl = URL(string: "")
    var spotLight = SCNNode()
    var scene = SCNScene()
    var subscene = SCNScene()
    var userid = ""
    var right_color = NSArray()
    var left_color = NSArray()
    var colorarr = NSArray()
    var colordict = NSDictionary()
    let documentInteractionController = UIDocumentInteractionController()
    var flag_zoom = false
    var frame = CGRect()
    var btn_frame = CGRect()
    var sceneUrl = [URL]()
    var secondNode = SCNNode()
    var signCompleteLogin = UIView()
    var signViewShow = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signViewShow = showViewLogIn()
        signCompleteLogin = showViewLogInComplete()
        signViewShow.isHidden = true
        signCompleteLogin.isHidden = true
      
        colorUpdate()
        if let res = UserDefaults.standard.value(forKey: "result"){
            userid = (res as AnyObject).value(forKey: "user_id") as! String
        }
        documentInteractionController.delegate = self
        priview_image.isHidden = true
        Priview_cancelbtn.isHidden = true
        Btnimage_cancel.layer.cornerRadius = Btnimage_cancel.frame.height/2
        Priview_cancelbtn.layer.cornerRadius = Priview_cancelbtn.frame.height/2
        Btnimage_cancel.layer.masksToBounds = true
        self.isRecording = false
        self.CallWebService()
        bllink_view.isHidden = true
        Image_uiview.isHidden = true
        colorview.isHidden = true
        self.labelIsRecordingOnOff.isHidden = true
        color_picker.setViewColor(UIColor.blue)
        btnBluecolor.layer.cornerRadius = 8
        btnBluecolor.layer.masksToBounds = true
        btnWhitecolor.layer.cornerRadius = 8
        btnWhitecolor.layer.masksToBounds = true
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        btnBluecolor.addGestureRecognizer(longGesture)
        let longGesture1 = UILongPressGestureRecognizer(target: self, action: #selector(longTap1(_:)))
        btnWhitecolor.addGestureRecognizer(longGesture1)
        comman().remove(key: "selection_nil")
        spotLight.light = SCNLight()
        spotLight.scale = SCNVector3(1,1,1)
        spotLight.light?.intensity = 800
        spotLight.castsShadow = true
        spotLight.position = SCNVector3Zero
        spotLight.light?.type = SCNLight.LightType.directional
        spotLight.light?.color = UIColor.white
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        print("didap")
        if let email = UserDefaults.standard.string(forKey: "login"){
            print("email:\(email)")
        }else{
            alartviewShow(str: "Valued Jewelers'Scale User,\n With our new updates, registration is now strongly suggested in order to manage user prefrences for STL File Viewer & Sharing")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
        self.showStlViewer()
    }
    func colorUpdate(){
        if let leftcolor = UserDefaults.standard.value(forKey: "left_color") as? NSArray{
            let color = UIColor(displayP3Red: CGFloat((leftcolor[0] as! NSNumber).floatValue/255.0), green: CGFloat((leftcolor[1] as! NSNumber).floatValue/255), blue: CGFloat((leftcolor[2] as! NSNumber).floatValue/255), alpha: 1.0)
            btnBluecolor.backgroundColor = color
        }else{   btnBluecolor.backgroundColor = .blue  }
        if let rightcolor = UserDefaults.standard.value(forKey: "right_color") as? NSArray{
            let color = UIColor(displayP3Red: CGFloat((rightcolor[0] as! NSNumber).floatValue/255), green: CGFloat((rightcolor[1] as! NSNumber).floatValue/255), blue: CGFloat((rightcolor[2] as! NSNumber).floatValue/255), alpha: 1.0)
            btnWhitecolor.backgroundColor = color
        }else{  btnWhitecolor.backgroundColor = .white }
        if let backcolor = UserDefaults.standard.value(forKey: "bg_color") as? NSArray{
            let color = UIColor.init(displayP3Red: backcolor[0] as! CGFloat, green: backcolor[1] as! CGFloat, blue: backcolor[2] as! CGFloat, alpha: 1.0)
            roundviewcolor.backgroundColor = color
        }else{ roundviewcolor.backgroundColor = .white}
    }
    func showStlViewer(){
        if let stlurl:NSArray = UserDefaults.standard.value(forKey: "url1") as? NSArray{
            self.sceneUrl.removeAll()
            DispatchQueue.main.async {
                do {
                    for i in 0..<stlurl.count{
                        print(stlurl.count,"stlurl : : \(stlurl[i])")
                        let data = try Data(contentsOf: URL(string: stlurl[i] as! String)!, options: Data.ReadingOptions())
                        if #available(iOS 10.0, *) {
                            self.stltempurl = FileManager.default.temporaryDirectory
                                .appendingPathComponent("stlfile\(i).stl")
                            do {
                                try data.write(to: self.stltempurl!)
                                UserDefaults.standard.set(self.stltempurl, forKey: "url")
                            } catch { print(error) }
                        }
                        self.sceneUrl.append(self.stltempurl!)
                    }
                    print("self.sceneUrl: == == = = =\(self.sceneUrl)")
                    var strings : [String] = []
                    if let array:[URL] = self.sceneUrl{
                        for url in array {
                            let string = url.absoluteString
                            strings.append(string) ////no null check
                            print("string:\(self.sceneUrl)")
                        }
                    }
                    UserDefaults.standard.set(strings, forKey: "url1")
                    //            self.scene = try SCNScene(url: stlurl, options: nil)
                    self.drawStlOnSceneKit()
                    self.bring()
                }catch let error{
                    print(error.localizedDescription)
                    self.drawStlOnSceneKit()
                }
            }
        }
    }
    
    @objc func longTap(_ sender: UIGestureRecognizer){
        if colorTimer != nil {
            colorTimer.invalidate()
            colorTimer = nil
        }
        color_picker.color = btnBluecolor.backgroundColor
        colorview.isHidden = false
        buttonString = "blue"
        colorTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(colorChanged), userInfo: nil, repeats: true)
    }
    @objc func longTap1(_ sender: UIGestureRecognizer){
        if colorTimer != nil {
            colorTimer.invalidate()
            colorTimer = nil
        }
        color_picker.color = btnWhitecolor.backgroundColor
        colorview.isHidden = false
        buttonString = "white"
        colorTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(colorChanged), userInfo: nil, repeats: true)
    }
    @objc func colorChanged() {
        self.roundviewcolor.backgroundColor = color_picker.color
        self.PickedColor = color_picker.color
        if buttonString == "blue"{
            btnBluecolor.backgroundColor = color_picker.color
            self.PickedColor = color_picker.color
        }else if buttonString == "white"{
            btnWhitecolor.backgroundColor = color_picker.color
            self.PickedColor = color_picker.color
        }
    }
    @IBAction func ColorHide(_ sender: UIButton) {
        if colorTimer != nil {
            colorTimer.invalidate()
            colorTimer = nil
        }
        colorview.isHidden = true
        if buttonString == "blue"{
            if let leftcolor = UserDefaults.standard.value(forKey: "left_color") as? NSArray{
                let color = UIColor(displayP3Red: CGFloat((leftcolor[0] as! NSNumber).floatValue/255), green: CGFloat((leftcolor[1] as! NSNumber).floatValue/255), blue: CGFloat((leftcolor[2] as! NSNumber).floatValue/255), alpha: 1.0)
                btnBluecolor.backgroundColor = color
                roundviewcolor.backgroundColor = color
            }
        }else if buttonString == "white"{
            if let rightcolor = UserDefaults.standard.value(forKey: "right_color") as? NSArray{
                let color = UIColor(displayP3Red: CGFloat((rightcolor[0] as! NSNumber).floatValue/255), green: CGFloat((rightcolor[1] as! NSNumber).floatValue/255), blue: CGFloat((rightcolor[2] as! NSNumber).floatValue/255), alpha: 1.0)
                btnWhitecolor.backgroundColor = color
                roundviewcolor.backgroundColor = color
            }
        }
    }
    @IBAction func Defult_color(_ sender: UIButton) {
        if colorTimer != nil {
            colorTimer.invalidate()
            colorTimer = nil
        }
        colorview.isHidden = true
        if buttonString == "blue"{
            self.PickedColor = UIColor.blue
            btnBluecolor.backgroundColor = self.PickedColor
            self.roundviewcolor.backgroundColor = self.PickedColor
            colorarr = [0,0,255]
            let rightcolor = nikhilsir(color: btnWhitecolor.backgroundColor!)
            colordict = ["left_color":[0,0,255],"right_color":rightcolor, "bg_color":colorarr]
            UserDefaults.standard.set(colorarr, forKey: "left_color")
            UserDefaults.standard.set(colorarr, forKey: "bg_color")
        }else if buttonString == "white"{
            self.PickedColor = UIColor.white
            
            btnWhitecolor.backgroundColor = self.PickedColor
            self.roundviewcolor.backgroundColor = self.PickedColor
            colorarr = [255,255,255]
            let leftcolor = nikhilsir(color: btnBluecolor.backgroundColor!)//
            UserDefaults.standard.set(colorarr, forKey: "right_color")
            UserDefaults.standard.set(colorarr, forKey: "bg_color")
            colordict = ["left_color":leftcolor, "right_color":[255,255,255], "bg_color":colorarr]
        }
        colorsetApi(bacgroundcolor: colordict)
    }
    @IBAction func Pick_color(_ sender: UIButton) {
        colorview.isHidden = true
        if let email = UserDefaults.standard.string(forKey: "login")  {
        if colorTimer != nil {
            colorTimer.invalidate()
            colorTimer = nil
        }
        colorview.isHidden = true
        self.roundviewcolor.backgroundColor = self.PickedColor
        colorarr = [PickedColor.redValue,PickedColor.greenValue,PickedColor.blueValue]
        left_color = nikhilsir(color: btnBluecolor.backgroundColor!)
        right_color = nikhilsir(color: btnWhitecolor.backgroundColor!)
        if buttonString == "blue"{
            btnBluecolor.backgroundColor = self.PickedColor
            UserDefaults.standard.set(left_color, forKey: "left_color")
            UserDefaults.standard.set(colorarr, forKey: "bg_color")
        }else if buttonString == "white"{
            btnWhitecolor.backgroundColor = self.PickedColor
            UserDefaults.standard.set(right_color, forKey: "right_color")
            UserDefaults.standard.set(colorarr, forKey: "bg_color")
        }
        colordict = ["left_color":left_color, "right_color":right_color, "bg_color":colorarr]
        colorsetApi(bacgroundcolor: colordict)
        }
    }
    open func takeScreenshot(_ shouldSave: Bool = true) -> UIImage? {
        CustomPhotoAlbum.albumName = "JewelersScale Images"
        if let stlurl = UserDefaults.standard.string(forKey: "theFileName"){
            theFileName = stlurl.components(separatedBy: ".")[0]
            theFileName.append(".jpg")
        }else{
            theFileName = "JewelersScale.jpg"
        }
        CustomPhotoAlbum.shared.save(image: share_image.image!, theFileName: theFileName)
        let alert = UIAlertController(title: "", message: "Image save successfully" , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
        return screenshotImage
    }
    let screenSize: CGRect = UIScreen.main.bounds
    @IBAction func Zoombtn(_ sender: UIButton) {
        
        if flag_zoom{
            zoom_btn.setImage(UIImage(named: "icon_full_screen_out.png"), for: .normal)
            roundviewcolor.frame = frame
            frame = self.view.convert(roundviewcolor.frame, from:vwSample)
            zoom_btn.frame = btn_frame
        }else{
            frame = self.view.convert(roundviewcolor.frame, from:vwSample)
            let y = frame.minY - 1
            let x = frame.minX
            let wid = frame.width
            let h = frame.height
            frame = CGRect(x: x, y: y, width: wid, height: h)
            zoom_btn.setImage(UIImage(named: "icon_full_screen_in.png"), for: .normal)
            let y1 = zoom_btn.frame.minY - 1
            let x1 = zoom_btn.frame.minX
            let wid1 = zoom_btn.frame.width
            let h1 = zoom_btn.frame.height
            zoom_btn.frame = CGRect(x: x1, y: y1, width: wid1, height: h1)
            btn_frame = self.view.convert(zoom_btn.frame, from:vwSample)
            if UI_USER_INTERFACE_IDIOM() == .pad{
                roundviewcolor.frame = CGRect(x:0, y:0, width:self.roundviewcolor.frame.width, height: UIScreen.main.bounds.maxY - 60)
                zoom_btn.frame = CGRect(x: screenSize.maxX - 50, y: screenSize.maxY - 60 - 50, width: 50, height: 50)
            }else{
                roundviewcolor.frame = CGRect(x:0, y:0, width:self.roundviewcolor.frame.width, height: UIScreen.main.bounds.maxY - 43)
                zoom_btn.frame = CGRect(x: screenSize.maxX - 30, y: screenSize.maxY - 40 - 40, width: 30, height: 30)
            }
        }
        flag_zoom = !flag_zoom
    }
    @IBAction func btnActionCaptureImage(_ sender: Any) {
        share_image.image = view.capture()
        Image_uiview.isHidden = false
    }
    @IBAction func Back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func PickImage_cancel(_ sender: UIButton) {
        Image_uiview.isHidden = true
    }
    @IBAction func PickImage_Save(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.takeScreenshot(true)
        }
    }
    @IBAction func Priview_imageshow(_ sender: UIButton) {
        priview_image.image = share_image.image
        priview_image.isHidden = false
        Priview_cancelbtn.isHidden = false
    }
    @IBAction func Priview_Cancelbtn(_ sender: UIButton) {
        priview_image.isHidden = true
        Priview_cancelbtn.isHidden = true
    }
    @IBAction func PickImage_Share(_ sender: UIButton) {
        var theFileName0 = ""
        let data = share_image.image!.pngData() as NSData?
        if let stlurl = UserDefaults.standard.string(forKey: "theFileName"){
            theFileName0 = stlurl
            theFileName = stlurl.components(separatedBy: ".")[0]
            theFileName.append(".jpg")
        }else{
            theFileName = "JewelersScale.jpg"
        }
        if #available(iOS 10.0, *) {
            let tmpURL = FileManager.default.temporaryDirectory
                .appendingPathComponent("\(theFileName)")
            do {
                try data?.write(to: tmpURL)
            } catch {}
            DispatchQueue.main.async {
                let title = "Please see attached Jewelers' Scale STL File Viewer \(theFileName0) model Share"
                let title1 = "Jewelers' Scale STL File Viewer \(theFileName0) model Share"
                let activityViewController = UIActivityViewController(activityItems:[title,tmpURL], applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that
                activityViewController.setValue(title1, forKey: "Subject")
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    @IBAction func btnActionRecordVideo(_ sender: Any) {
        self.recorder.isMicrophoneEnabled = false
        if !self.isRecording {
            btn_camra.isHidden = true
            self.startRecording()
            self.unmuted_view.isHidden = true
            blink(bllink_view: self.bllink_view, hidden: false)
        } else {
            self.labelIsRecordingOnOff.isHidden = true
            self.stopRecording()
            self.unmuted_view.isHidden = false
            btn_camra.isHidden = false
            self.muted_image.image = UIImage(named: "muted.png")
            self.bllink_view.layer.removeAllAnimations()
            blink(bllink_view: self.bllink_view, hidden: true)
        }
    }
    @IBAction func BtnActionUnmuted_record(_ sender: UIButton) {
        self.recorder.isMicrophoneEnabled = true
        if !self.isRecording {
            self.startRecording()
            btn_camra.isHidden = true
            self.unmuted_view.isHidden = true
            self.muted_image.image = UIImage(named: "unmute.png")
            blink(bllink_view: self.bllink_view, hidden: false)
        } else {
            self.labelIsRecordingOnOff.isHidden = true
            self.stopRecording()
            btn_camra.isHidden = false
            self.muted_image.image = UIImage(named: "muted.png")
            self.unmuted_view.isHidden = false
            self.bllink_view.layer.removeAllAnimations()
            blink(bllink_view: self.bllink_view, hidden: true)
        }
    }
    func DisplayAlert(_ AlertHeading:String, _ AlertDescription:String){
        let alert = UIAlertController(title: AlertHeading, message: AlertDescription , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    func startRecording() {
        guard recorder.isAvailable else {
            self.isRecording = false
            DisplayAlert("Alert", "Recording failed")
            return
        }
        recorder.startRecording{ [unowned self] (error) in
            guard error == nil else {
                self.isRecording = false
                blink(bllink_view: self.bllink_view, hidden: true)
                DispatchQueue.main.async {
                    self.muted_image.image = UIImage(named: "muted.png")
                    self.unmuted_view.isHidden = false
                    self.btn_camra.isHidden = false
                }
                self.DisplayAlert("Alert", "\(String(describing: error))")
                return
            }
            DispatchQueue.main.async {
                self.btn_camra.isHidden = true
                self.isRecording = true
            }
        }
    }
    func stopRecording() {
        btn_camra.isHidden = false
        recorder.stopRecording { [unowned self] (preview, error) in
            guard preview != nil else {
                self.isRecording = true
                return
            }
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                preview?.modalPresentationStyle = UIModalPresentationStyle.popover
                preview?.popoverPresentationController?.sourceRect = CGRect.zero
                preview?.popoverPresentationController?.sourceView = self.view
            }
            if preview != nil {
                preview?.previewControllerDelegate = self
            }
            self.isRecording = false
            self.present(preview!, animated: true, completion: nil)
        }
    }
    func previewControllerDidFinish(_ previewController: RPPreviewViewController) {
        dismiss(animated: true)
    }
    @IBAction func btnActionChooseFile(_ sender: Any) {
        clickFunction()
    }
    @IBAction func btn_clear(_ sender: Any) {
        comman().remove(key: "selection_nil")
        sceneUrl.removeAll()
        for arr in scene0{
            arr.rootNode.removeFromParentNode()
        }
        scene0.removeAll()
        scene.rootNode.removeFromParentNode()
        mySceneView.scene = nil
        volumeSum = 0
        UserDefaults.standard.removeObject(forKey: "url1")
        UserDefaults.standard.removeObject(forKey: "url")
        UserDefaults.standard.removeObject(forKey: "theFileName")
        mySceneView.isHidden = true
        lbl_valume.text = "0.00 cubic mm"
        volume = "0.00"
        ambientLightNode.removeFromParentNode()
        colorview.isHidden = true
        bllink_view.isHidden = true
        unmuted_view.isHidden = false
    }
    
    func nikhilsir(color: UIColor) -> NSArray{
        let col = CIColor(color:color)
        var redcolor = col.red
        var greencolor = col.green
        var bluecolor = col.blue
        
        redcolor = abs(redcolor) * 255
        greencolor = abs(greencolor) * 255
        bluecolor = abs(bluecolor) * 255
        
        redcolor = redcolor > 255 ? 255 : redcolor
        greencolor = greencolor > 255 ? 255 : greencolor
        bluecolor = bluecolor > 255 ? 255 : bluecolor
        
        let arr = [redcolor,greencolor,bluecolor] as NSArray
        return arr
    }
    @IBAction func btngoldbackground(_ sender: Any) {
           if let email = UserDefaults.standard.string(forKey: "login")  {
        roundviewcolor.backgroundColor = btnWhitecolor.backgroundColor
        let col = CIColor(color: btnWhitecolor.backgroundColor!)
        colorarr = [abs(col.red), abs(col.green), abs(col.blue)]
        UserDefaults.standard.set(colorarr, forKey: "bg_color")
        let right_color = nikhilsir(color: btnWhitecolor.backgroundColor!)
        let leftcolor = nikhilsir(color: btnBluecolor.backgroundColor!)
        colordict = ["left_color":leftcolor, "right_color":right_color, "bg_color":colorarr]
        colorsetApi(bacgroundcolor: colordict)
           }else{
           alartviewShow(str: "Valued Jewelers'Scale User,\n With our new updates, registration is now strongly suggested in order to manage user prefrences for STL File Viewer & Sharing")
        }
    }
    @IBAction func btnbluebackgounrd(_ sender: Any) {
        if let email = UserDefaults.standard.string(forKey: "login")  {
        roundviewcolor.backgroundColor = btnBluecolor.backgroundColor
        let col = CIColor(color: btnBluecolor.backgroundColor!)
        colorarr = [abs(col.red), abs(col.green), abs(col.blue)]
        let leftcolor = nikhilsir(color: btnWhitecolor.backgroundColor!)
        let rightcolor = nikhilsir(color: btnBluecolor.backgroundColor!)
        UserDefaults.standard.set(colorarr, forKey: "bg_color")
        colordict = ["left_color":leftcolor, "right_color":rightcolor, "bg_color":colorarr]
        colorsetApi(bacgroundcolor: colordict)
        }else{
            alartviewShow(str: "Valued Jewelers'Scale User,\n With our new updates, registration is now strongly suggested in order to manage user prefrences for STL File Viewer & Sharing")
        }
    }
    @IBAction func CalculateWeight(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "weightAndCostingViewController") as! weightAndCostingViewController
        let val = clean(String(volumeSum))
        let valueText = val.replacingOccurrences(of: ",", with: "")
        print("\(volumeSum),val;\(val)  valueText:\(valueText)")
        next.stringPassed = val//clean(String(valueText))
        next.goldValue = self.Gold
        next.silverValue = self.Silver
        next.platinumValue = self.Platinum
        next.datevalue = self.DateTime
        
        self.present(next, animated: true, completion: nil)
    }
    var showStl = ""
    @IBAction func btnActionAllColorSelection(_ sender: Any) {
        switch (sender as AnyObject).tag{
        case 1:
            colorSelection = 1
            self.showStl = "1"
            break;
        case 2:
            colorSelection = 2
            self.showStl = "1"
            break;
        case 3:
            colorSelection = 3
            self.showStl = "1"
            break;
        case 4:
            colorSelection = 4
            self.showStl = "1"
            break;
        case 5:
            colorSelection = 5
            self.showStl = "1"
            break;
        case 6:
            colorSelection = 6
            self.showStl = "1"
            break;
        default:
            break;
        }
        //        if let str =  comman().getVALUES(key: "selection_nil") as? String {
        //             drawStlOnSceneKit()
        //            self.showStl = ""
        //        }
        if  UserDefaults.standard.value(forKey: "url1") != nil{
            drawStlOnSceneKit()
            //            self.showStl = ""
        }
    }
    //TODO:drawStlOnSceneKit
    var scene0 = [SCNScene]()
    var volumeSum = Float()
    func drawStlOnSceneKit(){
        
        
        ambientLightNode.removeFromParentNode()
        spotLight.removeFromParentNode()
        
        ambientLightNode.light = SCNLight()
        ambientLightNode.light!.type = SCNLight.LightType.ambient    //ambient directional
        mySceneView.isHidden = false
        if showStl == ""{
            volumeSum = 0
            let url =  UserDefaults.standard.value(forKey: "url1") as! NSArray
            //array(forKey:
            for arr in scene0{
                arr.rootNode.removeFromParentNode()
            }
            scene0.removeAll()
            for i in 0..<url.count{
                let scene00 = try! SCNScene(url: URL(string: url[i] as! String)!, options: nil)
                scene0.append(scene00)
                print("scenes count:",scene0.count)
                scene.rootNode.addChildNode(scene0[i].rootNode)//subscene.rootNode
                vertices(node: scene0[i].rootNode)//mySceneView.scene!.rootNode)
            }
        }
        if colorSelection == 1{
            print("1")
            ambientLightNode.light!.color = UIColor(red: 218/255, green: 165/255, blue: 32/255, alpha: 1.0)
            let colorData0 = NSKeyedArchiver.archivedData(withRootObject: self.ambientLightNode.light!.color)
            UserDefaults.standard.set(colorData0, forKey: "btncolor")
        }else if colorSelection == 2 {
            print("2")
            ambientLightNode.light!.color = UIColor(red: 70/255, green: 70/255, blue: 70/255, alpha: 1.0)
            let colorData0 = NSKeyedArchiver.archivedData(withRootObject: self.ambientLightNode.light!.color)
            UserDefaults.standard.set(colorData0, forKey: "btncolor")
        }else if colorSelection == 3 {
            print("3")
            ambientLightNode.light!.color = UIColor(red: 139/255, green: 69/255, blue: 19/255, alpha: 1.0)
            let colorData0 = NSKeyedArchiver.archivedData(withRootObject: self.ambientLightNode.light!.color)
            UserDefaults.standard.set(colorData0, forKey: "btncolor")
        }else if colorSelection == 4 {
            print("4")
            ambientLightNode.light!.color = UIColor(red: 0/255, green: 165/255, blue: 240/255, alpha: 0.5)
            let colorData0 = NSKeyedArchiver.archivedData(withRootObject: self.ambientLightNode.light!.color)
            UserDefaults.standard.set(colorData0, forKey: "btncolor")
        }else if colorSelection == 5{
            print("5")
            ambientLightNode.light!.color = UIColor(red: 179/255, green: 38/255, blue: 22/255, alpha: 0.5)
            let colorData0 = NSKeyedArchiver.archivedData(withRootObject: self.ambientLightNode.light!.color)
            UserDefaults.standard.set(colorData0, forKey: "btncolor")
        }else if colorSelection == 6 {
            print("6")
            ambientLightNode.light!.color = UIColor(red: -150/255, green: -150/255, blue: -150/255, alpha: 0.5)
            let colorData0 = NSKeyedArchiver.archivedData(withRootObject: self.ambientLightNode.light!.color)
            UserDefaults.standard.set(colorData0, forKey: "btncolor")
        }else{
            let colorData = UserDefaults.standard.object(forKey: "btncolor") as? Data
            print("else")
            if let aData = colorData {
                ambientLightNode.light!.color = NSKeyedUnarchiver.unarchiveObject(with: aData) as? UIColor ?? .yellow
            }else{
                ambientLightNode.light!.color = UIColor(red: 229/255, green: 205/255, blue: 25/255, alpha: 1.0)
                let colorData0 = NSKeyedArchiver.archivedData(withRootObject: self.ambientLightNode.light!.color)
                UserDefaults.standard.set(colorData0, forKey: "btncolor")
            }
        }
        scene.rootNode.addChildNode(ambientLightNode)
        scene.rootNode.addChildNode(spotLight)
        mySceneView.allowsCameraControl = true
        mySceneView.scene = scene
        mySceneView.backgroundColor = UIColor.clear
    }
    
    func vertices(node:SCNNode){
        let planeSources1 = node.childNodes.first?.geometry
        let planeSources = planeSources1?.sources(for: SCNGeometrySource.Semantic.vertex)
        if let planeSource = planeSources?.first {
            var totalVolume = Float()
            
            let stride = planeSource.dataStride
            let offset = planeSource.dataOffset
            let componentsPerVector = planeSource.componentsPerVector
            let bytesPerVector = componentsPerVector * planeSource.bytesPerComponent
            
            let vectors = [SCNVector3](repeating: SCNVector3Zero, count: planeSource.vectorCount)
            let vertices = vectors.enumerated().map({
                (index: Int, element: SCNVector3) -> SCNVector3 in
                let vectorData = UnsafeMutablePointer<Float>.allocate(capacity: componentsPerVector)
                let nsByteRange = NSMakeRange(index * stride + offset, bytesPerVector)
                let byteRange = Range(nsByteRange)
                
                let buffer = UnsafeMutableBufferPointer(start: vectorData, count: componentsPerVector)
                planeSource.data.copyBytes(to: buffer, from: byteRange)
                return SCNVector3Make(buffer[0], buffer[1], buffer[2])
            })
            
            var x1 = Float(),x2 = Float(),x3 = Float(),y1 = Float(),y2 = Float(),y3 = Float(),z1 = Float(),z2 = Float(),z3 = Float()
            var i = 0
            while i < vertices.count{
                
                x1 = vertices[i].x;
                y1 = vertices[i].y;
                z1 = vertices[i].z;
                
                x2 = vertices[i + 1].x;
                y2 = vertices[i + 1].y;
                z2 = vertices[i + 1].z;
                
                x3 = vertices[i + 2].x;
                y3 = vertices[i + 2].y;
                z3 = vertices[i + 2].z;
                
                totalVolume +=
                    (-x3 * y2 * z1 +
                        x2 * y3 * z1 +
                        x3 * y1 * z2 -
                        x1 * y3 * z2 -
                        x2 * y1 * z3 +
                        x1 * y2 * z3);
                
                i = i + 3
            }
            totalVolume = totalVolume / 6;
            //            volume = "\(totalVolume)"
            print("totalvolume: \(totalVolume) sumvolume: \(volumeSum)")
            volumeSum = volumeSum + totalVolume
            lbl_valume.text = "\(clean(String(volumeSum))) cubic mm"
        }
        
    }
    
    func clickFunction(){
        let alertController = UIAlertController(title: "Open & Add Stl Files", message: "", preferredStyle: .alert)
        
        let open = UIAlertAction(title: "Open", style: .default) { (action:UIAlertAction) in
            self.sceneUrl.removeAll()
            for arr in self.scene0{
                arr.rootNode.removeFromParentNode()
            }
            self.scene0.removeAll()
            self.scene.rootNode.removeFromParentNode()
            self.lbl_valume.text = "0.0 cubic mm"
            self.mySceneView.scene = nil
            UserDefaults.standard.removeObject(forKey: "url1")
            let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: ["public.data"], in: UIDocumentPickerMode.import)
            documentPicker.delegate = self
            documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            self.present(documentPicker, animated: true, completion: nil)
        }
        let add = UIAlertAction(title: "Add", style: .default) { (action:UIAlertAction) in
            let documentPicker: UIDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: ["public.data"], in: UIDocumentPickerMode.import)
            documentPicker.delegate = self
            documentPicker.modalPresentationStyle = UIModalPresentationStyle.fullScreen
            self.present(documentPicker, animated: true, completion: nil)
        }
        //        let FTP = UIAlertAction(title: "FTP", style: .default) { (action:UIAlertAction) in
        //            let next = self.storyboard?.instantiateViewController(withIdentifier: "FTPLoginViewController") as! FTPLoginViewController
        //            self.present(next, animated: true, completion: nil)
        //        }
        let canecl = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(open)
        alertController.addAction(add)
        //        alertController.addAction(FTP)
        alertController.addAction(canecl)
        self.present(alertController, animated: true, completion: nil)
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        showStl = ""
        let myURL = url as URL
        strNameOfSTLfile = myURL.lastPathComponent
        newTempUrlFromiCloud = url
        self.theFileName = (strNameOfSTLfile as NSString).lastPathComponent
        UserDefaults.standard.set(self.theFileName, forKey: "theFileName")
        if newTempUrlFromiCloud?.pathExtension == "stl" {
            comman().userdefultSET(value: "1", key: "selection_nil")
            DispatchQueue.main.async {
                self.sceneUrl.append(url)
                var strings : [String] = []
                if let array:[URL] = self.sceneUrl{
                    for url in array {
                        let string = url.absoluteString
                        strings.append(string)
                    }
                }
                UserDefaults.standard.set(strings, forKey: "url1")
                self.drawStlOnSceneKit()
                self.bring()
            }
        }else{
            Utility().displayAlert(title: "Alert", message: "Please select STL file ", control: ["Yes"])
            bring()
        }
    }
    
    func bring(){
        let spring = CASpringAnimation(keyPath: "transform.scale")
        spring.duration = 0.5
        spring.fromValue = 0
        spring.toValue = 1
        spring.autoreverses = true
        spring.repeatCount = 100000000000
        spring.initialVelocity = 10
        spring.damping = 95
        bllink_view.layer.add(spring, forKey: nil)
    }
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        bring()
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        bring()
        comman().remove(key: "selection_nil")
    }
    func colorsetApi(bacgroundcolor: NSDictionary){
        let jsonData = try! JSONSerialization.data(withJSONObject: bacgroundcolor, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        postWithoutImage(url_string: "https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/insertColorCode", parameters: "user_id=\(userid)&color_json=\(jsonString)", CompletionHandler: {json, error in
        })
    }
    func CallWebService(){
        guard Reachability.isConnectedToNetwork() == true else{
            let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
            return
        }
        let strUrl : String = "https://adarshtated.com/dev/jeweller_app/test_new_index.php?fun=get_prices"
        let url = strUrl
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request, completionHandler: {(Data,response,Error) -> Void in
            guard let data = Data, Error == nil else {
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                return
            }
            let responseString = String(data: data, encoding: .utf8)
            var dictonary:NSDictionary?
            if let data = responseString?.data(using: String.Encoding.utf8) {
                DispatchQueue.main.async(execute:{
                    do {
                        dictonary =  try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject] as NSDictionary?
                        if let jsonobject = dictonary{
                            let data = jsonobject.value(forKey: "metal_details") as! NSArray
                            self.DateTime = jsonobject.value(forKey: "date_time") as! String
                            let data1 = data[0] as! NSDictionary
                            self.Gold = data1.value(forKey: "gold_pm") as! String
                            self.Silver = data1.value(forKey: "silver") as! String
                            self.Platinum = data1.value(forKey: "platinum_pm") as! String
                            UserDefaults.standard.set(self.Gold, forKey: "gold")
                            
                        }else{
                            let alert = UIAlertController(title: "Alert!", message: "Check Your Internet Connection", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                            }
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }catch let error {
                        print("eerroorr:-\(error.localizedDescription)")
                    }
                })
            }
        })
        task.resume()
    }
    public struct ReplayFileCoordinator {
        public static let shared = ReplayFileCoordinator()        
        private init() {
            if let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
                let replayDirectoryPath = documentDirectoryPath.appending("/Replays")
                let fileManager = FileManager.default
                if !fileManager.fileExists(atPath: replayDirectoryPath) {
                    do {
                        try fileManager.createDirectory(atPath: replayDirectoryPath,
                                                        withIntermediateDirectories: false,
                                                        attributes: nil)
                    } catch {}
                }
            }
        }
        public func filePath(_ fileName: String) -> String {
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let documentsDirectory = paths[0] as String
            let filePath = "\(documentsDirectory)/Replays/\(fileName).mp4"
            return filePath
        }
        public var allReplays: [URL] {
            let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            let replayPath = documentsDirectory?.appendingPathComponent("/Replays")
            let directoryContents = try! FileManager.default.contentsOfDirectory(at: replayPath!, includingPropertiesForKeys: nil, options: [])
            return directoryContents
        }
    }
    @IBAction func Menu_btn(_ sender: UIButton) {
        if let email = UserDefaults.standard.string(forKey: "login")  {
           signCompleteLogin.isHidden = false
        }else{
            signViewShow.isHidden = false
        }
    }
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        signViewShow.isHidden = true
        signCompleteLogin.isHidden = true
    }
}
extension SCNVector3 {
    func length() -> Float {
        return sqrtf(x * x + y * y + z * z)
    }
}
func - (l: SCNVector3, r: SCNVector3) -> SCNVector3 {
    return SCNVector3Make(l.x - r.x, l.y - r.y, l.z - r.z)
}
public func blink(bllink_view:UIView,hidden:Bool) {
    DispatchQueue.main.async {
        bllink_view.isHidden = hidden
        let spring = CASpringAnimation(keyPath: "transform.scale")
        spring.duration = 0.5
        spring.fromValue = 0
        spring.toValue = 1
        spring.autoreverses = true
        spring.repeatCount = 100000000000
        spring.initialVelocity = 10
        spring.damping = 95
        bllink_view.layer.add(spring, forKey: nil)
    }
}
extension UIView {
    func capture() -> UIImage? {
        var image: UIImage?
        
        if #available(iOS 10.0, *) {
            let format = UIGraphicsImageRendererFormat()
            format.opaque = isOpaque
            let renderer = UIGraphicsImageRenderer(size: frame.size, format: format)
            image = renderer.image { context in
                drawHierarchy(in: frame, afterScreenUpdates: true)
            }
        } else {
            UIGraphicsBeginImageContextWithOptions(frame.size, isOpaque, UIScreen.main.scale)
            drawHierarchy(in: frame, afterScreenUpdates: true)
            image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        return image
    }
}
extension ViewController1 {
    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.presentPreview(animated: true)
    }
}
extension ViewController1: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        guard let navVC = self.navigationController else {
            return self
        }
        return navVC
    }
}
extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}
class CustomPhotoAlbum: NSObject {
    static var albumName = "Album name"
    static let shared = CustomPhotoAlbum()
    private var assetCollection: PHAssetCollection!
    private override init() {
        super.init()
        if let assetCollection = fetchAssetCollectionForAlbum() {
            self.assetCollection = assetCollection
            return
        }
    }
    private func checkAuthorizationWithHandler(completion: @escaping ((_ success: Bool) -> Void)) {
        if PHPhotoLibrary.authorizationStatus() == .notDetermined {
            PHPhotoLibrary.requestAuthorization({ (status) in
                self.checkAuthorizationWithHandler(completion: completion)
            })
        }else if PHPhotoLibrary.authorizationStatus() == .authorized {
            self.createAlbumIfNeeded()
            completion(true)
        }else {
            completion(false)
        }
    }
    private func createAlbumIfNeeded() {
        if let assetCollection = fetchAssetCollectionForAlbum() {
            self.assetCollection = assetCollection
        } else {
            PHPhotoLibrary.shared().performChanges({
                PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: CustomPhotoAlbum.albumName)
            }) { success, error in
                if success {
                    self.assetCollection = self.fetchAssetCollectionForAlbum()
                } else {
                }
            }
        }
    }
    private func fetchAssetCollectionForAlbum() -> PHAssetCollection? {
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", CustomPhotoAlbum.albumName)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        if let _: AnyObject = collection.firstObject {
            return collection.firstObject
        }
        return nil
    }
    func save(image: UIImage,theFileName:String) {
        self.checkAuthorizationWithHandler { (success) in
            if success, self.assetCollection != nil {
                PHPhotoLibrary.shared().performChanges({
                    let data = image.pngData() as NSData?
                    if #available(iOS 10.0, *) {
                        let tmpURL = FileManager.default.temporaryDirectory
                            .appendingPathComponent("\(theFileName)")
                        do {
                            try data?.write(to: tmpURL)
                        } catch {}
                    }
                    let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: UIImage(data: data! as Data)!)
                    let assetPlaceHolder = assetChangeRequest.placeholderForCreatedAsset
                    let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
                    let enumeration: NSArray = [assetPlaceHolder!]
                    albumChangeRequest!.addAssets(enumeration)
                }, completionHandler: nil)
            }
        }
    }
}

extension UIColor {
    var redValue: CGFloat{
        return cgColor.components! [0]
    }
    var greenValue: CGFloat{
        return cgColor.components! [1]
    }
    var blueValue: CGFloat{
        return cgColor.components! [2]
    }
    var alphaValue: CGFloat{
        return cgColor.components! [3]
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return NSString(format:"%06x", rgb) as String
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
}
