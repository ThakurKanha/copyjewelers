//
//  weightAndCostingViewController.swift
//  JewelersScale
//  Created by Virendra on 09/01/19.
//  Copyright © 2019 MacOS High. All rights reserved.
//

import UIKit
class weighConst {
    static var weightFlag = [true,true,true,true,true,true,true];
}
class weightAndCostingObj{
    var val = Float(1.0)
    var viewFrame = UIView()
    var imgView = UIImageView()
    var lblWeight = UILabel()
    var lblWeight2 = UILabel()
    var txtWeight = UITextField()
    var lblcost = UILabel()
    var sendButton = UIButton()
    var checkButton = UIButton()
    static var weightVal = [184.45930, 149.64459, 128.61736, 116.16088, 100.41421,75.922143, 72.34581];
    init(size:CGRect,goldv2:weightAndCostingViewController,tag:Int,str:String) {
        let grect = goldv2.wcc_View.frame
        viewFrame.frame = size
        var txtsize = CGFloat()
        if UI_USER_INTERFACE_IDIOM() == .pad{
            txtsize = 22.0
        }else{
            txtsize = 14.0
        }
        
        let diff = (grect.width - size.width) * 0.4
        imgView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        imgView.image = UIImage(named: str)
        lblWeight.frame = CGRect(x: size.width * 0.01, y: size.height * 0.42, width: size.width * 0.23, height: size.height * 0.31)
        
        lblWeight2.frame = CGRect(x: size.width * 0.25, y: size.height * 0.42, width: size.width * 0.265, height: size.height * 0.31)
        txtWeight.frame = CGRect(x: size.width * 0.53, y: size.height * 0.42, width: size.width * 0.183, height: size.height * 0.31)
        txtWeight.backgroundColor = .blue
        lblcost.frame = CGRect(x: size.width * 0.725, y: size.height * 0.42, width: size.width * 0.265, height: size.height * 0.31)
        checkButton.frame = CGRect(x: (grect.width - diff * 0.9) , y: size.origin.y + size.height/2 - diff * 0.4, width: diff * 0.8 , height: diff * 0.8)
        sendButton.frame = CGRect(x: grect.width * 0.45, y: size.origin.y + size.height * 0.7, width: size.width * 0.65, height: size.height * 0.44)
        sendButton.backgroundColor = UIColor(displayP3Red: 67.0/255.0, green: 75.0/255.0, blue: 197.0/255, alpha: 1)
        checkButton.addTarget(self, action: #selector(checkbuttonAction), for: .touchUpInside)
        txtWeight.delegate = goldv2
        txtWeight.tag = tag
        checkButton.tag = tag
        sendButton.tag = tag
        txtWeight.placeholder = "Enter value"
        lblWeight.text = "$0"
        lblWeight2.text = "$0"
        lblcost.text = "$0.00"
        sendButton.setTitle("Send to Jeweler's Ledger", for: .normal)
        txtWeight.backgroundColor = .clear
        txtWeight.keyboardType = .decimalPad
        txtWeight.textAlignment = .center
        lblWeight.textAlignment = .center
        lblWeight2.textAlignment = .center
        lblcost.textAlignment = .center
        lblWeight2.font = UIFont.systemFont(ofSize: txtsize)
        lblWeight.font = UIFont.systemFont(ofSize: txtsize)
        txtWeight.font = UIFont.systemFont(ofSize: txtsize)
        lblcost.font = UIFont.systemFont(ofSize: txtsize)
        checkButton.setImage(UIImage(named: "empty_check_box.png"), for: .normal)
      
        viewFrame.addSubview(imgView)
        viewFrame.addSubview(lblWeight)
        viewFrame.addSubview(lblWeight2)
        viewFrame.addSubview(txtWeight)
        viewFrame.addSubview(lblcost)
        goldv2.wcc_View.addSubview(viewFrame)
        goldv2.wcc_View.addSubview(checkButton)
        //goldv2.wcc_View.addSubview(sendButton)
    }
    let value = weightAndCostingViewController()
    @objc func checkbuttonAction(sender:UIButton!) {
        value.didPressButtonCheckbutton00(sender)
    }
}
var obj = weightAndCostingViewController()
class weightAndCostingViewController: UIViewController,UITextFieldDelegate {
    
 
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_volume: UILabel!
    @IBOutlet weak var txt_Gold: UITextField!
    @IBOutlet weak var txt_Platinum: UITextField!
    @IBOutlet weak var txt_Silver: UITextField!
    @IBOutlet weak var wcc_View: UIView!
    @IBOutlet weak var laser_Button: UIButton!
    @IBOutlet weak var new_Calculation: UIButton!
    @IBOutlet weak var volumeView: UIView!
    
    var signViewShow = UIView()
    var signCompleteLogin = UIView()
    var arraycell = NSMutableArray()
    var goldValue = ""
    var silverValue = ""
    var platinumValue = ""
    var datevalue = ""
    var tag = Int()
    var arrValues:[Double] = []
    var countr = Int()
    var stringPassed = String()
    var textfield = UITextField()
    static var sendvalue = ""
    static var arrList = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()        
        obj = self
        txt_Gold.text = cleanDollars(goldValue)//"$"+goldValue
        txt_Platinum.text = cleanDollars(platinumValue)//"$"+platinumValue
        txt_Silver.text = cleanDollars(silverValue)//"$"+silverValue
        lbl_date.text = datevalue
        lbl_volume.text = stringPassed
        stringPassed = stringPassed.replacingOccurrences(of: ",", with: "")
        signViewShow = showViewLogIn()
        signCompleteLogin = showViewLogInComplete()
        signViewShow.isHidden = true
        signCompleteLogin.isHidden = true     
        arraycell = ["brass.png","silver_new.png","10k_gold.png","14k_gold.png","18k_gold.png","24K-GOLD.png","platinum.png"]
        if let scrap_data = UserDefaults.standard.value(forKey: "wcc"){
            for j in 0..<(scrap_data as! NSArray).count{
                let flag = (scrap_data as! NSArray)[j] as? Bool
                weighConst.weightFlag[j] = flag ?? false
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        CheckUpdate()
    }
    override func viewDidAppear(_ animated: Bool) {
        print("didap")
        if let email = UserDefaults.standard.string(forKey: "login"){
            print("email:\(email)")
        }else{
            alartviewShow(str: "Valued Jewelers'Scale User,\n With our new updates, registration is now strongly suggested in order to manage user prefrences for Weight & Casting Cost Calculator")
        }
    }
    @IBAction func reset_Button(_ sender: UIButton) {
        txt_Gold.text = cleanDollars(goldValue)//"$"+goldValue
        txt_Silver.text = cleanDollars(silverValue)//"$"+silverValue
        txt_Platinum.text = cleanDollars(platinumValue)//"$"+platinumValue
        SetDwtGram()
        
    }
    @IBAction func new_Calculation(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func calculator_Btn(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "ViewController12") as! ViewController12
        self.present(next, animated: true, completion: nil)
    }
    @IBAction func ledger_Btn(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "LaserViewController") as! LaserViewController
        self.present(next, animated: true, completion: nil)
    }
    func CheckUpdate() {
        weightAndCostingViewController.arrList.removeAllObjects()
        arrValues.removeAll()
        for view in wcc_View.subviews{
            view.removeFromSuperview()
        }
        var val = CGFloat(0)
        for i in 0..<7{
            if weighConst.weightFlag[i] {
            var rect = volumeView.frame
            rect.origin.y = val
            rect.size.height = (self.wcc_View.frame.height / 7.0) - 10
            let viewn = weightAndCostingObj(size: rect, goldv2: self, tag: i, str: arraycell[i] as! String)
                weightAndCostingViewController.arrList.add(viewn)
            val = val + CGFloat(rect.size.height) + 10
            let val = weightAndCostingObj.weightVal[i]
            arrValues.append(val)
            }
        }
        for obj in weightAndCostingViewController.arrList{
            let view = obj as! weightAndCostingObj
            self.wcc_View.addSubview((obj as! weightAndCostingObj).sendButton)
            let btn = view.sendButton
            btn.addTarget(self, action: #selector(sendbuttonAction), for: .touchUpInside)
            view.sendButton.isHidden = true
        }
        SetDwtGram()
    }
    @objc func sendbuttonAction(sender:UIButton!) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "LaserViewController") as! LaserViewController
        next.tatalCasting = (weightAndCostingViewController.sendvalue.replacingOccurrences(of: "$", with: ""))
        self.present(next, animated: true, completion: nil)
        
    }
    func didPressButtonCheckbutton00(_ sender: UIButton) {
        if  sender.currentImage == UIImage(named: "empty_check_box.png"){
            for val in weightAndCostingViewController.arrList{
                let gsObj = val as! weightAndCostingObj
                gsObj.checkButton.setImage(UIImage(named: "empty_check_box.png"), for: .normal)
                if gsObj.sendButton.tag == sender.tag{
                    gsObj.sendButton.isHidden = false
                    weightAndCostingViewController.sendvalue = gsObj.lblcost.text!
                }else{
                    gsObj.sendButton.isHidden = true
                }
            }
            sender.setImage(UIImage(named: "check_box.png"), for: .normal)
            print("check")
            obj.laser_Button.isHidden = true
            obj.new_Calculation.isHidden = true
        }else{
            for val in weightAndCostingViewController.arrList{
                let gsObj = val as! weightAndCostingObj
                gsObj.checkButton.setImage(UIImage(named: "empty_check_box.png"), for: .normal)
                gsObj.sendButton.isHidden = true
            }
            sender.setImage(UIImage(named: "empty_check_box.png"), for: .normal)
            print("un - check")
            obj.laser_Button.isHidden = false
            obj.new_Calculation.isHidden = false
        }
        
    }
    func SetDwtGram(){
        var cont = 0
        for val in weightAndCostingViewController.arrList{
            let gsObj = val as! weightAndCostingObj
            let lbl1 = Float(stringPassed)!/Float(arrValues[cont])
            gsObj.lblWeight.text = (NSString(format: "%.2f", lbl1) as String)
             let lbl2  = lbl1 * 1.55517;
            gsObj.lblWeight2.text = (NSString(format: "%.2f", lbl2) as String)
            cont = cont+1
        }
        SetTotalCasting()
    }
    func SetTotalCasting(){
        for val in weightAndCostingViewController.arrList{
            let gsObj = val as! weightAndCostingObj
            switch(gsObj.txtWeight.tag){
            case 0:
                 let val = 1
                gsObj.txtWeight.text = "$"+String(val)
                 let val1 = (Float(stringPassed)! / Float(weightAndCostingObj.weightVal[gsObj.txtWeight.tag]) * Float(val))
                 gsObj.lblcost.text = cleanDollars(String(val1))//"$"+(NSString(format: "%.2f", val1) as String)
                break
            case 1:
                let valueText = txt_Silver.text!.replacingOccurrences(of: "$", with: "")
                let valueText1 = valueText.replacingOccurrences(of: ",", with: "")
                let val = ((Float(valueText1) ?? 0.0) * 0.0775) + 1.50
                 gsObj.txtWeight.text =  cleanDollars(String(val))//"$"+(NSString(format: "%.2f", val) as String)
                let val1 = (Float(stringPassed)! / Float(weightAndCostingObj.weightVal[gsObj.txtWeight.tag]) * val)
                gsObj.lblcost.text =  cleanDollars(String(val1))//"$"+(NSString(format: "%.2f", val1) as String)
                break
            case 2:
                let valueText = txt_Gold.text!.replacingOccurrences(of: "$", with: "")
                let valueText1 = valueText.replacingOccurrences(of: ",", with: "")
                let val = (0.417 * (Float(valueText1) ?? 0.0) * 0.06) + 1.50
                gsObj.txtWeight.text =  cleanDollars(String(val))//"$"+(NSString(format: "%.2f", val) as String)
                let val1 = (Float(stringPassed)! / Float(weightAndCostingObj.weightVal[gsObj.txtWeight.tag]) * val)
                gsObj.lblcost.text =  cleanDollars(String(val1))//"$"+(NSString(format: "%.2f", val1) as String)
                break
            case 3:
                let valueText = txt_Gold.text!.replacingOccurrences(of: "$", with: "")
                let valueText1 = valueText.replacingOccurrences(of: ",", with: "")
                let val = (0.584 * (Float(valueText1) ?? 0.0) * 0.06) + 1.50
                gsObj.txtWeight.text =  cleanDollars(String(val))//"$"+(NSString(format: "%.2f", val) as String)
                let val1 = (Float(stringPassed)! / Float(weightAndCostingObj.weightVal[gsObj.txtWeight.tag]) * val)
                gsObj.lblcost.text =  cleanDollars(String(val1))//"$"+(NSString(format: "%.2f", val1) as String)
                break
            case 4:
                let valueText = txt_Gold.text!.replacingOccurrences(of: "$", with: "")
                let valueText1 = valueText.replacingOccurrences(of: ",", with: "")
                 let val = (0.75 *  (Float(valueText1) ?? 0.0) * 0.06) + 1.50
                gsObj.txtWeight.text =  cleanDollars(String(val))//"$"+(NSString(format: "%.2f", val) as String)
                let val1 = (Float(stringPassed)! / Float(weightAndCostingObj.weightVal[gsObj.txtWeight.tag]) * val)
                gsObj.lblcost.text =  cleanDollars(String(val1))//"$"+(NSString(format: "%.2f", val1) as String)
                break
            case 5:
                let valueText = txt_Gold.text!.replacingOccurrences(of: "$", with: "")
                let valueText1 = valueText.replacingOccurrences(of: ",", with: "")
                let   val = (0.995 * (Float(valueText1) ?? 0.0) * 0.06) + 1.50
                gsObj.txtWeight.text =  cleanDollars(String(val))//"$"+(NSString(format: "%.2f", val) as String)
                let val1 = (Float(stringPassed)! / Float(weightAndCostingObj.weightVal[gsObj.txtWeight.tag]) * val)
                gsObj.lblcost.text =  cleanDollars(String(val1))//"$"+(NSString(format: "%.2f", val1) as String)
                break
            case 6:
                let valueText = txt_Platinum.text!.replacingOccurrences(of: "$", with: "")
                let valueText1 = valueText.replacingOccurrences(of: ",", with: "")
                let   val = (Float(valueText1) ?? 0.0) * 0.0680
                gsObj.txtWeight.text =  cleanDollars(String(val))//"$"+(NSString(format: "%.2f", val) as String)
                let val1 = (Float(stringPassed)! / Float(weightAndCostingObj.weightVal[gsObj.txtWeight.tag]) * val)
                gsObj.lblcost.text =  cleanDollars(String(val1))//"$"+(NSString(format: "%.2f", val1) as String)
                break
            default:
                break;
            }
        }
    }
    func textfieldEditValue(tag:Int){
        var gsObj : weightAndCostingObj!
        for val in weightAndCostingViewController.arrList{
            let obj = val as! weightAndCostingObj
            if tag == obj.txtWeight.tag{
                gsObj = obj
            }
        }
        switch(gsObj.txtWeight.tag){
            case 0:
                let valueText = gsObj.txtWeight.text!.replacingOccurrences(of: "$", with: "")
                let val1 = Float(stringPassed)! / Float(weightAndCostingObj.weightVal[0]) * (Float(valueText) ?? 0.0)
                gsObj.lblcost.text = "$"+(NSString(format: "%.2f", val1) as String)
                break
            case 1:
                let valueText = gsObj.txtWeight.text!.replacingOccurrences(of: "$", with: "")
                let val1 = Float(stringPassed)! / Float(weightAndCostingObj.weightVal[1]) * (Float(valueText) ?? 0.0)
                gsObj.lblcost.text = "$"+(NSString(format: "%.2f", val1) as String)
                break
            case 2:
                let valueText = gsObj.txtWeight.text!.replacingOccurrences(of: "$", with: "")
                let val1 = Float(stringPassed)! / Float(weightAndCostingObj.weightVal[2]) * (Float(valueText) ?? 0.0)
                gsObj.lblcost.text = "$"+(NSString(format: "%.2f", val1) as String)
                break
            case 3:
                let valueText = gsObj.txtWeight.text!.replacingOccurrences(of: "$", with: "")
                let val1 = Float(stringPassed)! / Float(weightAndCostingObj.weightVal[3]) * (Float(valueText) ?? 0.0)
                gsObj.lblcost.text = "$"+(NSString(format: "%.2f", val1) as String)
                break
            case 4:
                let valueText = gsObj.txtWeight.text!.replacingOccurrences(of: "$", with: "")
                let val1 = Float(stringPassed)! / Float(weightAndCostingObj.weightVal[4]) * (Float(valueText) ?? 0.0)
                gsObj.lblcost.text = "$"+(NSString(format: "%.2f", val1) as String)
                break
            case 5:
               let valueText = gsObj.txtWeight.text!.replacingOccurrences(of: "$", with: "")
                let val1 = Float(stringPassed)! / Float(weightAndCostingObj.weightVal[5]) * (Float(valueText) ?? 0.0)
                gsObj.lblcost.text = "$"+(NSString(format: "%.2f", val1) as String)
                break
            case 6:
                let valueText = gsObj.txtWeight.text!.replacingOccurrences(of: "$", with: "")
                let val1 = Float(stringPassed)! / Float(weightAndCostingObj.weightVal[6]) * (Float(valueText) ?? 0.0)
                gsObj.lblcost.text = "$"+(NSString(format: "%.2f", val1) as String)
                break
            default:
                break;
            }
            return
    }
    
   
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField.tag == 101 || textField.tag == 102 || textField.tag == 103 {
//            SetDwtGram()
//            return true
//        }
        self.tag = textField.tag
        let myTimer = Timer(timeInterval: 0.01, target: self, selector: #selector(self.selectText1(_:)), userInfo: nil, repeats: false)
        RunLoop.current.add(myTimer, forMode: RunLoop.Mode.common)
        return true
    }
    @objc func selectText1(_ timer: Timer) {
        print("tag",tag)
        if tag == 101 || tag == 102 || tag == 103 {
            SetDwtGram()
            return
        }
        textfieldEditValue(tag: tag)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        signViewShow.isHidden = true
        signCompleteLogin.isHidden = true
    }
    @IBAction func Back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func Menu_btn(_ sender: UIButton) {
        if let email = UserDefaults.standard.string(forKey: "login")  {
           signCompleteLogin.isHidden = false
        }else{
           signViewShow.isHidden = false
        }
    }
   
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textfield = textField
        let myTimer = Timer(timeInterval: 0.01, target: self, selector: #selector(self.selectText(_:)), userInfo: nil, repeats: false)
        RunLoop.current.add(myTimer, forMode: RunLoop.Mode.common)
        
        return true
        
    }
    @objc func selectText(_ timer: Timer) {
        textfield.becomeFirstResponder()
        textfield.selectedTextRange = textfield.textRange(from: textfield.beginningOfDocument, to: textfield.endOfDocument)
    }
}
