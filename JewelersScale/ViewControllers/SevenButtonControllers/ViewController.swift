//
//  ViewController.swift
//  JewelersScale
//
//  Created by MacOS High on 06/04/18.
//  Copyright © 2018 MacOS High. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import SystemConfiguration
import GoogleMobileAds
import SwiftKeychainWrapper
class ViewController: UIViewController ,AVPlayerViewControllerDelegate,UITextFieldDelegate,GADBannerViewDelegate, GADInterstitialDelegate {
    

    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet weak var view_banner: GADBannerView!
    @IBOutlet weak var activator: UIActivityIndicatorView!
    @IBOutlet weak var fiealdAmount: UITextField!
    @IBOutlet weak var btnCalculate: UIButton!
    var interstitial: GADInterstitial?
    var playerController = AVPlayerViewController()
    var Gold = ""
    var Silver = ""
    var Platinum = ""
    var DateTime = ""
    let TAG = String("Osiya~~~~~~")
    var currentdate = Date()
    var lastdate = String()
    var signViewShow = UIView()
    var signCompleteLogin = UIView()
   
    @IBAction func Menu_btn(_ sender: UIButton) {
        if let email = UserDefaults.standard.string(forKey: "login")  {
            self.signCompleteLogin.isHidden = false
        }else{
            self.signViewShow.isHidden = false
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.signViewShow.isHidden = true
        self.signCompleteLogin.isHidden = true
    }
    override func viewDidLoad()
    {
        self.signViewShow = showViewLogIn()
        self.signCompleteLogin = showViewLogInComplete()
        self.signViewShow.isHidden = true
        self.signCompleteLogin.isHidden = true
        
        currentdate = Date.changeDaysBy(days: 0)
        fiealdAmount.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        self.fiealdAmount.delegate = self
        back_btn.imageView?.contentMode = .scaleAspectFit
        back_btn.setImage(UIImage(named: "back.png"), for: .normal)
        super.viewDidLoad()
        let boolen =  UserDefaults.standard.bool(forKey: "BOOL")
        if boolen == false{
            view_banner.adUnitID = "ca-app-pub-6572892411343856/7270604337"
            view_banner.rootViewController = self
            view_banner.load(GADRequest())
            interstitial = createAndLoadInterstitial()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        print("didap")
        if let email = UserDefaults.standard.string(forKey: "login"){
            print("email:\(email)")
        }else{
            alartviewShow(str: "Valued Jewelers'Scale User,\n With our new updates, registration is now strongly suggested in order to manage user prefrences for Weight & Casting Cost Calculator")
        }
    }
    func interstitialDidReceiveAd(_ ad: GADInterstitial!) {
        ad.present(fromRootViewController: self)
    }
    func interstitialDidFail(toPresentScreen ad: GADInterstitial!) {
        print("Fail to receive interstitial")
    }
    private func createAndLoadInterstitial() -> GADInterstitial? {
        interstitial = GADInterstitial(adUnitID: "  ")
        guard let interstitial = interstitial else {
            return nil
        }
        let request = GADRequest()
        request.testDevices = [ kGADSimulatorID ]
        interstitial.load(request)
        interstitial.delegate = self
        return interstitial
    }
    @IBAction func Errorvideo_play(_ sender: Any) {
        playvideo(str1: "volume", str2: "mp4")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func back(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    @IBAction func Calculate(_ sender: Any)
    {
        activator.startAnimating()
        if (fiealdAmount.text?.isEmpty)! {
            let alert = UIAlertController(title: "Information", message: "Enter the Volume", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            activator.stopAnimating()
        }else if (fiealdAmount.text! == ".") {
            let alert = UIAlertController(title: "Information", message: "Enter the Currect Volume", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            activator.stopAnimating()
        }
        else{
            self.fiealdAmount.resignFirstResponder()
            CallWebService()
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            self.view.endEditing(true)
            return false
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  allowcaretor = "1234567890."
        let allowcarectorset = CharacterSet(charactersIn: allowcaretor)
        let setone = CharacterSet(charactersIn: string)
        let cont = (fiealdAmount.text?.components(separatedBy: ".").count)! - 1
        if cont > 0 && string == "."{
            return false
        }
        let maxLength = 7
        let currentString: NSString = fiealdAmount.text! as NSString
        let newString: NSString =
           currentString.replacingCharacters(in: range, with: string) as NSString
        if newString.length <=  maxLength {
         return allowcarectorset.isSuperset(of: setone)
        }
        return  newString.length <= maxLength
    }
    @IBAction func HowToCalculateVolumeOfYourPeace(_ sender: Any){
       playvideo(str1: "volumevideo", str2: "mp4")
    }
    func playvideo(str1: String,str2: String){
        forcelandscapeRight()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(forcelandscapeRight), name: UIDevice.orientationDidChangeNotification, object: nil)
        let path = Bundle.main.path(forResource: str1, ofType: str2)
        let url = NSURL(fileURLWithPath: path!)
        let player = AVPlayer(url:url as URL)
        playerController = AVPlayerViewController()
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.didfinishplaying(note:)),name:NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        playerController.player = player
        playerController.allowsPictureInPicturePlayback = true
        playerController.delegate = self
        playerController.player?.play()
        self.present(playerController,animated:true,completion:nil)
    }
    @objc func forcelandscapeRight() {
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    @objc func didfinishplaying(note : NSNotification)
    {
       playerController.dismiss(animated: true,completion: nil)
    }
    func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.landscapeLeft
    }
    
    func playerViewController(_ playerViewController: AVPlayerViewController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        let currentviewController =  navigationController?.visibleViewController
        
        if currentviewController != playerViewController
        {
            currentviewController?.present(playerViewController,animated: true,completion:nil)
        }
    }
     func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .landscapeLeft
    }
    func CallWebService(){
        guard Reachability.isConnectedToNetwork() == true else{
            let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                self.activator.stopAnimating()
                self.activator.isHidden = true
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
            return
        }
        let strUrl : String = "https://adarshtated.com/dev/jeweller_app/test_new_index.php?fun=get_prices"
        let url = strUrl
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request, completionHandler: {(Data,response,Error) -> Void in
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                return
            }
            let responseString = String(data: Data!, encoding: .utf8)
            var dictonary:NSDictionary?
            if let data = responseString?.data(using: String.Encoding.utf8) {
                DispatchQueue.main.async(execute:{
            do {
                dictonary =  try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject] as NSDictionary?
            if let jsonobject = dictonary{
                let data = jsonobject.value(forKey: "metal_details") as! NSArray
                self.DateTime = jsonobject.value(forKey: "date_time") as! String
                let data1 = data[0] as! NSDictionary
                self.Gold = (data1.value(forKey: "gold_pm") ?? "0") as! String
                self.Silver = (data1.value(forKey: "silver") ?? "0") as! String
                self.Platinum = (data1.value(forKey: "platinum_pm") ?? "0") as! String
                UserDefaults.standard.set(self.Gold, forKey: "gold")
                let next = self.storyboard?.instantiateViewController(withIdentifier: "weightAndCostingViewController") as! weightAndCostingViewController
                next.stringPassed = self.fiealdAmount.text ?? ""
                next.goldValue = self.Gold
                next.silverValue = self.Silver
                next.platinumValue = self.Platinum
                next.datevalue = self.DateTime
                self.present(next,animated: true,completion:nil)
                self.fiealdAmount.text = ""
                self.activator.stopAnimating()
            }else{
                let alert = UIAlertController(title: "Alert!", message: "Check Your Internet Connection", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    self.activator.stopAnimating()
                }
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
                self.activator.stopAnimating()
                    }
            }catch _ {
                DispatchQueue.main.async {
                self.activator.stopAnimating()
                self.activator.isHidden = true
                     }
                   }
               })
            }
        })
        task.resume()
    }
}




