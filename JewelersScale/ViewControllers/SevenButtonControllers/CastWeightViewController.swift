//
//  CastWeightViewController.swift
//  JewelersScale
//
//  Created by Apple on 15/05/18.
//  Copyright © 2018 MacOS High. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SwiftKeychainWrapper
class TableViewCell3: UITableViewCell{
    
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var label1: UILabel!
    override func awakeFromNib() {
        super .awakeFromNib()
        view3.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
       
    }
}
class TableViewCell4: UITableViewCell{
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super .awakeFromNib()
        view4.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        
    }
}
class CastWeightViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,GADBannerViewDelegate, GADInterstitialDelegate{
    
    var DateTime = String()
    var Gold = String()
    var Silver = String()
    var Platinum = String()
    var textfield:UITextField!
    var b: Float = 184.459
    var c: Float = 149.644
    var d: Float = 128.617
    var e: Float = 116.160
    var f: Float = 100.414
    var g: Float = 72.345
    var h: Float = 1.55517
    var i: Float = 0.417
    var j: Float = 0.584
    var k: Float = 0.75
    var l: Float = 0.06
    var m: Float = 0.068 
    var n: Float = 0.0775
    var o: Float = 1.5
    var p: Float = 1.00
    var ba3:Float!
    var signViewShow = UIView()
    var signCompleteLogin = UIView()
    
    
    @IBOutlet weak var btn_gram: UIButton!
    @IBOutlet weak var Banner_view: GADBannerView!
    @IBOutlet weak var btn_kilogram: UIButton!
    @IBOutlet weak var btn_troy: UIButton!
    @IBOutlet weak var btn_pennyWeight: UIButton!
    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet weak var table_view_second: UITableView!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var calculate: UIButton!
    @IBOutlet weak var view_penny: UIView!
    @IBOutlet weak var penny_weight_btn: UIButton!
    @IBOutlet weak var wax_btn: UIButton!
    @IBOutlet weak var fourteen_ct_main: UIButton!
    @IBOutlet weak var result_unit: UILabel!
    @IBOutlet weak var result_lbl: UILabel!
    @IBOutlet weak var txt_digit: UITextField!
  
    @IBOutlet weak var lbl_gold: KGCopyableLabel!
    @IBOutlet weak var lbl_Platinum: KGCopyableLabel!
    @IBOutlet weak var lbl_silver: KGCopyableLabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var Activator: UIActivityIndicatorView!
    @IBOutlet weak var costing_cost: UILabel!
    @IBOutlet weak var costing_costvalue: UILabel!
  
    @IBOutlet weak var lbl_14k: KGCopyableLabel!
    @IBOutlet weak var lbl_18k: KGCopyableLabel!
    @IBOutlet weak var lbl_Cast_platinum: KGCopyableLabel!
    @IBOutlet weak var lbl_Cast_silver: KGCopyableLabel!
    
    
     var glob: Int = 0
     var glob1: Int = 3
     var units = String()
     var  wax: Double             = 1.00
     var  sterling_silver: Double = 10.30
     var  nine_gold: Double       = 11.20
     var  fourteen_gold: Double   = 14.10
     var  eighteen_gold: Double   = 15.70
     var  twenty_two_gold: Double = 17.80
     var  fine_gold: Double       = 19.50
     var  platinum: Double        = 20.60
     var  palladium: Double       = 12.00
    
    var  wax1: Double             = 0.097
    var  sterling_silver1: Double = 1.00
    var  nine_gold1: Double       = 1.11
    var  fourteen_gold1: Double   = 1.31
    var  eighteen_gold1: Double   = 1.50
    var  twenty_two_gold1: Double = 1.73
    var  fine_gold1: Double       = 1.87
    var  platinum1: Double        = 2.08
    var  palladium1: Double       = 1.15
    
    var  wax2: Double             = 0.089
    var  sterling_silver2: Double = 0.90
    var  nine_gold2: Double       = 1.00
    var  fourteen_gold2: Double   = 1.18
    var  eighteen_gold2: Double   = 1.36
    var  twenty_two_gold2: Double = 1.59
    var  fine_gold2: Double       = 1.72
    var  platinum2: Double        = 1.88
    var  palladium2: Double       = 1.04
    
    var  wax3: Double             = 0.071
    var  sterling_silver3: Double = 0.76
    var  nine_gold3: Double       = 0.85
    var  fourteen_gold3: Double   = 1.00
    var  eighteen_gold3: Double   = 1.14
    var  twenty_two_gold3: Double = 1.29
    var  fine_gold3: Double       = 1.40
    var  platinum3: Double        = 1.59
    var  palladium3: Double       = 0.88
    
    var  wax4: Double             = 0.064
    var  sterling_silver4: Double = 0.67
    var  nine_gold4: Double       = 0.74
    var  fourteen_gold4: Double   = 0.88
    var  eighteen_gold4: Double   = 1.00
    var  twenty_two_gold4: Double = 1.15
    var  fine_gold4: Double       = 1.25
    var  platinum4: Double        = 1.34
    var  palladium4: Double       = 0.77
    
    var  wax5: Double             = 0.056
    var  sterling_silver5: Double = 0.58
    var  nine_gold5: Double       = 0.63
    var  fourteen_gold5: Double   = 0.78
    var  eighteen_gold5: Double   = 0.90
    var  twenty_two_gold5: Double = 1.00
    var  fine_gold5: Double       = 1.08
    var  platinum5: Double        = 1.21
    var  palladium5: Double       = 0.67
    
    var  wax6: Double             = 0.051
    var  sterling_silver6: Double = 0.53
    var  nine_gold6: Double       = 0.58
    var  fourteen_gold6: Double   = 0.72
    var  eighteen_gold6: Double   = 0.83
    var  twenty_two_gold6: Double = 0.94
    var  fine_gold6: Double       = 1.00
    var  platinum6: Double        = 1.11
    var  palladium6: Double       = 0.62
    
    var  wax7: Double             = 0.049
    var  sterling_silver7: Double = 0.48
    var  nine_gold7: Double       = 0.53
    var  fourteen_gold7: Double   = 0.63
    var  eighteen_gold7: Double   = 0.72
    var  twenty_two_gold7: Double = 0.83
    var  fine_gold7: Double       = 0.90
    var  platinum7: Double        = 1.00
    var  palladium7: Double       = 0.58
    
    var  wax8: Double             = 0.083
    var  sterling_silver8: Double = 0.87
    var  nine_gold8: Double       = 0.96
    var  fourteen_gold8: Double   = 1.14
    var  eighteen_gold8: Double   = 1.30
    var  twenty_two_gold8: Double = 1.49
    var  fine_gold8: Double       = 1.62
    var  platinum8: Double        = 1.74
    var  palladium8: Double       = 1.00
    var values = Double()
    var pennyweight:Double = 1
    var ounce:Double = 19.999996
    var gram:Double = 0.643015
    var killogram:Double = 643.014930
    var silver_n: Float = 0.0775
    var silvern_o: Float = 1.5            
    var valuetype1 = Double()
    var array = NSMutableArray()
    var arraylist = NSMutableArray()
    var lastdate = String()
    var currentdate = Date()
    
    @IBAction func Menu_btn(_ sender: UIButton) {
        if let email = UserDefaults.standard.string(forKey: "login")  {
            signCompleteLogin.isHidden = false
        }else{
            signViewShow.isHidden = false
        }
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signViewShow = showViewLogIn()
        signCompleteLogin = showViewLogInComplete()
        signViewShow.isHidden = true
        signCompleteLogin.isHidden = true
        
        currentdate = Date.changeDaysBy(days: 0)
        CallWebService()
        result_unit.isHidden = true
         units = "gr"
        valuetype1 = gram
        txt_digit.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        wax_btn.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        fourteen_ct_main.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        penny_weight_btn.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        
        btn_pennyWeight.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        btn_kilogram.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        btn_gram.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        btn_troy.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        
        
        
       back_btn.imageView?.contentMode = .scaleAspectFit
        back_btn.setImage(UIImage(named: "back.png"), for: .normal)
        let padding: CGFloat = 5.0
         txt_digit.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        wax_btn.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: padding, bottom: 0.0, right: -padding)
        wax_btn.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: padding)
        fourteen_ct_main.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: padding, bottom: 0.0, right: -padding)
        fourteen_ct_main.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: padding)
        table_view.delegate = self
        table_view.dataSource = self
        table_view_second.delegate = self
        table_view_second.dataSource = self
        view_penny.isHidden = true
        table_view_second.isHidden = true
        table_view.isHidden = true
        txt_digit.delegate = self
        array = ["Wax","Sterling Silver","9K Gold","14K Gold","18K Gold","22K Gold","24K Fine Gold","Platinum","950 Palladium"]
         arraylist = ["Wax","Sterling Silver **","9K Gold","14K Gold **","18K Gold **","22K Gold","24K Fine Gold","Platinum **","950 Palladium"]
        txt_digit.text = "1.00"
        result_lbl.text = "14.10" + "  gr"
        result_unit.text = "gr"
        penny_weight_btn.setTitle("Gram(s)", for:UIControl.State.normal)
        wax_btn.setTitle("Wax", for:UIControl.State.normal)
        fourteen_ct_main.setTitle("14K Gold **", for:UIControl.State.normal)
       
        let boolen =  UserDefaults.standard.bool(forKey: "BOOL")
        if boolen == false{
            Banner_view.adUnitID = "ca-app-pub-6572892411343856/7270604337"
            Banner_view.rootViewController = self
            Banner_view.load(GADRequest())
        }    
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textfield = textField
        let myTimer = Timer(timeInterval: 0.01, target: self, selector: #selector(self.selectText(_:)), userInfo: nil, repeats: false)
        RunLoop.current.add(myTimer, forMode: RunLoop.Mode.common)
        return true
    }
    @objc func selectText(_ timer: Timer) {
        
        textfield.becomeFirstResponder()
        textfield.selectedTextRange = textfield.textRange(from: textfield.beginningOfDocument, to: textfield.endOfDocument)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.table_view {
            return array.count
        }
        else {
            return arraylist.count
        }
    }
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.table_view {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell3
            cell.label1.text = array[indexPath.row] as? String
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TableViewCell4
            cell.label.text = arraylist[indexPath.row] as? String
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if tableView == self.table_view {
            let selectedValue = array[indexPath.row] as? String
            wax_btn.setTitle(selectedValue, for: .normal)
            table_view.isHidden = true
            if indexPath.row == 0{
              glob = indexPath.row
            }
            if indexPath.row == 1{
                glob = indexPath.row
            }
            if indexPath.row == 2{
                glob = indexPath.row
            }
            if indexPath.row == 3{
                glob = indexPath.row
            }
            if indexPath.row == 4{
                glob = indexPath.row
            }
            if indexPath.row == 5{
                glob = indexPath.row
            }
            if indexPath.row == 6{
                glob = indexPath.row
            }
            if indexPath.row == 7{
                glob = indexPath.row
            }
            if indexPath.row == 8{
                glob = indexPath.row
            }
        }
        else{
            let selectedValue = arraylist[indexPath.row] as? String
            fourteen_ct_main.setTitle(selectedValue, for: .normal)
             table_view_second.isHidden = true
            if indexPath.row == 0{
                glob1 = indexPath.row
            }
            if indexPath.row == 1{
                glob1 = indexPath.row
            }
            if indexPath.row == 2{
                glob1 = indexPath.row
            }
            if indexPath.row == 3{
                glob1 = indexPath.row
            }
            if indexPath.row == 4{
                glob1 = indexPath.row
            }
            if indexPath.row == 5{
                glob1 = indexPath.row
            }
            if indexPath.row == 6{
                glob1 = indexPath.row
            }
            if indexPath.row == 7{
                glob1 = indexPath.row
            }
            if indexPath.row == 8{
                glob1 = indexPath.row
            }
        }
         valuecalculation()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 35
    }
    @IBAction func back(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        valuecalculation()
    }
    func clean(_ value: String?) -> String {

        guard value != nil else { return "$0.00" }
        let doubleValue = Double(value!) ?? 0.0
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.maximumFractionDigits = 2
        currencyFormatter.minimumFractionDigits = 2
        currencyFormatter.numberStyle = .decimal
        currencyFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        return currencyFormatter.string(from: NSNumber(value: doubleValue))! + ("  \(units)") 
    
    }
    func clean2(_ value: String?) -> String {
        
        guard value != nil else { return "$0.00" }
        let doubleValue = Double(value!) ?? 0.0
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.maximumFractionDigits = 2
        currencyFormatter.minimumFractionDigits = 2
        currencyFormatter.numberStyle = .decimal
        currencyFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        return "$ "+currencyFormatter.string(from: NSNumber(value: doubleValue))!  
        
    }
    func calculatvalues(apivalue:String,multipalone:Float,multipaltwo:Float,valuetype:Double){
        let tempString3 = (apivalue as NSString).floatValue
        let b21 = ((tempString3) * (multipalone)) + (multipaltwo)
        let doblevalue = Double(b21)
        let str2 =  values * valuetype * doblevalue
        costing_costvalue.text = clean2(String(str2))
    }
    @IBAction func calculate_click(_ sender: UIButton) {
        
        txt_digit.text = "1.00"
        result_lbl.text = "14.10" + "  gr"
        result_unit.text = "gr"
        self.valuetype1 = gram
        units = "gr"
        penny_weight_btn.setTitle("Gram(s)", for:UIControl.State.normal)
        wax_btn.setTitle("Wax", for:UIControl.State.normal)
        fourteen_ct_main.setTitle("14K Gold **", for:UIControl.State.normal)
        self.firstCastingValue()
    }
    func valuecalculation(){
       let str = txt_digit.text!
       let new = (str as NSString).doubleValue
       let st = result_lbl.text!
       var newyd = (st as NSString).doubleValue
    if glob == 0 && glob1 == 0   {
        newyd = new * wax
        result_lbl.text = clean(String(newyd))
        values = newyd
        costing_cost.isHidden = true
        costing_costvalue.isHidden = true
        }
    if glob == 0 && glob1 == 1{
           newyd = new * sterling_silver
           result_lbl.text = clean(String(newyd))
           costing_cost.isHidden = false
           costing_costvalue.isHidden = false
           let tempString3 = (self.Silver as NSString).floatValue
           let b21 = ((tempString3) * (self.n)) + (self.o)
           let lastvalue = newyd * valuetype1 * Double(b21)
           costing_costvalue.text = clean2(String(lastvalue))
           values = newyd
        }
        if glob == 0 && glob1 == 2{
            newyd = new * nine_gold
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 0 && glob1 == 3{
            newyd = new * fourteen_gold
            result_lbl.text = clean(String(newyd))
            values = newyd
            let tempString1 = (self.Gold as NSString).floatValue
            let b16 = ((self.j) * (tempString1) * (self.l)) + (self.o)
            let lastcalc = newyd * valuetype1 * Double(b16)
            costing_costvalue.text = clean2(String(lastcalc)) //String(b16)
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
        }
        if glob == 0 && glob1 == 4{
            newyd = new * eighteen_gold
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString2 = (self.Gold as NSString).floatValue
            let b18 = ((self.k) * (tempString2) * (self.l)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b18)
            self.costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 0 && glob1 == 5{
            newyd = new * twenty_two_gold
            result_lbl.text = clean(String(newyd))
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
            values = newyd
        }
        if glob == 0 && glob1 == 6{
            newyd = new * fine_gold
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 0 && glob1 == 7{
            newyd = new * platinum
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString15 = (self.Platinum as NSString).floatValue
            let b14 = (tempString15) * (self.m)
            let lastvalue = newyd * valuetype1 * Double(b14)
            self.costing_costvalue.text = clean2(String(lastvalue)) //String(b14)
            
        }
        if glob == 0 && glob1 == 8{
            newyd = new * palladium
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 1 && glob1 == 0{
            newyd = new * wax1
            result_lbl.text = clean(String(newyd))
            values = newyd
            Gold18(nyvalue: newyd)
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 1 && glob1 == 1{
            newyd = new * sterling_silver1
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString3 = (self.Silver as NSString).floatValue
            let b21 = ((tempString3) * (self.n)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b21)
            costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 1 && glob1 == 2{
            newyd = new * nine_gold1
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 1 && glob1 == 3{
            newyd = new * fourteen_gold1
            result_lbl.text = clean(String(newyd))
            values = newyd
            let tempString1 = (self.Gold as NSString).floatValue
            let b16 = ((self.j) * (tempString1) * (self.l)) + (self.o)
            let lastcalc = newyd * valuetype1 * Double(b16)
            costing_costvalue.text = clean2(String(lastcalc))
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
        }
        if glob == 1 && glob1 == 4{
            newyd = new * eighteen_gold1
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString2 = (self.Gold as NSString).floatValue
            let b18 = ((self.k) * (tempString2) * (self.l)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b18)
            self.costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 1 && glob1 == 5{
            newyd = new * twenty_two_gold1
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 1 && glob1 == 6{
            newyd = new * fine_gold1
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 1 && glob1 == 7{
            newyd = new * platinum1
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString15 = (self.Platinum as NSString).floatValue
            let b14 = (tempString15) * (self.m)
            let lastvalue = newyd * valuetype1 * Double(b14)
            self.costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 1 && glob1 == 8{
            newyd = new * palladium1
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 2 && glob1 == 0{
            newyd = new * wax2
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 2 && glob1 == 1{
            newyd = new * sterling_silver2
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString3 = (self.Silver as NSString).floatValue
            let b21 = ((tempString3) * (self.n)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b21)
            costing_costvalue.text = clean2(String(lastvalue)) //String(b21)
        }
        if glob == 2 && glob1 == 2{
            newyd = new * nine_gold2
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 2 && glob1 == 3{
            newyd = new * fourteen_gold2
            result_lbl.text = clean(String(newyd))
            values = newyd
            let tempString1 = (self.Gold as NSString).floatValue
            let b16 = ((self.j) * (tempString1) * (self.l)) + (self.o)
            let lastcalc = newyd * valuetype1 * Double(b16)
            costing_costvalue.text = clean2(String(lastcalc))
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
        }
        if glob == 2 && glob1 == 4{
            newyd = new * eighteen_gold2
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString2 = (self.Gold as NSString).floatValue
            let b18 = ((self.k) * (tempString2) * (self.l)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b18)
            self.costing_costvalue.text = clean2(String(lastvalue))
         
        }
        if glob == 2 && glob1 == 5{
            newyd = new * twenty_two_gold2
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 2 && glob1 == 6{
            newyd = new * fine_gold2
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 2 && glob1 == 7{
            newyd = new * platinum2
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString15 = (self.Platinum as NSString).floatValue
            let b14 = (tempString15) * (self.m)
            let lastvalue = newyd * valuetype1 * Double(b14)
            self.costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 2 && glob1 == 8{
            newyd = new * palladium2
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 3 && glob1 == 0{
            newyd = new * wax3
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 3 && glob1 == 1{
            newyd = new * sterling_silver3
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString3 = (self.Silver as NSString).floatValue
            let b21 = ((tempString3) * (self.n)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b21)
            costing_costvalue.text = clean2(String(lastvalue)) //String(b21)
        }
        if glob == 3 && glob1 == 2{
            newyd = new * nine_gold3
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 3 && glob1 == 3{
            newyd = new * fourteen_gold3
            result_lbl.text = clean(String(newyd))
            values = newyd
            let tempString1 = (self.Gold as NSString).floatValue
            let b16 = ((self.j) * (tempString1) * (self.l)) + (self.o)
            let lastcalc = newyd * valuetype1 * Double(b16)
            costing_costvalue.text = clean2(String(lastcalc))
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
        }
        if glob == 3 && glob1 == 4{
            newyd = new * eighteen_gold3
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString2 = (self.Gold as NSString).floatValue
            let b18 = ((self.k) * (tempString2) * (self.l)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b18)
            self.costing_costvalue.text = clean2(String(lastvalue))
         
        }
        if glob == 3 && glob1 == 5{
            newyd = new * twenty_two_gold3
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 3 && glob1 == 6{
            newyd = new * fine_gold3
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 3 && glob1 == 7{
            newyd = new * platinum3
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString15 = (self.Platinum as NSString).floatValue
            let b14 = (tempString15) * (self.m)
            let lastvalue = newyd * valuetype1 * Double(b14)
            self.costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 3 && glob1 == 8{
            newyd = new * palladium3
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 4 && glob1 == 0{
            newyd = new * wax4
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 4 && glob1 == 1{
            newyd = new * sterling_silver4
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString3 = (self.Silver as NSString).floatValue
            let b21 = ((tempString3) * (self.n)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b21)
            costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 4 && glob1 == 2{
            newyd = new * nine_gold4
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 4 && glob1 == 3{
            newyd = new * fourteen_gold4
            result_lbl.text = clean(String(newyd))
            values = newyd
            let tempString1 = (self.Gold as NSString).floatValue
            let b16 = ((self.j) * (tempString1) * (self.l)) + (self.o)
            let lastcalc = newyd * valuetype1 * Double(b16)
            costing_costvalue.text = clean2(String(lastcalc))
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
        }
        if glob == 4 && glob1 == 4{
            newyd = new * eighteen_gold4
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString2 = (self.Gold as NSString).floatValue
            let b18 = ((self.k) * (tempString2) * (self.l)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b18)
            self.costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 4 && glob1 == 5{
            newyd = new * twenty_two_gold4
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 4 && glob1 == 6{
            newyd = new * fine_gold4
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 4 && glob1 == 7{
            newyd = new * platinum4
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString15 = (self.Platinum as NSString).floatValue
            let b14 = (tempString15) * (self.m)
            let lastvalue = newyd * valuetype1 * Double(b14)
            self.costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 4 && glob1 == 8{
            newyd = new * palladium4
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 5 && glob1 == 0{
            newyd = new * wax5
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 5 && glob1 == 1{
            newyd = new * sterling_silver5
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString3 = (self.Silver as NSString).floatValue
            let b21 = ((tempString3) * (self.n)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b21)
            costing_costvalue.text = clean2(String(lastvalue))

        }
        if glob == 5 && glob1 == 2{
            newyd = new * nine_gold5
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 5 && glob1 == 3{
            newyd = new * fourteen_gold5
            result_lbl.text = clean(String(newyd))
            values = newyd
            let tempString1 = (self.Gold as NSString).floatValue
            let b16 = ((self.j) * (tempString1) * (self.l)) + (self.o)
            let lastcalc = newyd * valuetype1 * Double(b16)
            costing_costvalue.text = clean2(String(lastcalc)) //String(b16)
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
        }
        if glob == 5 && glob1 == 4{
            newyd = new * eighteen_gold5
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString2 = (self.Gold as NSString).floatValue
            let b18 = ((self.k) * (tempString2) * (self.l)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b18)
            self.costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 5 && glob1 == 5{
            newyd = new * twenty_two_gold5
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 5 && glob1 == 6{
            newyd = new * fine_gold5
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 5 && glob1 == 7{
            newyd = new * platinum5
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString15 = (self.Platinum as NSString).floatValue
            let b14 = (tempString15) * (self.m)
            let lastvalue = newyd * valuetype1 * Double(b14)
            self.costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 5 && glob1 == 8{
            newyd = new * palladium5
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 6 && glob1 == 0{
            newyd = new * wax6
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 6 && glob1 == 1{
            newyd = new * sterling_silver6
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString3 = (self.Silver as NSString).floatValue
            let b21 = ((tempString3) * (self.n)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b21)
            costing_costvalue.text = clean2(String(lastvalue))
  
        }
        if glob == 6 && glob1 == 2{
            newyd = new * nine_gold6
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 6 && glob1 == 3{
            newyd = new * fourteen_gold6
            result_lbl.text = clean(String(newyd))
            values = newyd
            let tempString1 = (self.Gold as NSString).floatValue
            let b16 = ((self.j) * (tempString1) * (self.l)) + (self.o)
            let lastcalc = newyd * valuetype1 * Double(b16)
            costing_costvalue.text = clean2(String(lastcalc))
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
        }
        if glob == 6 && glob1 == 4{
            newyd = new * eighteen_gold6
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString2 = (self.Gold as NSString).floatValue
            let b18 = ((self.k) * (tempString2) * (self.l)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b18)
            self.costing_costvalue.text = clean2(String(lastvalue))
  
        }
        if glob == 6 && glob1 == 5{
            newyd = new * twenty_two_gold6
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 6 && glob1 == 6{
            newyd = new * fine_gold6
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 6 && glob1 == 7{
            newyd = new * platinum6
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString15 = (self.Platinum as NSString).floatValue
            let b14 = (tempString15) * (self.m)
            let lastvalue = newyd * valuetype1 * Double(b14)
            self.costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 6 && glob1 == 8{
            newyd = new * palladium6
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 7 && glob1 == 0{
            newyd = new * wax7
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 7 && glob1 == 1{
            newyd = new * sterling_silver7
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString3 = (self.Silver as NSString).floatValue
            let b21 = ((tempString3) * (self.n)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b21)
            costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 7 && glob1 == 2{
            newyd = new * nine_gold7
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 7 && glob1 == 3{
            newyd = new * fourteen_gold7
            result_lbl.text = clean(String(newyd))
            values = newyd
            let tempString1 = (self.Gold as NSString).floatValue
            let b16 = ((self.j) * (tempString1) * (self.l)) + (self.o)
            let lastcalc = newyd * valuetype1 * Double(b16)
            costing_costvalue.text = clean2(String(lastcalc))
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
        }
        if glob == 7 && glob1 == 4{
            newyd = new * eighteen_gold7
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString2 = (self.Gold as NSString).floatValue
            let b18 = ((self.k) * (tempString2) * (self.l)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b18)
            self.costing_costvalue.text = clean2(String(lastvalue))

        }
        if glob == 7 && glob1 == 5{
            newyd = new * twenty_two_gold7
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 7 && glob1 == 6{
           newyd = new * fine_gold7
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 7 && glob1 == 7{
            newyd = new * platinum7
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString15 = (self.Platinum as NSString).floatValue
            let b14 = (tempString15) * (self.m)
            let lastvalue = newyd * valuetype1 * Double(b14)
            self.costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 7 && glob1 == 8{
            newyd = new * palladium7
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 8 && glob1 == 0{
           newyd = new * wax8
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 8 && glob1 == 1{
            newyd = new * sterling_silver8
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString3 = (self.Silver as NSString).floatValue
            let b21 = ((tempString3) * (self.n)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b21)
            costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 8 && glob1 == 2{
            newyd = new * nine_gold8
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 8 && glob1 == 3{
            newyd = new * fourteen_gold8
            result_lbl.text = clean(String(newyd))
            values = newyd
            let tempString1 = (self.Gold as NSString).floatValue
            let b16 = ((self.j) * (tempString1) * (self.l)) + (self.o)
            let lastcalc = newyd * valuetype1 * Double(b16)
            costing_costvalue.text = clean2(String(lastcalc)) //String(b16)
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
        }
        if glob == 8 && glob1 == 4{
            newyd = new * eighteen_gold8
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString2 = (self.Gold as NSString).floatValue
            let b18 = ((self.k) * (tempString2) * (self.l)) + (self.o)
            let lastvalue = newyd * valuetype1 * Double(b18)
            self.costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 8 && glob1 == 5{
            newyd = new * twenty_two_gold8
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 8 && glob1 == 6{
            newyd = new * fine_gold8
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
        if glob == 8 && glob1 == 7{
            newyd = new * platinum8
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = false
            costing_costvalue.isHidden = false
            let tempString15 = (self.Platinum as NSString).floatValue
            let b14 = (tempString15) * (self.m)
            let lastvalue = newyd * valuetype1 * Double(b14)
            self.costing_costvalue.text = clean2(String(lastvalue))
        }
        if glob == 8 && glob1 == 8{
            newyd = new * palladium8
            result_lbl.text = clean(String(newyd))
            values = newyd
            costing_cost.isHidden = true
            costing_costvalue.isHidden = true
        }
    }
    @IBAction func fourteen_main_click(_ sender: UIButton) {
        if table_view_second.isHidden == true{
            table_view_second.isHidden = false
        }
        else{
            table_view_second.isHidden = true
        }
    }
    @IBAction func penny_btn_action(_ sender: UIButton) {
        if view_penny.isHidden == true{
        view_penny.isHidden = false
       }
        else{
            view_penny.isHidden = true
        }
    }
    @IBAction func wax_btn_action(_ sender: UIButton) {
        if table_view.isHidden == true{
             table_view.isHidden = false
        }
        else{
             table_view.isHidden = true
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.table_view.isHidden = true
        self.table_view_second.isHidden = true
        self.view_penny.isHidden = true
        signViewShow.isHidden = true
        signCompleteLogin.isHidden = true
    }
    
    @IBAction func penny_second_action(_ sender: UIButton) {
         penny_weight_btn.setTitle(" PennyWeight(S)", for:UIControl.State.normal)
        result_unit.text = "dwt"
        units = "dwt"
        let str = values
        valuetype1 = pennyweight
        result_lbl.text = clean(String(str))
        self.view_penny.isHidden = true
        valuecalculation()
    }
    
    
    @IBAction func ounce_action(_ sender: UIButton) {
        penny_weight_btn.setTitle(" Ounce,Troy(S)", for:UIControl.State.normal)
         result_unit.text = "T/oz"
        units = "T/oz"
        let str =  values
        valuetype1 = ounce
        result_lbl.text = clean(String(str))
        self.view_penny.isHidden = true
        valuecalculation()
    }
    
    
    @IBAction func kilogram_action(_ sender: UIButton) {
        penny_weight_btn.setTitle(" Kilogram(S)", for:UIControl.State.normal)
        result_unit.text = "kg"
        units = "kg"
        let str =  values
        valuetype1 = killogram
        result_lbl.text = clean(String(str))
        self.view_penny.isHidden = true
        valuecalculation()
     }
    
    @IBAction func gram_action(_ sender: UIButton) {
        penny_weight_btn.setTitle(" Gram(S)", for:UIControl.State.normal)
        result_unit.text = "gr"
        units = "gr"
        let str =  values
        valuetype1 = gram
        result_lbl.text = clean(String(str))
        self.view_penny.isHidden = true
        valuecalculation()
    }
    func Gold18(nyvalue: Double){
        let tempString1 = (self.Gold as NSString).floatValue
        let b16 = ((self.j) * (tempString1) * (self.l)) + (self.o)
        let lastcalc = nyvalue * valuetype1 * Double(b16)
        costing_costvalue.text = clean2(String(lastcalc))
        costing_cost.isHidden = false
        costing_costvalue.isHidden = false
    }
    func CallWebService(){
        guard Reachability.isConnectedToNetwork() == true else{
            let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
            return
        }
        let strUrl : String = "https://adarshtated.com/dev/jeweller_app/test_new_index.php?fun=get_prices"
        let url = strUrl
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request, completionHandler: {(Data,response,Error) -> Void in
            if Data != nil{
                let jsonobject : NSDictionary = try! JSONSerialization.jsonObject(with: Data!, options: []) as! NSDictionary
                DispatchQueue.main.async(execute:
                    {
                        let data = jsonobject.value(forKey: "metal_details") as! NSArray
                        self.lbl_date.text = jsonobject.value(forKey: "date_time") as? String
                        let data1 = data[0] as! NSDictionary
                        self.Gold = data1.value(forKey: "gold_pm") as! String
                        self.lbl_gold.text = self.clean2(self.Gold)
                        self.Silver = data1.value(forKey: "silver") as! String
                        self.lbl_silver.text = self.clean2(self.Silver)
                        self.Platinum = data1.value(forKey: "platinum_pm") as! String
                        self.lbl_Platinum.text =  self.clean2(self.Platinum)
                        UserDefaults.standard.set(self.Gold, forKey: "gold")
                        
                        let tempString3 = (self.Silver as NSString).floatValue
                        let b21 = ((tempString3) * (self.n)) + (self.o)
                        let changevalue = String(b21)
                        self.lbl_Cast_silver.text = self.clean2(changevalue)
                        
                        let tempString1 = (self.Gold as NSString).floatValue
                        let b16 = ((self.j) * (tempString1) * (self.l)) + (self.o)
                        let changevalue1 = String(b16)
                        self.lbl_14k.text = self.clean2(changevalue1)
                        
                        let tempString2 = (self.Gold as NSString).floatValue
                        let b18 = ((self.k) * (tempString2) * (self.l)) + (self.o)
                        let changevalue2 = String(b18)
                        self.lbl_18k.text = self.clean2(changevalue2)
                        
                        let tempString15 = (self.Platinum as NSString).floatValue
                        let b14 = (tempString15) * (self.m)
                        let changevalue3 = String(b14)
                        self.lbl_Cast_platinum.text = self.clean2(changevalue3)
                        
                        self.Activator.stopAnimating()
                        self.Activator.isHidden = true
                        self.firstCastingValue()
                })
            }
            else
            {
                let alert = UIAlertController(title: "Alert!", message: "Check Your Internet Connection", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                }
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        })
        task.resume()
    }
    func firstCastingValue(){
        let str = self.txt_digit.text!
        let new = (str as NSString).doubleValue
        let st = self.result_lbl.text!
        var newyd = (st as NSString).doubleValue
        newyd = new * self.fourteen_gold
        self.result_lbl.text = self.clean(String(newyd))
        let tempString1 = (self.Gold as NSString).floatValue
        let b16 = ((self.j) * (tempString1) * (self.l)) + (self.o)
        let lastcalc = newyd * self.valuetype1 * Double(b16)
        self.costing_costvalue.text = self.clean2(String(lastcalc))
        costing_costvalue.isHidden = false
        costing_cost.isHidden = false
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    

}
