//
//  ViewController.swift
//  Calculator
//
//  Created by Sachin Kesiraju on 6/3/14.
//  Copyright (c) 2014 Sachin Kesiraju. All rights reserved.
//
import UIKit
class ViewController12: UIViewController {
    @IBOutlet weak var labelOutput: UILabel!
    var runningNumber = ""
    var leftValue = ""
    var rightValue = ""
    var currentOperation = Operation.Empty
    var result = ""
    var equalPressed = false
    enum Operation: String {
        case Add = "+"
        case Subtract = "-"
        case Multiply = "*"
        case Divide = "/"
        case Empty = "Empty"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func buttonNumberPressed(sender: UIButton) {
        if equalPressed == true {
            runningNumber = ""
            equalPressed = false
        }        
        if runningNumber.contains(".") != true {
            runningNumber += "\(sender.currentTitle!)"
            labelOutput.text = runningNumber
        } else {
            runningNumber += "\(sender.currentTitle!)".replacingOccurrences(of: ".", with: "")
            labelOutput.text = runningNumber
        }
    }
    @IBAction func buttonAddPressed(_ sender: Any) {
        OperationProcess(operation: .Add)
    }
    @IBAction func buttonSubtractPressed(_ sender: Any) {
        OperationProcess(operation: .Subtract)
    }
    @IBAction func buttonMultiplyPressed(_ sender: Any) {
        OperationProcess(operation: .Multiply)
    }
    @IBAction func buttonDividePressed(_ sender: Any) {
        OperationProcess(operation: .Divide)
    }
    //Instant convert the current Number to Percentage
    @IBAction func buttonPercentPressed(_ sender: Any) {
        if runningNumber != "" {
            runningNumber = "\(Double(runningNumber)! / 100)"
            labelOutput.text = runningNumber
        }
    }
    //Instant convert the current Number to negative or positive
    @IBAction func buttonPlusMinusPressed(_ sender: Any) {
        if runningNumber != "" {
            runningNumber = "\(Double(runningNumber)! * -1)"
            labelOutput.text = runningNumber
        }
    }
    //Clear all the value
    @IBAction func buttonClearPressed(_ sender: Any) {
        labelOutput.text = ""
        leftValue = ""
        rightValue = ""
        currentOperation = Operation.Empty
        runningNumber = ""
        result = ""
    }
    @IBAction func buttonEqualPressed(_ sender: Any) {
        OperationProcess(operation: currentOperation)
        currentOperation = Operation.Empty
        runningNumber = result
        equalPressed = true
    }
    func OperationProcess(operation: Operation) {
        if currentOperation != Operation.Empty && runningNumber != "" {
            rightValue = runningNumber
            runningNumber = ""
            switch currentOperation {
            case .Add:
                result = "\(Double(leftValue)! + Double(rightValue)!)"
            case .Subtract:
                result = "\(Double(leftValue)! - Double(rightValue)!)"
            case .Multiply:
                result = "\(Double(leftValue)! * Double(rightValue)!)"
            case .Divide:
                result = Double(rightValue)! > 0 ? "\(Double(leftValue)! / Double(rightValue)!)" : "Divide by 0 ?"
            default:
                result = "Error!!!!!!!"
            }
            leftValue = result
            labelOutput.text = result
            currentOperation = operation
        } else {
            if runningNumber != "" {
                leftValue = runningNumber
                runningNumber = ""
                currentOperation = operation
            }
        }
    }
    @IBOutlet weak var btn_BAck: UIButton!
    @IBAction func btn_Back(_ sender: Any){
        self.dismiss(animated: true, completion: nil);
    }
}

