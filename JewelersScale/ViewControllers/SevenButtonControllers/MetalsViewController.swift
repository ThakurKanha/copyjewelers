//
//  MetalsViewController.swift
//  JewelersScale
//
//  Created by Apple on 28/05/18.
//  Copyright © 2018 MacOS High. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SwiftKeychainWrapper
class TableMetalCell:UITableViewCell{
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var metal: UILabel!
    override func awakeFromNib() {
        super .awakeFromNib()
        view1.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        view1.layer.borderWidth = 1
        view1.layer.cornerRadius = 3
    }
}
class MetalsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,GADBannerViewDelegate,GADInterstitialDelegate{
    var glob = Int()
    var array = NSMutableArray()
    var Gold = ""
    var Silver = ""
    var Platinum = ""
    var Gold_ask = ""
    var Silver_ask = ""
    var Platinum_ask = ""
    var currentdate = Date()
    var lastdate = String()
    var goldvalue = Double()
    var silvervalue = Double()
    var platinumvalue = Double()
    var gold_ask_value = Double()
    var silver_ask_value = Double()
    var platinum_ask_value = Double()
    var signViewShow = UIView()
    var signCompleteLogin = UIView()
    
    @IBOutlet weak var ounce_btn: UIButton!
    @IBOutlet weak var table_view: UITableView!
    
    @IBOutlet weak var Banner_view: GADBannerView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var ask_silver: UILabel!
    @IBOutlet weak var ask_platinum: UILabel!
    @IBOutlet weak var ask_gold: UILabel!
    @IBOutlet weak var third_label: UILabel!
    @IBOutlet weak var second_label: UILabel!
    @IBOutlet weak var first_label: UILabel!
    @IBOutlet weak var back_btn: UIButton!
    @IBOutlet weak var third_lbl: UILabel!
    @IBOutlet weak var second_lbl: UILabel!
    @IBOutlet weak var first_lbl: UILabel!
    @IBOutlet weak var refresh: UIButton!
    @IBOutlet weak var date_lbl: UILabel!
    @IBOutlet weak var platinum_lbl: UILabel!
    @IBOutlet weak var silver_lbl: UILabel!
    @IBOutlet weak var gold_lbl: UILabel!
    
    @IBAction func back_click(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func ounce_click(_ sender: Any) {
        if table_view.isHidden == true{
            table_view.isHidden = false
        }
        else{
            table_view.isHidden = true
        }
    }
    @IBAction func refresh_btn(_ sender: UIButton) {
        self.spinner.startAnimating()
         CallWebService()
    }
    func datavalue(){
        let styler = NumberFormatter()
        styler.maximumFractionDigits = 2
        styler.numberStyle = .currency
        
        let ounce : Double = 1.000000
        let kilo : Double = 0.031103
        let gram : Double = 31.103471
        let st = gold_lbl.text!
        var res = (st as NSString).doubleValue
        let st1 = silver_lbl.text!
        var res1 = (st1 as NSString).doubleValue
        let st2 = platinum_lbl.text!
        var res2 = (st2 as NSString).doubleValue
        
        let st11 = gold_lbl.text!
        var res11 = (st11 as NSString).doubleValue
        let st12 = silver_lbl.text!
        var res12 = (st12 as NSString).doubleValue
        let st13 = platinum_lbl.text!
        var res13 = (st13 as NSString).doubleValue
        
        if glob == 0{
            first_lbl.text = "/Ounce"
            second_lbl.text = "/Ounce"
            third_lbl.text = "/Ounce"
            first_label.text = "/Ounce"
            second_label.text = "/Ounce"
            third_label.text = "/Ounce"
            res = goldvalue / ounce
            gold_lbl.text = clean(String(res))
            
            let newdata = String(res)
            self.gold_lbl.text = self.clean(newdata)
            
            res1 = silvervalue / ounce
            silver_lbl.text = clean(String(res1))
            
            let newdata1 = String(res1)
            self.silver_lbl.text = self.clean(newdata1)
            
            res2 = platinumvalue / ounce
            platinum_lbl.text = clean(String(res2))
            
            let newdata2 = String(res2)
            self.platinum_lbl.text = self.clean(newdata2)
            
            
            
            res11 = gold_ask_value / ounce
            ask_gold.text = clean(String(res))
            
            let newdata3 = String(res11)
            self.ask_gold.text = self.clean(newdata3)
            
            res12 = silver_ask_value / ounce
            ask_silver.text = clean(String(res1))
            
            let newdata4 = String(res12)
            self.ask_silver.text = self.clean(newdata4)
            
            res13 = platinum_ask_value / ounce
            ask_platinum.text = clean(String(res2))
            
            let newdata5 = String(res13)
            self.ask_platinum.text = self.clean(newdata5)
            
        }
        if glob == 1{
            first_lbl.text = "/Gram"
            second_lbl.text = "/Gram"
            third_lbl.text = "/Gram"
            first_label.text = "/Gram"
            second_label.text = "/Gram"
            third_label.text = "/Gram"
            res = goldvalue / gram
            gold_lbl.text = clean(String(res))
            
            let newdata = String(res)
            self.gold_lbl.text = self.clean(newdata)
            
            res1 = silvervalue / gram
            silver_lbl.text = clean(String(res1))
            
            let newdata1 = String(res1)
            self.silver_lbl.text = self.clean(newdata1)
            
            res2 = platinumvalue / gram
            platinum_lbl.text = clean(String(res2))
            
            let newdata2 = String(res2)
            self.platinum_lbl.text = self.clean(newdata2)
            
            res11 = gold_ask_value / gram
            ask_gold.text = clean(String(res))
            
            let newdata3 = String(res11)
            self.ask_gold.text = self.clean(newdata3)
            
            res12 = silver_ask_value / gram
            ask_silver.text = clean(String(res1))
            let newdata4 = String(res12)
            self.ask_silver.text = self.clean(newdata4)
            
            res13 = platinum_ask_value / gram
            ask_platinum.text = clean(String(res2))
            
            let newdata5 = String(res13)
            self.ask_platinum.text = self.clean(newdata5)
        }
        if glob == 2{
            first_label.text = "/Kilo"
            second_label.text = "/Kilo"
            third_label.text = "/Kilo"
            first_lbl.text = "/Kilo"
            second_lbl.text = "/Kilo"
            third_lbl.text = "/Kilo"
            res = goldvalue / kilo
            gold_lbl.text = clean(String(res))
            
            let newdata = String(res)
            self.gold_lbl.text = self.clean(newdata)
            
            res1 = silvervalue / kilo
            silver_lbl.text = clean(String(res1))
            
            let newdata1 = String(res1)
            self.silver_lbl.text = self.clean(newdata1)
            
            res2 = platinumvalue / kilo
            platinum_lbl.text = clean(String(res2))
            
            let newdata2 = String(res2)
            self.platinum_lbl.text = self.clean(newdata2)
            
            
            res11 = gold_ask_value / kilo
            ask_gold.text = clean(String(res))
            
            let newdata3 = String(res11)
            self.ask_gold.text = self.clean(newdata3)
            
            res12 = silver_ask_value / kilo
            ask_silver.text = clean(String(res1))
            
            let newdata4 = String(res12)
            self.ask_silver.text = self.clean(newdata4)
            
            res13 = platinum_ask_value / kilo
            ask_platinum.text = clean(String(res2))
            
            let newdata5 = String(res13)
            self.ask_platinum.text = self.clean(newdata5)
        }
    }
    @IBAction func Menu_btn(_ sender: UIButton) {
        if let email = UserDefaults.standard.string(forKey: "login")  {
           self.signCompleteLogin.isHidden = false
        }else{
            self.signViewShow.isHidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.signViewShow = showViewLogIn()
        self.signCompleteLogin = showViewLogInComplete()
        self.signViewShow.isHidden = true
        self.signCompleteLogin.isHidden = true
        currentdate = Date.changeDaysBy(days: 0)
        let boolen =  UserDefaults.standard.bool(forKey: "BOOL")
        if  boolen == false{
            Banner_view.adUnitID = "ca-app-pub-6572892411343856/7270604337"
            Banner_view.rootViewController = self
            Banner_view.load(GADRequest())
        }
        ounce_btn.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        refresh.imageView?.contentMode = .scaleAspectFit
      
        CallWebService()
        table_view.isHidden = true
        table_view.delegate = self
        table_view.dataSource = self
        let padding: CGFloat = 8.0
        ounce_btn.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: padding, bottom: 0.0, right: -padding)
        ounce_btn.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: padding)
        array = ["Ounce","Gram","Kilo"]
        back_btn.imageView?.contentMode = .scaleAspectFit
        back_btn.setImage(UIImage(named: "back.png"), for: .normal)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.signCompleteLogin.isHidden = true
        self.signViewShow.isHidden = true
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "metalcell", for: indexPath) as! TableMetalCell
        cell.metal.text = array[indexPath.row] as? String
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let  selectedValue = (array[indexPath.row] as? String)!
        ounce_btn.setTitle(selectedValue, for: .normal)
        
        let ounce : Double = 1.000000
        let kilo : Double = 0.031103
        let gram : Double = 31.103471
        let st = gold_lbl.text!
        var res = (st as NSString).doubleValue
        let st1 = silver_lbl.text!
        var res1 = (st1 as NSString).doubleValue
        let st2 = platinum_lbl.text!
        var res2 = (st2 as NSString).doubleValue
        
        let st11 = gold_lbl.text!
        var res11 = (st11 as NSString).doubleValue
        let st12 = silver_lbl.text!
        var res12 = (st12 as NSString).doubleValue
        let st13 = platinum_lbl.text!
        var res13 = (st13 as NSString).doubleValue
        
        if indexPath.row == 0{
            glob = indexPath.row
            first_lbl.text = "/Ounce"
            second_lbl.text = "/Ounce"
            third_lbl.text = "/Ounce"
            first_label.text = "/Ounce"
            second_label.text = "/Ounce"
            third_label.text = "/Ounce"
            res = goldvalue / ounce
            gold_lbl.text = clean(String(res))
            let newdata = String(res)
            self.gold_lbl.text = self.clean(newdata)
            res1 = silvervalue / ounce
            silver_lbl.text = clean(String(res1))
            let newdata1 = String(res1)
            self.silver_lbl.text = self.clean(newdata1)
            res2 = platinumvalue / ounce
            platinum_lbl.text = clean(String(res2))
            let newdata2 = String(res2)
            self.platinum_lbl.text = self.clean(newdata2)
            res11 = gold_ask_value / ounce
            ask_gold.text = clean(String(res))
            let newdata11 = String(res11)
            self.ask_gold.text = self.clean(newdata11)
                
            res12 = silver_ask_value / ounce
            ask_silver.text = clean(String(res1))
            let newdata12 = String(res12)
            self.ask_silver.text = clean(String(newdata12))
            res13 = platinum_ask_value / ounce
            ask_platinum.text = clean(String(res2))
          
            let newdata13 = String(res13)
            self.ask_platinum.text = clean(String(newdata13))
            table_view.isHidden = true
        }
        if indexPath.row == 1{
            glob = indexPath.row
            first_lbl.text = "/Gram"
            second_lbl.text = "/Gram"
            third_lbl.text = "/Gram"
            first_label.text = "/Gram"
            second_label.text = "/Gram"
            third_label.text = "/Gram"
            res = goldvalue / gram
            gold_lbl.text = clean(String(res))
            let newdata = String(res)
            self.gold_lbl.text = clean(String(newdata))
                
            res1 = silvervalue / gram
            silver_lbl.text = clean(String(res1))
            let newdata1 = String(res1)
            self.silver_lbl.text = clean(String(newdata1))
               
            res2 = platinumvalue / gram
            platinum_lbl.text = clean(String(res2))
            let newdata2 = String(res2)
            self.platinum_lbl.text = clean(String(newdata2))
               
            res11 = gold_ask_value / gram
            ask_gold.text = clean(String(res))
            let newdata11 = String(res11)
            self.ask_gold.text = clean(String(newdata11))
                
            res12 = silver_ask_value / gram
            ask_silver.text = clean(String(res1))
            let newdata12 = String(res12)
            self.ask_silver.text = clean(String(newdata12))
               
            res13 = platinum_ask_value / gram
            ask_platinum.text = clean(String(res2))
            let newdata13 = String(res13)
            self.ask_platinum.text = clean(String(newdata13))
            table_view.isHidden = true
        }
        if indexPath.row == 2{
            glob = indexPath.row
            first_label.text = "/Kilo"
            second_label.text = "/Kilo"
            third_label.text = "/Kilo"
            first_lbl.text = "/Kilo"
            second_lbl.text = "/Kilo"
            third_lbl.text = "/Kilo"
            res = goldvalue / kilo
            gold_lbl.text = clean(String(res))
            let newdata8 = String(res)
            self.gold_lbl.text = clean(String(newdata8))
               
            res1 = silvervalue / kilo
            silver_lbl.text = clean(String(res1))
            let newdata9 = String(res1)
            self.silver_lbl.text = clean(String(newdata9))
                
            res2 = platinumvalue / kilo
            platinum_lbl.text = clean(String(res2))
            let newdata10 = String(res2)
            self.platinum_lbl.text = clean(String(newdata10))
             
            res11 = gold_ask_value / kilo
            ask_gold.text = clean(String(res))
            let newdata11 = String(res11)
            self.ask_gold.text = clean(String(newdata11))
              
            res12 = silver_ask_value / kilo
            ask_silver.text = clean(String(res1))
            let newdata12 = String(res12)
            self.ask_silver.text = clean(String(newdata12))
                
            res13 = platinum_ask_value / kilo
            ask_platinum.text = clean(String(res2))
            let newdata13 = String(res13)
            self.ask_platinum.text = clean(String(newdata13))
            table_view.isHidden = true
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 33
    }
    func clean(_ value: String?) -> String {
        guard value != nil else { return "$0.00" }
        let doubleValue = Double(value!) ?? 0.0
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.maximumFractionDigits = 2
        currencyFormatter.minimumFractionDigits = 2
        currencyFormatter.numberStyle = .decimal
        currencyFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        let priceString = currencyFormatter.string(from: NSNumber(value: doubleValue))!
        return "$"+priceString
    }
    func CallWebService(){
        guard Reachability.isConnectedToNetwork() == true else{
            let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                self.spinner.stopAnimating()
                self.spinner.isHidden = true
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
            return
        }//https://adarshtated.com/dev/jeweller_app/test_new_index.php?fun=get_prices
        let url = String(format:  "https://adarshtated.com/dev/jeweller_app/metal_prices.php?fun=get_prices")
        let webStringURL: String? = (url as String).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let url1 = URL(string: webStringURL!)
        var request = URLRequest(url: url1!)
        request.httpMethod = "POST"
        request.addValue("0", forHTTPHeaderField: "Content-Length")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            guard let data = data, error == nil else { return }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 { }
            let responseString = String(data: data, encoding: .utf8)
            var dictonary:NSDictionary?
            if let data = responseString?.data(using: String.Encoding.utf8) {
                DispatchQueue.main.async {
                do {
                    dictonary =  try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:AnyObject] as NSDictionary?
                    if let myDictionary = dictonary
                    {
                        let statusmessage1 = "(\(myDictionary["status"]!)"
                        let data = myDictionary.value(forKey: "metal_details") as! NSArray
                        let data1 = data[0] as! NSDictionary
                      
                        if (statusmessage1.contains("1")){
                            DispatchQueue.main.async {
                                self.Gold = data1.value(forKey: "gold_bidprice") as! String
                                self.Silver = data1.value(forKey: "silver_bidprice") as! String
                                self.Platinum = data1.value(forKey: "platinum_bidprice") as! String
                                self.Gold_ask = data1.value(forKey: "gold_askprice") as! String
                                self.Silver_ask = data1.value(forKey: "silver_askprice") as! String
                                self.Platinum_ask = data1.value(forKey: "platinum_askprice") as! String
                                
                                self.goldvalue = (self.Gold as NSString).doubleValue
                                self.silvervalue = (self.Silver as NSString).doubleValue
                                self.platinumvalue = (self.Platinum as NSString).doubleValue
                                self.gold_ask_value = (self.Gold_ask as NSString).doubleValue
                                self.silver_ask_value = (self.Silver_ask as NSString).doubleValue
                                self.platinum_ask_value = (self.Platinum_ask as NSString).doubleValue
                                let date = Date()
                                let formatter = DateFormatter()
                                formatter.dateFormat = "EE,dd-MMM-yyyy,  h:mm:ss a "
                                let result = formatter.string(from: date)
                                self.date_lbl.text = result
                                let styler = NumberFormatter()
                                styler.maximumFractionDigits = 2
                                styler.numberStyle = .currency
                                let converter = NumberFormatter()
                                  self.spinner.stopAnimating()
                                if let result = converter.number(from: String(self.goldvalue)) {
                                  self.gold_lbl.text = styler.string(from: result )
                                }
                                if let result = converter.number(from: String(self.silvervalue)) {
                                    self.silver_lbl.text = styler.string(from: result )
                                }
                                if let result = converter.number(from: String(self.platinumvalue)) {
                                    self.platinum_lbl.text = styler.string(from: result )
                                }
                                if let result = converter.number(from: String(self.gold_ask_value)) {
                                    self.ask_gold.text = styler.string(from: result )
                                }
                                if let result = converter.number(from: String(self.silver_ask_value)) {
                                    self.ask_silver.text = styler.string(from: result )
                                }
                                if let result = converter.number(from: String(self.platinum_ask_value)) {
                                    self.ask_platinum.text = styler.string(from: result )
                                }
                                self.datavalue()
                            }
                        } else {
                            
                            DispatchQueue.main.async {
                                let alertcontroller = UIAlertController(title: "Alert", message: "data not found" , preferredStyle: .alert)
                                alertcontroller.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                self.present(alertcontroller, animated: true, completion: nil)
                            }
                        }
                    }
                } catch let error as NSError {
                    print(error)
                }
                }
            }
        })
        task.resume()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()       
    }
}
