//
//  SettingStoneViewController.swift
//  JewelersScale
//
//  Created by abc on 3/8/19.
//  Copyright © 2019 MacOS High. All rights reserved.
//

import UIKit
class tableViewStoneCell:UITableViewCell{
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    
}


class SettingStoneViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    
    @IBOutlet weak var txtf_StoneName: UITextField!
    @IBOutlet weak var table_View: UITableView!
    var arrList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getStoneWebService()
    }
    @IBAction func Back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func addStonrs(_ sender: UIButton) {
        var dict = NSDictionary()
        if !(txtf_StoneName.text?.isEmpty)!{
            dict = ["stone_name":txtf_StoneName.text!,"stone_status":true]
            print(":dict:\(dict)")
          self.arrList.add(dict) //adding(dict)
            print("self.arrList answer:\(self.arrList)")
            self.insertJsonData()
        }else{
            self.alartviewShow(str: "Enter Stone Name")
        }
    }
    @IBAction func submit(_ sender: UIButton) {
        self.insertJsonData()
    }    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dict = self.arrList[indexPath.row] as? NSDictionary
        let status = dict?.value(forKey: "stone_status") as? Bool
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewStoneCell") as! tableViewStoneCell
        cell.deleteBtn.isHidden = true
        if indexPath.row > 13{
            cell.deleteBtn.isHidden = false
        }
         cell.btnCheck.setImage(UIImage(named: "empty_check_box.png"), for: .normal)
        if status == true{
          cell.btnCheck.setImage(UIImage(named: "check_box.png"), for: .normal)
        }
        let myNormalAttributedTitle = NSAttributedString(string: "\(dict?.value(forKey: "stone_name") as? String ?? "")",
            attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
        cell.btnCheck.setAttributedTitle(myNormalAttributedTitle, for: .normal)
        cell.btnCheck.tag = indexPath.row
        cell.deleteBtn.tag = indexPath.row
        cell.btnCheck.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        cell.deleteBtn.addTarget(self, action: #selector(deleteButtonAction), for: .touchUpInside)
        return cell
        
    }
    
    @objc func buttonAction(sender: UIButton!){
        print("btn tag :\(sender.tag)")
        if sender.currentImage == UIImage(named: "empty_check_box.png"){
            sender.setImage(UIImage(named: "check_box.png"), for: .normal)
            checkUpdate(tag: sender.tag, status: true)
        }else{
            checkUpdate(tag: sender.tag, status: false)
            sender.setImage(UIImage(named: "empty_check_box.png"), for: .normal)
        }
    }
     @objc func deleteButtonAction(sender: UIButton!){
        let alertController = UIAlertController(title: "", message: "Do you want delete", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            self.arrList.removeObject(at: sender.tag)
            self.insertJsonData()
        }
        let cancel = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancel)
        self.present(alertController, animated: true, completion:nil)
    }
    func checkUpdate(tag:Int,status:Bool){
        var addDict = NSDictionary()
        let dict = self.arrList[tag] as? NSDictionary
        let valname = dict?.value(forKey: "stone_name") as? String
        addDict = ["stone_name":(valname ?? ""),"stone_status":status]
        self.arrList.replaceObject(at: tag, with: addDict)
        print("self.arrList")
    }
    func insertJsonData(){
        self.view.endEditing(true)
        self.txtf_StoneName.text = ""
        var userid = String()
        if let res = UserDefaults.standard.value(forKey: "result"){
            userid = (res as AnyObject).value(forKey: "user_id") as! String
            print("userid:\(userid)")
        }
        let dict = ["stone_json":self.arrList]
        print("dict api:\(dict)")
        let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
        guard Reachability.isConnectedToNetwork() == true else{
            let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
            return
        }
        
        postWithImage(filename: "docfile", url_string: "https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/insertLedgerStone", parameters: ["user_id":"\(userid)"], imageParameterName:"doc_file" , selected_Image: jsonData as NSData, CompletionHandler: {json, error in
            print("json:\(json)")
            if json?.value(forKey: "success") as! String == "true"{
                self.getStoneWebService()
                self.alartviewShow(str: json?.value(forKey: "message") as! String)
            }else{
                self.alartviewShow(str: json?.value(forKey: "message") as! String)
            }
        })
    }
    func getStoneWebService(){
        var userid = String()
        if let res = UserDefaults.standard.value(forKey: "result"){
            userid = (res as AnyObject).value(forKey: "user_id") as! String
            print("userid:\(userid)")
        }
        DispatchQueue.main.async {
            guard Reachability.isConnectedToNetwork() == true else{
                let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                return
            }
        }
        postWithoutImage(url_string: "https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/getStoneData", parameters: "user_id=\(userid)", CompletionHandler: {json, error in
            print("josn:\(String(describing: json))")
            let status = json?.value(forKey: "success") as? String ?? ""
            guard status == "true" else { return}
            DispatchQueue.main.async {
                let result = json?.value(forKey: "result") as? NSDictionary
                self.arrList = result?.value(forKey: "stone_json") as! NSArray as! NSMutableArray
                print("self.arrList:\(self.arrList)")
                self.table_View.reloadData()
            }
        })
    }
}
