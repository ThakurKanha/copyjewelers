//
//  ScrapsettingViewController.swift
//  JewelersScale
//
//  Created by Virendra on 04/01/19.
//  Copyright © 2019 MacOS High. All rights reserved.
//

import UIKit

class ScrapsettingViewController: UIViewController {

    @IBOutlet weak var img_silver: UIImageView!
    @IBOutlet weak var eightkarat: UIImageView!
    @IBOutlet weak var tenkarat: UIImageView!
    @IBOutlet weak var twowelKarat: UIImageView!
    @IBOutlet weak var forthinKarat: UIImageView!
    @IBOutlet weak var sixthinKarat: UIImageView!
    @IBOutlet weak var ethinKarat: UIImageView!
    @IBOutlet weak var twintytwoKarat: UIImageView!
    @IBOutlet weak var twentyfourKarat: UIImageView!
    @IBOutlet weak var platinum: UIImageView!
    @IBOutlet weak var sppiner: UIActivityIndicatorView!
    
    var colorSelection = Int()
    var userid = ""
    var scraparr = [false,false,false,false,false,false,false,false,false,false]
    var checkImages: [UIImage] = []
    var dict = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        let imgname = UIImage(named: "empty_check_box.png")
        img_silver.image = imgname
        eightkarat.image = imgname
        tenkarat.image = imgname
        twowelKarat.image = imgname
        forthinKarat.image = imgname
        sixthinKarat.image = imgname
        ethinKarat.image = imgname
        twintytwoKarat.image = imgname
        twentyfourKarat.image = imgname
        platinum.image = imgname
        // Do any additional setup after loading the view.
        if let res = UserDefaults.standard.value(forKey: "result"){
            userid = (res as AnyObject).value(forKey: "user_id") as! String
            if let scrap_data = UserDefaults.standard.value(forKey: "scrapgold"){
                for i in 0..<(scrap_data as! NSArray).count{
                let flag = (scrap_data as! NSArray)[i] as? Bool
                if flag == true{
                    selectedimages(id: i)
                }
            }
          }
        }
        
    }
    @IBAction func Back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func Submit_btn(_ sender: UIButton) {
        callUpdateScrap(arr: scraparr as NSArray)
    }
    @IBAction func SelectScrap(_ sender: UIButton) {
        colorSelection = sender.tag
        selectedimages(id: sender.tag)
    }
    func alartview(str:String){
        let alert = UIAlertController(title: "", message: str, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    func selectedimages(id:Int){
        if id == 0{
            if img_silver.image == UIImage(named: "empty_check_box.png"){
            img_silver.image = UIImage(named: "check_box.png")
                scraparr[0] = true
        }else{
               img_silver.image = UIImage(named: "empty_check_box.png")
                scraparr[0] = false
            }
        }else if id == 1{
            if eightkarat.image == UIImage(named: "empty_check_box.png"){
                eightkarat.image = UIImage(named: "check_box.png")
                scraparr[1] = true
            }else{
                eightkarat.image = UIImage(named: "empty_check_box.png")
                scraparr[1] = false
            }
        }else if id == 2{
            if tenkarat.image == UIImage(named: "empty_check_box.png"){
                tenkarat.image = UIImage(named: "check_box.png")
                scraparr[2] = true
            }else{
                tenkarat.image = UIImage(named: "empty_check_box.png")
                scraparr[2] = false
            }
        }else if id == 3{
            if twowelKarat.image == UIImage(named: "empty_check_box.png"){
                twowelKarat.image = UIImage(named: "check_box.png")
                scraparr[3] = true
            }else{
                twowelKarat.image = UIImage(named: "empty_check_box.png")
                scraparr[3] = false
            }
        }else if id == 4{
            if forthinKarat.image == UIImage(named: "empty_check_box.png"){
                forthinKarat.image = UIImage(named: "check_box.png")
                scraparr[4] = true
            }else{
                forthinKarat.image = UIImage(named: "empty_check_box.png")
                scraparr[4] = false
            }
        }else if id == 5{
            if sixthinKarat.image == UIImage(named: "empty_check_box.png"){
                sixthinKarat.image = UIImage(named: "check_box.png")
                scraparr[5] = true
            }else{
                sixthinKarat.image = UIImage(named: "empty_check_box.png")
                scraparr[5] = false
            }
        }else if id == 6{
            if ethinKarat.image == UIImage(named: "empty_check_box.png"){
                ethinKarat.image = UIImage(named: "check_box.png")
                scraparr[6] = true
            }else{
                ethinKarat.image = UIImage(named: "empty_check_box.png")
                scraparr[6] = false
            }
        }else if id == 7{
            if twintytwoKarat.image == UIImage(named: "empty_check_box.png"){
                twintytwoKarat.image = UIImage(named: "check_box.png")
                scraparr[7] = true
            }else{
                twintytwoKarat.image = UIImage(named: "empty_check_box.png")
                scraparr[7] = false
            }
        }else if id == 8{
            if twentyfourKarat.image == UIImage(named: "empty_check_box.png"){
                twentyfourKarat.image = UIImage(named: "check_box.png")
                scraparr[8] = true
            }else{
                twentyfourKarat.image = UIImage(named: "empty_check_box.png")
                scraparr[8] = false
            }
        }else if id == 9{
            if platinum.image == UIImage(named: "empty_check_box.png"){
                platinum.image = UIImage(named: "check_box.png")
                scraparr[9] = true
            }else{
                platinum.image = UIImage(named: "empty_check_box.png")
                scraparr[9] = false
            }
        }
    }
    func callUpdateScrap(arr:NSArray){
        sppiner.isHidden = false
        sppiner.startAnimating()
        dict = ["ScrapJson":arr]
        let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        guard Reachability.isConnectedToNetwork() == true else{
            let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in             
                self.sppiner.isHidden = true
                self.sppiner.stopAnimating()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
            return
        }
        postWithoutImage(url_string: "https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/insertScrap", parameters: "user_id=\(userid)&scrap=\(jsonString)", CompletionHandler: {json, error in
        if json?.value(forKey: "success") as! String == "true"{
            DispatchQueue.main.async {
            self.sppiner.isHidden = true
        let result = json?.value(forKey: "result") as! NSDictionary
        let dict = convertToDictionary(text: result.value(forKey: "scrap") as! String)
        let scrapArr = dict?["ScrapJson"] ?? [] as! [Bool]
            for j in 0..<(scrapArr as! NSArray).count{
                let flag = (scrapArr as! NSArray)[j] as? Bool
                GSConst.goldFlag[j] = flag ?? false
            }
            UserDefaults.standard.set(scrapArr, forKey: "scrapgold")
            self.alartview(str: json?.value(forKey: "message") as! String)
            }
        }else{
            DispatchQueue.main.async {
            self.alartview(str: json?.value(forKey: "message") as! String)
            self.sppiner.isHidden = true
            self.sppiner.stopAnimating()
          }
        }
      })
    }
}
