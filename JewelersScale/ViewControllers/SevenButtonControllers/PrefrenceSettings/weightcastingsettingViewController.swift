//
//  weightcastingsettingViewController.swift
//  JewelersScale
//
//  Created by Virendra on 04/01/19.
//  Copyright © 2019 MacOS High. All rights reserved.
//

import UIKit

class weightcastingsettingViewController: UIViewController {

    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img5: UIImageView!
    @IBOutlet weak var img6: UIImageView!
    @IBOutlet weak var img7: UIImageView!
    @IBOutlet weak var sppiner: UIActivityIndicatorView!
    
    var weightarr = [false,false,false,false,false,false,false]
    var colorSelection = Int()
    var userid = ""
    var dict = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        let img = UIImage(named: "empty_check_box.png")
        img1.image = img
        img2.image = img
        img3.image = img
        img4.image = img
        img5.image = img
        img6.image = img
        img7.image = img
        if let res = UserDefaults.standard.value(forKey: "result"){
            userid = (res as AnyObject).value(forKey: "user_id") as! String
            if let scrap_data = UserDefaults.standard.value(forKey: "wcc"){
                for i in 0..<(scrap_data as! NSArray).count{
                    let flag = (scrap_data as! NSArray)[i] as? Bool
                    if flag == true{
                        selectedimages(tag: i)
                    }
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func Back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func Submit(_ sender: UIButton) {
        callUpdateScrap(arr: weightarr as NSArray)
    }
    @IBAction func Select_action(_ sender: UIButton) {
        selectedimages(tag: sender.tag)
    }
    func selectedimages(tag:Int){
        if tag == 0{
            if img1.image == UIImage(named: "empty_check_box.png"){
                img1.image = UIImage(named: "check_box.png")
                weightarr[0] = true
            }else{
                img1.image = UIImage(named: "empty_check_box.png")
                weightarr[0] = false
            }
        }else if tag == 1{
            if img2.image == UIImage(named: "empty_check_box.png"){
                img2.image = UIImage(named: "check_box.png")
                weightarr[1] = true
            }else{
                img2.image = UIImage(named: "empty_check_box.png")
                weightarr[1] = false
            }
        }else if tag == 2{
            if img3.image == UIImage(named: "empty_check_box.png"){
                img3.image = UIImage(named: "check_box.png")
                weightarr[2] = true
            }else{
                img3.image = UIImage(named: "empty_check_box.png")
                weightarr[2] = false
            }
        }else if tag == 3{
            if img4.image == UIImage(named: "empty_check_box.png"){
                img4.image = UIImage(named: "check_box.png")
                weightarr[3] = true
            }else{
                img4.image = UIImage(named: "empty_check_box.png")
                weightarr[3] = false
            }
        }else if tag == 4{
            if img5.image == UIImage(named: "empty_check_box.png"){
                img5.image = UIImage(named: "check_box.png")
                weightarr[4] = true
            }else{
                img5.image = UIImage(named: "empty_check_box.png")
                weightarr[4] = false
            }
        }else if tag == 5{
            if img7.image == UIImage(named: "empty_check_box.png"){
                img7.image = UIImage(named: "check_box.png")
                weightarr[5] = true
            }else{
                img7.image = UIImage(named: "empty_check_box.png")
                weightarr[5] = false
            }
        }else if tag == 6{
            if img6.image == UIImage(named: "empty_check_box.png"){
                img6.image = UIImage(named: "check_box.png")
                weightarr[6] = true
            }else{
                img6.image = UIImage(named: "empty_check_box.png")
                weightarr[6] = false
            }
        }
    }
    func callUpdateScrap(arr:NSArray){
        sppiner.isHidden = false
        sppiner.startAnimating()
        dict = ["wcc":arr]
        let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        guard Reachability.isConnectedToNetwork() == true else{
            let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                self.sppiner.isHidden = true
                self.sppiner.stopAnimating()
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
            return
        }//https://adarshtated.com/dev/jeweller_app/api/index.php/api/V2/
        postWithoutImage(url_string: "https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/insertWcc", parameters: "user_id=\(userid)&wcc=\(jsonString)", CompletionHandler: {json, error in
            if json?.value(forKey: "success") as! String == "true"{
                DispatchQueue.main.async {
                self.sppiner.isHidden = true
                self.sppiner.stopAnimating()
                let result = json?.value(forKey: "result") as! NSDictionary
                let dict = convertToDictionary(text: result.value(forKey: "wcc") as! String)
                let scrapArr = dict?["wcc"] ?? [] as! [Bool]
                UserDefaults.standard.set(scrapArr, forKey: "wcc")
                weighConst.weightFlag = scrapArr as! [Bool]
                for j in 0..<(scrapArr as! NSArray).count{
                    let flag = (scrapArr as! NSArray)[j] as? Bool
                    weighConst.weightFlag[j] = flag ?? false
                }
                self.sppiner.isHidden = true
                self.sppiner.stopAnimating()
                self.alartview(str: json?.value(forKey: "message") as! String)
                }
            }else{
                DispatchQueue.main.async {
                self.alartview(str: json?.value(forKey: "message") as! String)
                self.sppiner.isHidden = true
                self.sppiner.stopAnimating()
                }
            }
            
        })
    }
    func alartview(str:String){
        let alert = UIAlertController(title: "", message: str, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
}
