//
//  GoldScrapV2.swift
//  JewelersScale
//
//  Created by baps on 05/01/19.
//  Copyright © 2019 MacOS High. All rights reserved.
//

import UIKit
class GSConst {
    static var userid = ""
    static var goldFlag = [true,true,true,true,true,true,true,true,true,true];
    static var karatarr = ["Silver 925","8 karat Gold","10 karat Gold","12 karat Gold","14 karat Gold","16 karat Gold","18 karat Gold","22 karat Gold","24 karat Gold","Platinum 950"];
    static let goldVal = [1.0,0.333,0.417,0.500,0.583,0.667,0.750,0.917,1.0,1.0];
    static let units = [20.0,1.0,31.10350];
    static let unitname = [" Pennyweight(s)"," Ounce Troy(s)"," Gram(s)"];

}

class GoldScrapObj{
    var val = Float(1.0)
    var txtWeight = UITextField()
    var lblWeight = UILabel()
    var lblcost = UILabel()
    var lblline = UILabel()
    var viewFrame = UIView()
    
    init(size:CGRect,goldv2:GoldScrapV2,tag:Int,str:String) {
        viewFrame.frame = size
        var txtsize = CGFloat()
        var lblwsize = CGFloat()
        var lblwsize2 = CGFloat()
        if UI_USER_INTERFACE_IDIOM() == .pad{
            txtsize = 22.0
            lblwsize = 40
            lblwsize2 = 40
        }else{
            txtsize = 14.0
            lblwsize = 14
            lblwsize2 = 4
        }
        
        txtWeight.frame = CGRect(x: 4, y: 2, width: size.width/3.0-6, height: size.height*0.8)
        txtWeight.delegate = goldv2
        txtWeight.layer.borderWidth = 0.5
        txtWeight.layer.cornerRadius = 3
        txtWeight.tag = tag
        txtWeight.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        txtWeight.placeholder = " Enter Weight"
        txtWeight.backgroundColor = .white
        lblWeight.frame = CGRect(x: lblwsize+size.width/3.0, y: 0, width: size.width/3.0-6, height: size.height*0.8)
        lblWeight.text = str
        lblcost.frame = CGRect(x: lblwsize2+(size.width/3.0)*2, y: 0, width: size.width/3.0-6, height: size.height*0.8)
        lblcost.text = "$0.00"
        txtWeight.font = UIFont.systemFont(ofSize: txtsize)
        txtWeight.keyboardType = .decimalPad
        txtWeight.textAlignment = .center
        lblWeight.font = UIFont.boldSystemFont(ofSize: txtsize)
        lblcost.font = UIFont.boldSystemFont(ofSize: txtsize)
        lblline.frame = CGRect(x: 0, y: size.height-1, width: size.width, height: 1)
        lblline.backgroundColor = UIColor(displayP3Red: 163.0/255.0, green: 91.0/255.0, blue: 0, alpha: 1)
        viewFrame.backgroundColor = .clear
        viewFrame.addSubview(txtWeight)
        viewFrame.addSubview(lblWeight)
        viewFrame.addSubview(lblcost)
        viewFrame.addSubview(lblline)
    
    }
    
}

class GoldScrapV2: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    var arrList = NSMutableArray()
    
    var totalcost = UILabel()
    
    var textfield = UITextField()
    var countr = 0
    var signViewShow = UIView()
    var signCompleteLogin = UIView()
    
    @IBOutlet weak var txtSilverVal: UITextField!
    @IBOutlet weak var txtSilverPersent: UITextField!
    @IBOutlet weak var txtGoldVal: UITextField!
    @IBOutlet weak var txtGoldPersent: UITextField!
    @IBOutlet weak var txtPlatinumVal: UITextField!
    @IBOutlet weak var txtPlatinumPersent: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var goldView: UIView!
    @IBOutlet weak var gram_first_btn: UIButton!
    @IBOutlet weak var back_btn: UIButton!
    
    
  
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.signViewShow.isHidden = true
        self.signCompleteLogin.isHidden = true
        tableView.isHidden = true
         view.endEditing(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.signViewShow = showViewLogIn()
        self.signCompleteLogin = showViewLogInComplete()
        self.signViewShow.isHidden = true
        self.signCompleteLogin.isHidden = true
        
        tableView.isHidden = true
        
        updatefield()
        createuiscreens()
        CallWebService()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        print("didap")
        if let email = UserDefaults.standard.string(forKey: "login"){
            print("email:\(email)")
        }else{
            alartviewShow(str: "Valued Jewelers'Scale User,\n With our new updates, registration is now strongly suggested in order to manage user prefrences for Scrap Metal Calculator")
        }
    }
    func createuiscreens(){
        countr = 0
        for view in goldView.subviews{
            view.removeFromSuperview()
        }
        var txtposi = CGFloat()
        var txtsize = CGFloat()
        if UI_USER_INTERFACE_IDIOM() == .pad{
            txtsize = 22.0
            txtposi = 40
        }else{
            txtsize = 14.0
            txtposi = 4
        }
        let hieght = Int(goldView.frame.height / 12.0)
        let wid = view.frame.width
        for i in 0 ..< GSConst.karatarr.count{
            if GSConst.goldFlag[i] {
                let rect = CGRect(x: 0, y: countr*hieght, width: Int(wid) , height: hieght)
                let gold = GoldScrapObj(size: rect , goldv2:self,tag:i,str:GSConst.karatarr[i] )
                arrList.add(gold)
                goldView.addSubview(gold.viewFrame)
                countr = countr + 1
            }
        }
        let total = UILabel()
        let lbl_lastline = UILabel()
        let resetbtn = UIButton()
        total.frame = CGRect(x: 4, y: hieght*countr,width: Int(wid), height: hieght)
        totalcost.frame = CGRect(x: Int(txtposi+(wid/3.0)*2), y: hieght*countr,width: Int(wid), height: hieght)
        countr = countr+1
        lbl_lastline.frame = CGRect(x: 0, y: hieght*countr,width: Int(wid), height: 1)
        lbl_lastline.backgroundColor = UIColor(displayP3Red: 163.0/255.0, green: 91.0/255.0, blue: 0, alpha: 1)
        resetbtn.frame = CGRect(x: 10, y: hieght*countr+3,width: 80, height: hieght+5)
        resetbtn.setImage(UIImage(named: "Reset_form_btn.png"), for: .normal)
        resetbtn.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        resetbtn.addTarget(self, action: #selector(selectbtnChange), for: .touchUpInside)
      
        total.font = UIFont.boldSystemFont(ofSize: txtsize)
        totalcost.font = UIFont.boldSystemFont(ofSize: txtsize)
        total.text = "ESTIMATED SCRAP TOTAL: "
        totalcost.text = "$0.00 "
        goldView.addSubview(total)
        goldView.addSubview(totalcost)
        goldView.addSubview(lbl_lastline)
        goldView.addSubview(resetbtn)
   
    }
    @objc func selectbtnChange(){
        for val in arrList{
            let gsObj = val as! GoldScrapObj
            gsObj.txtWeight.text = ""
        }
        Update()
        CallWebService()
        txtSilverPersent.text = "89"
        txtGoldPersent.text = "98"
        txtPlatinumPersent.text = "94"
        gram_first_btn.setTitle("Pennyweight(s)", for: .normal)
        gram_first_btn.tag = 0
    }
    override func viewWillAppear(_ animated: Bool) {
        updatefield()
        
        createuiscreens()
    }
    func updatefield(){
        if let scrap_data = UserDefaults.standard.value(forKey: "scrapgold"){
            for j in 0..<(scrap_data as! NSArray).count{
                let flag = (scrap_data as! NSArray)[j] as? Bool
                GSConst.goldFlag[j] = flag ?? false
            }
        }
        
    }
    func Update() {
        var total = Double(0)
        for val in arrList{
            let gsObj = val as! GoldScrapObj
            var calVal = Double(0)
            
            if gsObj.txtWeight.tag == 0{
                let fistCal = ((Double(gsObj.txtWeight.text!) ?? 0.0)/GSConst.units[gram_first_btn.tag])
                let secondCal = (Double(txtSilverVal.text!) ?? 0.0) * ((Double(txtSilverPersent.text!) ?? 0.0 )/100.0)
                calVal  = fistCal * secondCal
                //((Double(gsObj.txtWeight.text!) ?? 0.0)/GSConst.units[gram_first_btn.tag])*(Double(txtSilverVal.text!) ?? 0.0) * ((Double(txtSilverPersent.text!) ?? 0.0 )/100.0)
            }else  if gsObj.txtWeight.tag == 9{
                let fistCal = ((Double(gsObj.txtWeight.text!) ?? 0.0)/GSConst.units[gram_first_btn.tag])
                let secondCal = (Double(txtPlatinumVal.text!) ?? 0.0) * ((Double(txtPlatinumPersent.text!) ?? 0.0 )/100.0)
                calVal = fistCal * secondCal
                //((Double(gsObj.txtWeight.text!) ?? 0.0)/GSConst.units[gram_first_btn.tag])*(Double(txtPlatinumVal.text!) ?? 0.0) * ((Double(txtPlatinumPersent.text!) ?? 0.0 )/100.0)
            }else{
                let valueText = txtGoldVal.text!.replacingOccurrences(of: ",", with: "")
                let fistCal = ((Double(gsObj.txtWeight.text!) ?? 0.0)/GSConst.units[gram_first_btn.tag])*(Double(valueText) ?? 0.0)
                let secondVal = ((Double(txtGoldPersent.text!) ?? 0.0 )/100.0)*GSConst.goldVal[gsObj.txtWeight.tag]
                calVal  = fistCal * secondVal
                //((Double(gsObj.txtWeight.text!) ?? 0.0)/GSConst.units[gram_first_btn.tag])*(Double(valueText) ?? 0.0) * ((Double(txtGoldPersent.text!) ?? 0.0 )/100.0)*GSConst.goldVal[gsObj.txtWeight.tag]
            }
            total += calVal
            gsObj.lblcost.text = cleanDollars(String(calVal))  //"$"+(NSString(format: "%.2f", calVal) as String)
        }
        totalcost.text = cleanDollars(String(total)) //"$"+(NSString(format: "%.2f", total) as String)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 101 || textField.tag == 102 || textField.tag == 103 {
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            let perVal = Double(newString as String) ?? 0.0
            
            if textField.tag == 101{
                if perVal > 92.5 {
                    textField.text = "89"
                    self.alartview(message: "% value can be 92.5% or less")
                }
            }
            if textField.tag == 102{
                if perVal > 100 {
                    textField.text = "98"
                    self.alartview(message: "% value can be 100% or less")
                }
            }
            if textField.tag == 103{
                if perVal > 95 {
                    textField.text = "94"
                    self.alartview(message: "% value can be 95% or less")
                }
            }
        }
        
        let myTimer = Timer(timeInterval: 0.01, target: self, selector: #selector(self.selectText1(_:)), userInfo: nil, repeats: false)
        RunLoop.current.add(myTimer, forMode: RunLoop.Mode.common)
        return true
    }
    @objc func selectText1(_ timer: Timer) {
        Update()
    }
    func CallWebService(){
        DispatchQueue.main.async {
            guard Reachability.isConnectedToNetwork() == true else{
                self.spinner.isHidden = true
                let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                return
            }
        }
        postWithoutImage(url_string: "https://adarshtated.com/dev/jeweller_app/test_new_index.php?fun=get_prices", parameters: "nil", CompletionHandler: {json, error in
            let status = json?.value(forKey: "status") as? Int ?? 0
            guard status == 1 else { return}
            DispatchQueue.main.async {
            let data = json?.value(forKey: "metal_details") as! NSArray
            let data1 = data[0] as! NSDictionary
            self.txtGoldVal.text = clean(data1.value(forKey: "gold_pm") as? String ?? "")
            self.txtSilverVal.text = clean(data1.value(forKey: "silver") as? String ?? "")
            self.txtPlatinumVal.text = clean(data1.value(forKey: "platinum_pm") as? String ?? "")
            self.spinner.isHidden = true
            self.spinner.stopAnimating()
            self.Update()
            }
        })
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GSConst.units.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "gold", for: indexPath) as! TableGold
        cell.cell_lbl.text = GSConst.unitname[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        gram_first_btn.tag = indexPath.row
        gram_first_btn.setTitle(GSConst.unitname[indexPath.row], for: .normal)
        Update()
        tableView.isHidden = true
    }
    @IBAction func gram_first_action(_ sender: UIButton) {
        if tableView.isHidden == true{
            tableView.isHidden = false
        }else{
            tableView.isHidden = true
        }
    }
    func alartview(message:String){
        let alertController1 = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        let cancelAction1 = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        alertController1.addAction(cancelAction1)
        self.present(alertController1, animated: true, completion: nil)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            textfield = textField
            let myTimer = Timer(timeInterval: 0.01, target: self, selector: #selector(self.selectText(_:)), userInfo: nil, repeats: false)
            RunLoop.current.add(myTimer, forMode: RunLoop.Mode.common)
            return true
    }
    @objc func selectText(_ timer: Timer) {
        textfield.becomeFirstResponder()
        textfield.selectedTextRange = textfield.textRange(from: textfield.beginningOfDocument, to: textfield.endOfDocument)
    }
    @IBAction func Menu_btn(_ sender: UIButton) {
        if let email = UserDefaults.standard.string(forKey: "login")  {
            self.signCompleteLogin.isHidden = false
        }else{
           self.signViewShow.isHidden = false
        }
    }
  
}
public func cleanDollars(_ value: String?) -> String {
    var str = clean(_:value)
    if !str.contains("."){
        str = str + ".00"
    }
    return "$"+str
}
public func clean(_ value: String?) -> String {
    guard value != nil else { return "$0.00" }
    let doubleValue = Double(value!) ?? 0.0
    let currencyFormatter = NumberFormatter()
    currencyFormatter.usesGroupingSeparator = true
    currencyFormatter.maximumFractionDigits = 2
    currencyFormatter.minimumFractionDigits = 2
    currencyFormatter.numberStyle = .decimal
    currencyFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
    //       // en_US_POSIX
    let priceString = currencyFormatter.string(from: NSNumber(value: doubleValue))!
    print("0-0-0-value-",priceString)
    return priceString
    
}
