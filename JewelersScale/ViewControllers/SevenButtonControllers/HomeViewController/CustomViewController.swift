//
//  CustomViewController.swift
//  JewelersScale
//
//  Created by Apple on 19/05/18.
//  Copyright © 2018 MacOS High. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SwiftKeychainWrapper
import StoreKit

@available(iOS 11.2, *)
extension SKProduct.PeriodUnit {
    func description(capitalizeFirstLetter: Bool = false, numberOfUnits: Int? = nil) -> String {
        let period:String = {
            switch self {
            case .day: return "day"
            case .week: return "week"
            case .month: return "month"
            case .year: return "year"
            }
        }()
        
        var numUnits = ""
        var plural = ""
        if let numberOfUnits = numberOfUnits {
            numUnits = "\(numberOfUnits) " // Add space for formatting
            plural = numberOfUnits > 1 ? "s" : ""
        }
        return "\(numUnits)\(capitalizeFirstLetter ? period.capitalized : period)\(plural)"
    }
}
struct gmailvalue {
    var name = ""
    var gmailid = ""
}

public func textfieldpedding(txtfield:UITextField){
    let  paddingview4 = UIView(frame: CGRect(x: 0, y: 0, width: 24, height: txtfield.frame.height))
    txtfield.leftView = paddingview4
    txtfield.leftViewMode = UITextField.ViewMode.always
}
var controllerobj = CustomViewController()
class CustomViewController: UIViewController,GADBannerViewDelegate, GADInterstitialDelegate, UITextViewDelegate {
    
    @IBOutlet weak var viewAnim: UIView!
    @IBOutlet weak var imgViewAnim: UIImageView!
    @IBOutlet weak var weight_btn: UIButton!
    @IBOutlet weak var btn: UIButton!
    @IBOutlet weak var jeweler_ledger: UIButton!
    @IBOutlet weak var precious_metal_btn: UIButton!
    @IBOutlet weak var casting_weight: UIButton!
    @IBOutlet weak var stl_btn: UIButton!
    @IBOutlet weak var weight_unit: UIButton!
    @IBOutlet weak var scrap_btn: UIButton!
    @IBOutlet weak var imgLogoView: UIImageView!
    @IBOutlet weak var menu_btn: UIButton!
    @IBOutlet weak var activityIdecator: UIActivityIndicatorView!
    @IBOutlet weak var restoreBtn: UIButton!
    @IBOutlet weak var popUPView: roundView!
    @IBOutlet weak var restoreBtnView: UIButton!
    @IBOutlet weak var popUpTextView: UITextView!
    
    var imgarr = NSMutableArray()
    var counter = 1
    var timer = Timer()
    var logoImages: [UIImage] = []
    var interstitial: GADInterstitial?
    var curentdate = Date()
    var lastdate = Date()
    var name = ""
    var signViewShow = UIView()
    var signCompleteLogin = UIView()
    var strRestore = String()
    func alartview(str:String){
        let alert = UIAlertController(title: "", message: str, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func weight_volume(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "volume") as! ViewController
        present(next,animated:false,completion: nil)
    }
    @IBAction func gold_scrap_action(_ sender: Any) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "GoldScrapV2") as! GoldScrapV2
        present(next,animated:false,completion: nil)
    }
    @IBAction func unit(_ sender: Any) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "weight_calculate") as! WeightCalculateViewController
        present(next,animated:false,completion: nil)
    }
    
    @IBAction func casting_weight_action(_ sender: Any) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "cast_weight") as! CastWeightViewController
        present(next,animated:false,completion: nil)
    }
    @IBAction func precious_click(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "metal") as! MetalsViewController
        present(next,animated:false,completion: nil)
    }
    @IBAction func stl_view_click(_ sender: UIButton) {
        if UserDefaults.standard.value(forKey: "popUP") == nil{
            self.popUPView.isHidden = false
        }
        let boolen =  UserDefaults.standard.bool(forKey: "BOOL")
        let dvc_token = UserDefaults.standard.string(forKey: "dvc_token") ?? ""
        if dvc_token != "" {
            print("dvc_token not ==  :\(dvc_token)")
            let next = self.storyboard?.instantiateViewController(withIdentifier: "ViewController1") as! ViewController1
            self.present(next, animated: true, completion: nil)
        }
        if   boolen == false{
            print("boolen  :\(boolen)")
            if let purchaseDate = UserDefaults.standard.string(forKey: "datedata"){
                print("purchaseDate :\(purchaseDate)")
                InAppManager.shared.tryCheckValidateReceiptAndUpdateExpirationDate()
                var pd = purchaseDate
                pd.removeLast()
                pd.removeLast()
                pd.removeLast()
                let date1 = NSDate(timeIntervalSince1970: Double(pd) ?? 0.0)
                let purchaseDate = Calendar.current.date(byAdding: .day, value: 7, to: date1 as Date)!
                let components = Set<Calendar.Component>([.day])
                let anchorComponents = Calendar.current.dateComponents(components, from: curentdate, to: purchaseDate).day!
                if curentdate > purchaseDate as Date{
                    UserDefaults.standard.set(true, forKey: "BOOL")
                    let next = self.storyboard?.instantiateViewController(withIdentifier: "ViewController1") as! ViewController1
                    self.present(next,animated:false,completion: nil)
                }else{
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "", message: "Your trial period is expiring in \(anchorComponents) days", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            let next = self.storyboard?.instantiateViewController(withIdentifier: "ViewController1") as! ViewController1
                            self.present(next,animated:false,completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }else{
                self.popUPView.isHidden = false
                //                DispatchQueue.main.async {
                //                    let alert = UIAlertController(title: "", message: "Please read our STL File Viewer & Sharing Subscription Terms before purchasing.\nStart 7 day Free trial for STL File Viewer & Sharing", preferredStyle: .alert)
                //                    alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                //                        let alert1 = UIAlertController(title: "", message: "Please read our STL File Viewer & Sharing Subscription Terms before purchasing.\nStart Subscription Type\n Annually : $39.99 \n Monthly : $4.99 ", preferredStyle: .alert)
                //                         alert1.addAction(UIAlertAction(title: "Annually", style: .default, handler: { action in
                //                          InAppManager.shared.purchaseProduct(productType:.weekly)
                //                         }))
                //                        alert1.addAction(UIAlertAction(title: "Monthly", style: .default, handler: { action in
                //                                InAppManager.shared.purchaseProduct(productType:.monthly)
                //
                //                            }))
                //                        alert1.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                //                        }))
                //                        self.present(alert1, animated: true, completion: nil)
                //                    }))
                //                    alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                //                    self.present(alert, animated: true, completion: nil)
                //                }
            }
        }else{
            print("boolen else  :\(boolen)")
            let next = self.storyboard?.instantiateViewController(withIdentifier: "ViewController1") as! ViewController1
            self.present(next,animated:false,completion: nil)
        }
        //        }else{
        ////            alartview(str: "Please Sign In")
        //            View_sign.isHidden = true
        //            View_sign2.isHidden = true
        //            let next = self.storyboard?.instantiateViewController(withIdentifier: "ViewController1") as! ViewController1
        //            self.present(next,animated:false,completion: nil)
        //        }
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print("url")
        UIApplication.shared.open(URL, options: [:])
        return false
    }
    
    func sevenDayTrial(){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "", message: "Start 7 day Free trial for STL File Viewer & Sharing", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                let alert1 = UIAlertController(title: "", message: "Start Subscription Type\n Annually : $39.99 \n Monthly : $4.99 ", preferredStyle: .alert)
                alert1.addAction(UIAlertAction(title: "Annually", style: .default, handler: { action in
                    InAppManager.shared.purchaseProduct(productType:.weekly)
                }))
                alert1.addAction(UIAlertAction(title: "Monthly", style: .default, handler: { action in
                    InAppManager.shared.purchaseProduct(productType:.monthly)
                    
                }))
                alert1.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                }))
                self.present(alert1, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func Menu_btn(_ sender: UIButton) {
        if UserDefaults.standard.string(forKey: "login") != nil  {
            self.signCompleteLogin.isHidden = false
        }else{
            self.signViewShow.isHidden = false
        }
    }
    
    @IBAction func cancelPopUP(_ sender: UIButton) {
        self.popUPView.isHidden = true
    }
    
    @IBAction func restore(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Do you want to restore your purchase?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
            self.activityIdecator.isHidden = false
            self.popUPView.isHidden = true
            self.strRestore = "strRestore"
            self.activityIdecator.startAnimating()
            InAppManager.shared.restoreSubscription()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func convertToDictionary(from text: String) throws -> [String: String] {
        guard let data = text.data(using: .utf8) else { return [:] }
        let anyResult: Any = try JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? [String: String] ?? [:]
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        popUpTextView.delegate = true as? UITextViewDelegate
        self.signViewShow = showViewLogIn()
        self.signCompleteLogin = showViewLogInComplete()
        self.signViewShow.isHidden = true
        self.signCompleteLogin.isHidden = true
        self.popUPView.isHidden = true
        activityIdecator.isHidden = true
        
        controllerobj = self
        navigationController?.navigationBar.isHidden = true
        curentdate =  Date.changeDaysBy(days: 0)
        orientationChangedNSNotification()
        InAppManager.shared.loadProducts()
        if UserDefaults.standard.string(forKey: "datedata") != nil{
            //            InAppManager.shared.updateSubscriptionStatus()
            InAppManager.shared.startMonitoring()
            InAppManager.shared.tryCheckValidateReceiptAndUpdateExpirationDate()
        }
        let ischeck = UserDefaults.standard.bool(forKey: "CheckAnim")
        viewAnim.isHidden = !ischeck
        if ischeck {
            menu_btn.isHidden = true
            restoreBtn.isHidden = true
            for i in 0..<31{
                logoImages.append((UIImage(named: "logo\(i).jpg"))!)
                let img = UIImage(named: "logo\(i).jpg")
                imgarr.add(img!)
            }
            self.imgViewAnim.animationImages = imgarr as? [UIImage];
            self.imgViewAnim.animationDuration = 3.5
            self.imgViewAnim.animationRepeatCount = 1
            self.imgViewAnim.startAnimating()
            perform(#selector(doSomeAnimation), with: nil, afterDelay: self.imgViewAnim.animationDuration)
        }
        UserDefaults.standard.set(false, forKey: "CheckAnim")
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.value(forKey: "restore") != nil{
            restoreBtn.isHidden = true
        }
        self.popUPView.isHidden = true
    }
    @objc func doSomeAnimation() {
        self.viewAnim.isHidden = true
        let boolen =  UserDefaults.standard.bool(forKey: "BOOL")
        if boolen == false{
            interstitial = createAndLoadInterstitial()
        }
        menu_btn.isHidden = false
        restoreBtn.isHidden = false
        if UserDefaults.standard.value(forKey: "restore") != nil{
            restoreBtn.isHidden = true
        }
    }
    
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial!) {
        ad.present(fromRootViewController: self)
    }
    func interstitialDidFail(toPresentScreen ad: GADInterstitial!) {
        
    }
    private func createAndLoadInterstitial() -> GADInterstitial? {
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-6572892411343856/9513624290")
        guard let interstitial = interstitial else {
            return nil
        }
        let request = GADRequest()
        request.testDevices = [ kGADSimulatorID ]
        interstitial.load(request)
        interstitial.delegate = self
        return interstitial
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        signViewShow.isHidden = true
        signCompleteLogin.isHidden = true
    }
    @IBAction func jeweler_ledger_click(_ sender: UIButton) {
        //        if UserDefaults.standard.string(forKey: "login") != nil {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "LaserViewController") as! LaserViewController
        present(next,animated:false,completion: nil)
        //    }else{
        //            alartview(str: "Please Sign In")
        //            View_sign.isHidden = true
        //            View_sign2.isHidden = true
        //       }
    }
    func orientationChangedNSNotification() {        
        if UI_USER_INTERFACE_IDIOM() == .pad
        {
            let incX: Float = 0.50
            let incY: Float = 0.74
            var rect: CGRect = weight_btn.frame
            rect.origin.x = (self.view.frame.size.width - rect.size.width)*0.5
            
            rect.origin.y -= 100
            rect.size.width = rect.size.width
            rect.size.height = rect.size.width * (191 / 167.0)
            weight_btn.frame = rect
            rect.origin.x -= rect.size.width * CGFloat(incX)
            rect.origin.y += rect.size.height * CGFloat(incY)
            scrap_btn.frame = rect
            rect.origin.x += (rect.size.width-2) * CGFloat(incX) * 2
            weight_unit.frame = rect
            rect.origin.x -= (rect.size.width) * CGFloat(incX  )
            rect.origin.y += rect.size.height * CGFloat(incY)
            casting_weight.frame = rect
            rect.origin.x += (rect.size.width-3) * CGFloat(incX) * 2
            precious_metal_btn.frame = rect
            
            rect.origin.x -= (rect.size.width-2) * CGFloat(incX )
            rect.origin.y += (rect.size.height) * CGFloat(incY )
            stl_btn.frame = rect
            rect.origin.x -= (rect.size.width-2) * CGFloat(incX) * 2
            jeweler_ledger.frame = rect
            rect.origin.x -= (rect.size.width-5) * CGFloat(incX)
            rect.origin.y += rect.size.height * CGFloat(incY-20) / 26
            btn.frame = rect
            
            
        }else{
            let incX: Float = 0.50
            let incY: Float = 0.74
            var rect: CGRect = weight_btn.frame
            rect.origin.x = (self.view.frame.size.width - rect.size.width)*0.5
            rect.origin.y -= 20
            rect.size.width = rect.size.width*1.05
            rect.size.height = rect.size.width * (191 / 167.0)
            weight_btn.frame = rect
            rect.origin.x -= (rect.size.width) * CGFloat(incX)
            rect.origin.y += rect.size.height * CGFloat(incY)
            scrap_btn.frame = rect
            rect.origin.x += (rect.size.width-2) * CGFloat(incX) * 2
            weight_unit.frame = rect
            
            
            rect.origin.x -= (rect.size.width) * CGFloat(incX)
            rect.origin.y += rect.size.height * CGFloat(incY)
            casting_weight.frame = rect
            
            
            
            rect.origin.x += (rect.size.width-2) * CGFloat(incX) * 2
            precious_metal_btn.frame = rect
            rect.origin.x -= rect.size.width * CGFloat(incX)
            rect.origin.y += rect.size.height * CGFloat(incY)
            stl_btn.frame = rect
            rect.origin.x -= (rect.size.width-2) * CGFloat(incX) * 2
            jeweler_ledger.frame = rect
            rect.origin.x -= (rect.size.width-2) * CGFloat(incX)
            rect.origin.y += rect.size.height * CGFloat(incY-20) / 26
            btn.frame = rect
            
        }
        btn.imageView?.contentMode = .scaleAspectFit
        jeweler_ledger.imageView?.contentMode = .scaleAspectFit
        weight_unit.imageView?.contentMode = .scaleAspectFit
        precious_metal_btn.imageView?.contentMode = .scaleAspectFit
        casting_weight.imageView?.contentMode = .scaleAspectFit
        stl_btn.imageView?.contentMode = .scaleAspectFit
        scrap_btn.imageView?.contentMode = .scaleAspectFit
        weight_btn.imageView?.contentMode = .scaleAspectFit
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func DonePopUPBtn(_ sender: UIButton) {
        self.popUPView.isHidden = true
        self.sevenDayTrial()
    }
    
    
}
extension Date {
    static func changeDaysBy(days : Int) -> Date {
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.day = days
        return Calendar.current.date(byAdding: dateComponents, to: currentDate)!
    }
}

enum ProductType: String {
    case weekly = "Jewelers_scale"
    case monthly = "Jewelers_Scale1"
    static var all: [ProductType] {
        return [.weekly,.monthly]
    }
}

enum InAppErrors: Swift.Error {
    case noSubscriptionPurchased
    case noProductsAvailable
    var localizedDescription: String {
        switch self {
        case .noSubscriptionPurchased:
            return "No subscription purchased"
        case .noProductsAvailable:
            return "No products available"
        }
    }
}

protocol InAppManagerDelegate: class {
    func inAppLoadingStarted()
    func inAppLoadingSucceded(productType: ProductType)
    func inAppLoadingFailed(error: Swift.Error?)
    func subscriptionStatusUpdated(value: Bool)
}

class InAppManager: NSObject {
    static let shared = InAppManager()
    weak var delegate: InAppManagerDelegate?
    var products: [SKProduct] = []
    var isTrialPurchased: Bool?
    var expirationDate: Date?
    var purchasedProduct: ProductType?
    var isSubscriptionAvailable: Bool = true
    {
        didSet(value) {
            self.delegate?.subscriptionStatusUpdated(value: value)
        }
    }
    func startMonitoring() {
        SKPaymentQueue.default().add(self)
        self.tryCheckValidateReceiptAndUpdateExpirationDate()
    }
    
    func stopMonitoring() {
        SKPaymentQueue.default().remove(self)
    }
    
    func loadProducts() {
        print("loadProducts")
        let productIdentifiers = Set<String>(ProductType.all.map({$0.rawValue}))
        let request = SKProductsRequest(productIdentifiers: productIdentifiers)
        request.delegate = self
        request.start()
    }
    
    func purchaseProduct(productType: ProductType) {
        
        guard let product = self.products.filter({$0.productIdentifier == productType.rawValue}).first else {
            print("product:)")
            self.delegate?.inAppLoadingFailed(error: InAppErrors.noProductsAvailable)
            return
        }
        print("purchaseProduct")
        let payment = SKMutablePayment(product: product)
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(payment)
        SKPaymentQueue.default().restoreCompletedTransactions()
//        InAppManager.shared.tryCheckValidateReceiptAndUpdateExpirationDate()
    }
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        print("paymentQueueRestoreCompletedTransactionsFinished")
//        purchaseStatusBlock?(.restored)
    }
    
    func restoreSubscription() {
        print("restoreSubscription")
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
        self.delegate?.inAppLoadingStarted()
        //        UserDefaults.standard.set("restore", forKey: "restore")
    }
    
    func tryCheckValidateReceiptAndUpdateExpirationDate() {
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
            print("^A receipt found. Validating it...")             // We will allow user to use all premium features until
            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                let receiptString = receiptData.base64EncodedString(options: [])
                let dict = ["receipt-data" : receiptString, "password" : "b154431a94104e48866f265ce868cf71"] as [String : Any]
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                    if let storeURL = Foundation.URL(string:"https://buy.itunes.apple.com/verifyReceipt"),
                        let sandboxURL = Foundation.URL(string: "https://sandbox.itunes.apple.com/verifyReceipt") {
                        var request = URLRequest(url: storeURL)
                        request.httpMethod = "POST"
                        request.httpBody = jsonData
                        let session = URLSession(configuration: URLSessionConfiguration.default)
                        NSLog("^Connecting to production...")
                        let task = session.dataTask(with: request) { data, response, error in
                            if let receivedData = data, let httpResponse = response as? HTTPURLResponse,
                                error == nil, httpResponse.statusCode == 200 {
                                print("^Received 200, verifying data...")
                                do {
                                    if let jsonResponse = try JSONSerialization.jsonObject(with: receivedData, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>,
                                        let status = jsonResponse["status"] as? Int64 {
                                        switch status {
                                        case 0:
                                            let receiptInfo: NSArray = (jsonResponse["latest_receipt_info"] as? NSArray ?? [])
                                            let lastReceipt = receiptInfo.lastObject as? NSDictionary
                                            let datedata = lastReceipt?["expires_date_ms"]
                                            DispatchQueue.main.async {
                                                controllerobj.activityIdecator.isHidden = true
                                                controllerobj.activityIdecator.stopAnimating()
                                            }
                                            if datedata == nil{return}
                                            print("datedata :\(String(describing: datedata))")
                                            UserDefaults.standard.set(datedata, forKey: "datedata")
                                            UserDefaults.standard.set("abc", forKey: "popUP")
                                            DispatchQueue.main.async {
                                                if controllerobj.strRestore == "strRestore"{
                                                    controllerobj.strRestore = ""
                                                    controllerobj.alartview(str: "Restore successfully")
                                                }
                                                controllerobj.restoreBtn.isHidden = true
                                            }
                                        case 21007:
                                            print("^need to repeat evrything with Sandbox")
                                            var request = URLRequest(url: sandboxURL)
                                            request.httpMethod = "POST"
                                            request.httpBody = jsonData
                                            let session = URLSession(configuration: URLSessionConfiguration.default)
                                            print("^Connecting to Sandbox...")
                                            let task = session.dataTask(with: request) { data, response, error in
                                                if let receivedData = data, let httpResponse = response as? HTTPURLResponse,
                                                    error == nil, httpResponse.statusCode == 200 {
                                                    print("^Received 200, verifying data...")
                                                    do {
                                                        if let jsonResponse = try JSONSerialization.jsonObject(with: receivedData, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>,
                                                            let status = jsonResponse["status"] as? Int64 {
                                                            switch status {
                                                            case 0:
                                                                let receiptInfo: NSArray = (jsonResponse["latest_receipt_info"] as? NSArray ?? [])
                                                                let lastReceipt = receiptInfo.lastObject as? NSDictionary
                                                                let datedata = lastReceipt?["expires_date_ms"]
                                                                DispatchQueue.main.async {
                                                                    controllerobj.activityIdecator.isHidden = true
                                                                    controllerobj.activityIdecator.stopAnimating()
                                                                }
                                                                if datedata == nil{return}
                                                                UserDefaults.standard.set(datedata, forKey: "datedata")
                                                                UserDefaults.standard.set("abc", forKey: "popUP")
                                                                DispatchQueue.main.async {
                                                                    if controllerobj.strRestore == "strRestore"{
                                                                        controllerobj.strRestore = ""
                                                                        controllerobj.alartview(str: "Restore successfully")
                                                                    }
                                                                    controllerobj.restoreBtn.isHidden = true
                                                                    //                                                                    controllerobj.alartview(str: "Restore successfully")
                                                                }
                                                            //
                                                            default:
                                                                print("^ succesfull---")
                                                            }
                                                        } else {
                                                            
                                                            controllerobj.alartview(str: "Failed to cast serialized JSON to Dictionary<String, AnyObject>") }
                                                    }
                                                    catch { controllerobj.alartview(str: "Couldn't serialize JSON with error: " + error.localizedDescription)
                                                        
                                                    }
                                                } else { //self.handleNetworkError(data: data, response: response, error: error)
                                                    
                                                }
                                            }
                                            // END of closure #2 = verification with Sandbox
                                            task.resume()
                                            //                                            controllerobj.alartview(str: "\(status)")
                                        //                                        default: self.showAlertWithErrorCode(errorCode: status)
                                        default:
                                            print("default0123")
                                        }
                                    } else {  controllerobj.alartview(str:"Failed to cast serialized JSON to Dictionary<String, AnyObject>")
                                        
                                    }
                                }
                                catch {controllerobj.alartview(str: "Couldn't serialize JSON with error: " + error.localizedDescription)
                                    
                                }
                            } else { //self.handleNetworkError(data: data, response: response, error: error)
                                
                            }
                        }
                        task.resume()
                    } else {  controllerobj.alartview(str:"Couldn't convert string into URL. Check for special characters.") }
                }
                catch {  controllerobj.alartview(str:"Couldn't create JSON with error: " + error.localizedDescription) }
            }
            catch {  controllerobj.alartview(str:"Couldn't read receipt data with error: " + error.localizedDescription) }
            DispatchQueue.main.async {
                controllerobj.activityIdecator.isHidden = true
                controllerobj.activityIdecator.stopAnimating()
            }
        } else {
//            controllerobj.alartview(str:"No receipt found even though there is an indication something has been purchased before")
                        print("^No receipt found. Need to refresh receipt.")
            //            self.refreshReceipt()
        }
        DispatchQueue.main.async {
            controllerobj.activityIdecator.isHidden = true
            controllerobj.activityIdecator.stopAnimating()
        }
    }
    
    
    
    
    //    func checkSubscriptionAvailability(_ completionHandler: @escaping (Bool) -> Void) {
    //        let SUBSCRIPTION_SECRET = "b154431a94104e48866f265ce868cf71"//"b7d64bdc064e4a40865bf1d3508e4aa2"
    //        let receiptPath = Bundle.main.appStoreReceiptURL?.path
    //        if FileManager.default.fileExists(atPath: receiptPath!){
    //            var receiptData:NSData?
    //            do{
    //                receiptData = try NSData(contentsOf: Bundle.main.appStoreReceiptURL!, options: NSData.ReadingOptions.alwaysMapped)
    //            }catch{}
    //            let base64encodedReceipt = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn)
    //            let requestDictionary = ["receipt-data":base64encodedReceipt!,"password":SUBSCRIPTION_SECRET]
    //            guard JSONSerialization.isValidJSONObject(requestDictionary) else {return }
    //            do {
    //                let requestData = try JSONSerialization.data(withJSONObject: requestDictionary)
    //                let validationURLString = "https://sandbox.itunes.apple.com/verifyReceipt"  // this works but as noted above it's best to use your own trusted server
    //                //https://buy.itunes.apple.com/verifyReceipt"
    //                guard let validationURL = URL(string: validationURLString) else {
    //                     controllerobj.alartview(str: "the validation url could not be created, unlikely error")
    //                    return }
    //                let session = URLSession(configuration: URLSessionConfiguration.default)
    //                var request = URLRequest(url: validationURL)
    //                request.httpMethod = "POST"
    //                request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
    //                let task = session.uploadTask(with: request, from: requestData) { (data, response, error) in
    //                    if let data = data , error == nil {
    //                        DispatchQueue.main.async {
    //                        do {
    //                            //let appReceiptJSON = try JSONSerialization.jsonObject(with: data)
    //                            let appReceiptJSON = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? NSDictionary
    //                            let receiptInfo: NSArray = (appReceiptJSON!["latest_receipt_info"] as? NSArray ?? [])
    //                            let lastReceipt = receiptInfo.lastObject as? NSDictionary
    //                            let datedata = lastReceipt?["expires_date_ms"]
    //                            controllerobj.activityIdecator.isHidden = true
    //                            controllerobj.activityIdecator.stopAnimating()
    //                            if datedata == nil{
    //                                controllerobj.alartview(str: "You have no restore ")
    //                                return
    //                            }
    //                            print("datedata :\(String(describing: datedata))")
    ////                            let statusCheck = lastReceipt["is_trial_period"] as! String
    ////                            let product_id = lastReceipt.value(forKey: "product_id")
    //
    //                            UserDefaults.standard.set(datedata, forKey: "datedata")
    //                            UserDefaults.standard.set("abc", forKey: "popUP")
    //                            DispatchQueue.main.async {
    ////                                controllerobj.alartview(str: "Success restore")
    //                                controllerobj.restoreBtn.isHidden = true
    //
    //                            }
    //                        } catch let error as NSError {
    //                             controllerobj.alartview(str: "\(error)")
    //                            print("json serialization failed with error: \(error)")
    //                         }
    //                        }
    //                     } else {
    //                        controllerobj.alartview(str: "\(String(describing: error))")
    //                        print("the upload task returned an error: \(error!)")
    //                    }
    //                }
    //                task.resume()
    //            } catch let error as NSError {
    //                controllerobj.alartview(str: "json serialization failed with error: \(error)")
    //                print("json serialization failed with error: \(error)")
    //            }
    //        }
    //    }
    //    func updateSubscriptionStatus() {
    //        self.checkSubscriptionAvailability({ [weak self] (isSubscribed) in
    //            self?.isSubscriptionAvailable = isSubscribed
    //        })
    //    }
    
    
    
    
    
}


extension InAppManager: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        //        self.updateSubscriptionStatus()
        print("updatedTransactions")
        for transaction in transactions {
            guard let productType = ProductType(rawValue: transaction.payment.productIdentifier) else {
                print("fatalError")
                fatalError()
            }
            print("productType:",productType)
            switch transaction.transactionState {
            case .purchasing:
                controllerobj.activityIdecator.isHidden = false
                controllerobj.activityIdecator.startAnimating()
                self.delegate?.inAppLoadingStarted()
                print("purchasing")
            case .purchased:
                controllerobj.activityIdecator.isHidden = true
                controllerobj.activityIdecator.stopAnimating()
                SKPaymentQueue.default().finishTransaction(transaction)
                //                self.updateSubscriptionStatus()
                tryCheckValidateReceiptAndUpdateExpirationDate()
                self.isSubscriptionAvailable = true
                self.delegate?.inAppLoadingSucceded(productType: productType)
                //                 InAppManager.shared.updateSubscriptionStatus()
                tryCheckValidateReceiptAndUpdateExpirationDate()
                print("paymentQueue purchased")
                UserDefaults.standard.set("restore", forKey: "restore")
                UserDefaults.standard.set("abc", forKey: "popUP")
                let next = controllerobj.storyboard?.instantiateViewController(withIdentifier: "ViewController1") as! ViewController1
                controllerobj.present(next, animated: true, completion: nil)
            case .failed:
                controllerobj.activityIdecator.isHidden = true
                controllerobj.activityIdecator.stopAnimating()
                if let transactionError = transaction.error as NSError?,
                    transactionError.code != SKError.paymentCancelled.rawValue {
                    self.delegate?.inAppLoadingFailed(error: transaction.error)
                } else {
                    self.delegate?.inAppLoadingFailed(error: InAppErrors.noSubscriptionPurchased)
                }
                SKPaymentQueue.default().finishTransaction(transaction)
                print("failed")
            case .restored:
                SKPaymentQueue.default().finishTransaction(transaction)
                UserDefaults.standard.set("restore", forKey: "restore")
                //                self.updateSubscriptionStatus()
                tryCheckValidateReceiptAndUpdateExpirationDate()
                self.isSubscriptionAvailable = true
                self.delegate?.inAppLoadingSucceded(productType: productType)
                UserDefaults.standard.set("abc", forKey: "popUP")
                print("restored")
            case .deferred:
                self.delegate?.inAppLoadingSucceded(productType: productType)
                print("deferred")
            }
        }
    }
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Swift.Error) {
        print("paymentQueue inAppLoadingFailed")
        controllerobj.activityIdecator.isHidden = true
        controllerobj.alartview(str: "\(error)")
        self.delegate?.inAppLoadingFailed(error: error)
    }
}
extension InAppManager: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        guard response.products.count > 0 else {return}
        self.products = response.products
        for product in self.products{
            print("products: \(product.productIdentifier)")
            let numberFormatter = NumberFormatter()
            numberFormatter.formatterBehavior = .behavior10_4
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = product.priceLocale
            _ = numberFormatter.string(from: product.price)
        }
    }
}


