//
//  LaserViewController.swift
//  JewelersScale
//
//  Created by baps on 16/06/18.
//  Copyright © 2018 MacOS High. All rights reserved.
//

import UIKit
import MessageUI
import iOSDropDown

class DropdownView: NSObject{
    static var sharedInstance = DropdownView()
    private let view0 = UIScrollView()
    private var arrayBtns = [UIButton]()
    private func setUpView(){
        removeView()
        view0.backgroundColor = UIColor.orange
//        view0.frame = CGRect(x: 60, y: 0, width: 100, height: 100)
    }
    private func setButtons(){
        arrayBtns.removeAll()
        let dropdownValues = objLaserViewController.dropDownArr
        var xpos = 0
        for i in 0..<dropdownValues.count{
            let btn = UIButton(frame: CGRect(x: 0, y: xpos, width: 100, height: 30))
            btn.titleLabel?.adjustsFontSizeToFitWidth = true
            btn.tag = i
            btn.titleLabel?.minimumScaleFactor = 0.1
            btn.titleLabel?.numberOfLines = 1
            btn.clipsToBounds = true
            btn.setTitle(dropdownValues[i], for: .normal)
            btn.setTitleColor(UIColor.black, for: .normal)
            xpos += 30
//            self.view0.addSubview(btn)
            self.arrayBtns.append(btn)
        }
    }
    
    func showView(){
        setUpView()
        setButtons()
        DispatchQueue.main.async {
            for i in 0..<self.arrayBtns.count{
                self.view0.addSubview(self.arrayBtns[i])
                self.arrayBtns[i].addTarget(self, action: #selector(self.clickOnPopButton), for: .touchUpInside)
            }
            let cellview = objLaserViewController.tableView.rectForRow(at: IndexPath(row: 0, section: btnClicked.tag))
            print("cellview: \(cellview)")
            self.view0.frame = CGRect(x: cellview.minX+5, y: cellview.maxY-2, width: btnClicked.frame.width, height: 100)
            self.view0.contentSize = CGSize(width: 100, height: 30 * self.arrayBtns.count)
            self.view0.layer.cornerRadius = 5
            self.view0.layer.masksToBounds = true
            objLaserViewController.tableView.addSubview(self.view0)
        }
    }
    func removeView(){
        DispatchQueue.main.async {
            print("count: \(self.view0.subviews.count)")
            for view in self.view0.subviews {
                view.removeFromSuperview()
            }
            self.view0.removeFromSuperview()
            print("number of subviews in dropdown:",self.view0.subviews.count)
        }
    }
    @objc func clickOnPopButton(sender:UIButton){
        print("clickOnPopButton",sender.tag)
        print("clickOnPopButton",sender.titleLabel?.text ?? "")
        btnClicked.titleLabel?.text = sender.titleLabel!.text
        //objLaserViewController.arrStonesCount.insert(sender.titleLabel!.text!, at: objLaserViewController.selected_index)
        let arrValue = objLaserViewController.arrStonesCount
        arrValue[objLaserViewController.cont ?? 0] = "\(sender.titleLabel!.text!)"
        print("objLaserViewController.arrStonesCount:\(objLaserViewController.arrStonesCount)")
        self.removeView()
        
    }
}

class LaserValues: NSObject {
    var quntity = String("")
    var rate = String("1.00")
    
}

class JewelerCellPDF: UITableViewCell {
    
    @IBOutlet weak var lblPdf: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
}
class LaserCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var txtQuntity: UITextField!
    @IBOutlet weak var txtRate: UITextField!
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var txtTitle: UITextField!
    
    @IBOutlet weak var txtLine: UILabel!
    @IBOutlet weak var txtX: UILabel!
    
    @IBOutlet weak var txtEqual: UILabel!
    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var btnAddStone: UIButton!
    
    override func awakeFromNib() {
        super .awakeFromNib()
        txtTitle.autocapitalizationType = .allCharacters
        txtQuntity.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        txtQuntity.layer.borderWidth = 1
        txtQuntity.layer.cornerRadius = 3
        
        txtRate.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        txtRate.layer.borderWidth = 1
        txtRate.layer.cornerRadius = 3
        
        //        cellView.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        //        cellView.layer.borderWidth = 1
        
    }
}
var objLaserViewController: LaserViewController!
var btnClicked: UIButton!
class LaserViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate,UISearchBarDelegate, UISearchDisplayDelegate {
    
    let colorSeparator =  UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1)
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var btnOpen: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnOpen1: UIButton!
    @IBOutlet weak var btnSave2: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var lbl_filename: UILabel!
    @IBOutlet weak var txtGTotal: UILabel!
    @IBOutlet weak var lblGtotal: UILabel!
    @IBOutlet weak var btnCalc: UIButton!
    @IBOutlet weak var btnMail: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    var tagged = Int()
    var dropDownArr = ["Option 1 and two how", "Option 2", "Option 3","Option 1", "Option 2", "Option 3","Option 1", "Option 2", "Option 3"]
    
    var lasertitle = NSMutableArray()
    var laserArray = NSMutableArray()
    var lng1 = Int(10000)
    var lng2 = Int(10000)
    var textfield:UITextField!
    var txtGrandTotal = UILabel()
    var txtGrandLable = UILabel()
    var noOfPdf: [String]!
    var selPfd = Int(-1)
    var isMailSend = Bool(false)
    var tatalCasting = String("")
    var rownotedit = 12
    var fileName = ""
    var fileDate = ""
    var userid = String()
    var result = NSMutableArray()
    var currentUserArray = [String]()
    var signViewShow = UIView()
    var signCompleteLogin = UIView()
    var isflagShowingView = false
    var arrStonesCount = NSMutableArray()
    @objc func clickOnPopButton(sender:UIButton) {
        print("clickOnPopButton",sender.tag)
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true);
        DropdownView.sharedInstance.removeView()
        
    }
    @IBAction func Menu_btn(_ sender: UIButton) {
        if let email = UserDefaults.standard.string(forKey: "login")  {
            self.signCompleteLogin.isHidden = false
        }else{
            self.signViewShow.isHidden = false
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.signViewShow.isHidden = true
        self.signCompleteLogin.isHidden = true
        self.view.endEditing(true)
        self.searchBar.endEditing(true)
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let searchText = searchText.lowercased()
        guard !searchText.isEmpty  else {
            result = result0.mutableCopy() as! NSMutableArray
            tableViewList.reloadData()
            return }
        currentUserArray.removeAll()
        for i in 0..<result0.count{
            let dict = result0[i] as! NSDictionary
            let name = dict.value(forKey: "mTableName") as! String
            currentUserArray.insert(name, at: i)
        }
        print("currentUserArray :\(currentUserArray)")
        let filterdata = searchText.isEmpty ? currentUserArray : currentUserArray.filter { $0.localizedCaseInsensitiveContains(searchText) }
        result = []
        print("filterdata :\(filterdata)")
        for j in 0..<filterdata.count{
            let filtername = filterdata[j]
            for i in 0..<result0.count{
                let dict1 = result0[i] as! NSDictionary
                let name = dict1.value(forKey: "mTableName") as! String
                if name == filtername{
                    print("name :\(name)")
                    result.add(dict1)
                    tableViewList.reloadData()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.signViewShow = showViewLogIn()
        self.signCompleteLogin = showViewLogInComplete()
        self.signViewShow.isHidden = true
        self.signCompleteLogin.isHidden = true
        objLaserViewController = self
        searchBar.isHidden = true
        let y = searchBar.frame.maxY
        tableViewList.frame = CGRect(x: 0, y: y, width: view.frame.width, height: view.frame.height)
        if UI_USER_INTERFACE_IDIOM() == .pad {
            searchBar.frame = CGRect(x: 0, y: y+25, width: view.frame.width, height: 56)
            let ySize = searchBar.frame.maxY
            tableViewList.frame = CGRect(x: 0, y: ySize, width: view.frame.width, height: view.frame.height)
        }
        searchBar.placeholder = "Search file"
        if let res = UserDefaults.standard.value(forKey: "result"){
            userid = (res as AnyObject).value(forKey: "user_id") as! String
        }
        if let email = UserDefaults.standard.string(forKey: "login")  {
            openLaserFile()
        }
        lbl_filename.isHidden = true
        lasertitle = ["CAD DESIGN(S)", "WAX DESIGN(S)", "3D PRINT(S)", "MOLD(S)", "CASTING(S)", "MOUNTING(S)", "FINDING(S)", "JEWELER", "STONE(S)","SETTER","POLISHING","FINISHING","ENGRAVING","MISC (click to edit)",""];
        self.arrStonesCount = ["Diamond", "Diamond", "Diamond", "Diamond", "Diamond", "Diamond", "Diamond", "Diamond", "Diamond","Diamond","Diamond","Diamond","Diamond","Diamond",""];
        for i in 0..<lasertitle.count{
            let valse = NSMutableArray()
            if i < lasertitle.count - 1{
                let laserVal = LaserValues()
                if i == 4{
                    laserVal.quntity = tatalCasting
                }
                valse.add(laserVal)
            }
            laserArray.add(valse)
        }
        txtGrandTotal.text = "$0.00"
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        tap.delegate = self
        tableView.separatorStyle = .none
        tableViewList.separatorColor = colorSeparator;
        tableViewList.isHidden = true
        let tapGesture = UITapGestureRecognizer(target: self, action: Selector(("hideKeyboard")))
        tapGesture.cancelsTouchesInView = true
        tapGesture.delegate = self
        tableViewList.addGestureRecognizer(tapGesture)
        setGradientBackground(btnSave)
        setGradientBackground(btnReset)
        setGradientBackground(btnOpen)
        setGradientBackground(btnOpen1)
        setGradientBackground(btnSave2)
        selPfd = -1
    }
    override func viewDidAppear(_ animated: Bool) {
        print("didap")
        if let email = UserDefaults.standard.string(forKey: "login"){
            print("email:\(email)")
        }else{
            alartviewShow(str: "Valued Jewelers'Scale User,\n With our new updates, registration is now required in order to manage user prefrences and provide access to saved ladger files from other iOS devices")
        }
    }
    func hideKeyboard() {
        print("touch")
        
        self.searchBar.endEditing(true)
        tableViewList.endEditing(true)
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        self.searchBar.endEditing(true)
    }
    func setGradientBackground(_ uibtton:UIButton) {
        let colorTop =  UIColor(red: 230.0/255.0, green: 180.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        let colorMidile = UIColor(red: 255.0/255.0, green: 229.0/255.0, blue: 106.0/255.0, alpha: 1.0).cgColor
        let colorBottom =  UIColor(red: 230.0/255.0, green: 180.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorMidile,colorBottom]
        gradientLayer.locations = [0.0,0.5, 1.0]
        gradientLayer.frame = uibtton.bounds
        
        uibtton.layer.insertSublayer(gradientLayer, at: 0)
        uibtton.layer.borderWidth = 1
        uibtton.layer.borderColor =  UIColor(red: 200.0/255.0, green: 150.0/255.0, blue: 1.0/255.0, alpha: 1.0).cgColor
    }
    @IBAction func Back_action(_ sender: Any) {
        if(self.tableViewList.isHidden == false){
            btnSave2.isHidden = false
            btnOpen1.isHidden = false
            searchBar.isHidden = true
            view.endEditing(true)
            UIView.animate(withDuration: 0.3, animations: {
                self.tableViewList.alpha = 0
            }, completion:  {
                (value: Bool) in
                self.tableViewList.isHidden = true
            })
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func Remove(name:String)  {
        guard Reachability.isConnectedToNetwork() == true else{
            let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
            return
        }
        postWithoutImage(url_string: "https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/deleteJsonFile", parameters: "user_id=\(userid)&doc_file=\(name).doc", CompletionHandler: {json, error in
            if json?.value(forKey: "success") as! String == "true"{
                DispatchQueue.main.async {
                    self.alartview(message: json?.value(forKey: "message") as! String)
                    self.tableViewList.reloadData()
                }
            }else{
                DispatchQueue.main.async {
                    self.alartview(message: json?.value(forKey: "message") as! String)
                }
            }
        })
    }
    func dismissKeyboard() {
        self.view.endEditing(true);
    }
    func Open(_ no: Int) {
        print("result:\(result)")
        let valtit = result[no] as! NSDictionary
        let mvalue = valtit.value(forKey: "mMain1") as! NSArray
        lbl_filename.text = valtit.value(forKey: "mTableName") as? String
        lasertitle.removeAllObjects()
        for val in mvalue{
            let vald = val as! NSDictionary
            let name = vald.value(forKey: "Name")
            lasertitle.add(name as! String)
        }
        laserArray.removeAllObjects()
        for val in mvalue{
            let vald = val as! NSDictionary
            let valse = NSMutableArray()
            let quntity = (vald.value(forKey: "Quantity") ?? []) as! NSArray
            let rate = vald.value(forKey: "Prize") as! NSArray
            for j in 0..<rate.count{
                let lsrow  = LaserValues()
                lsrow.quntity = rate[j] as! String
                lsrow.rate = quntity[j] as! String
                valse.add(lsrow)
            }
            laserArray.add(valse)
        }
        lasertitle.add("")
        let valse1 = NSMutableArray()
        laserArray.add(valse1)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 1))
        view.backgroundColor = colorSeparator
        if(section == lasertitle.count - 1){
            var rect = txtGTotal.frame
            rect.origin.y = 10
            txtGrandTotal.frame = rect
            txtGrandTotal.text = GrandTotal()
            txtGrandTotal.textAlignment = .right
            txtGrandTotal.font = txtGTotal.font
            txtGTotal.text = txtGrandTotal.text
            rect.size.width = lblGtotal.frame.size.width * 1.4
            rect.origin.x -= rect.size.width + 5
            txtGrandLable.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
            if UI_USER_INTERFACE_IDIOM() == .pad{
                rect.origin.x -= 5
                txtGrandLable.font = txtGTotal.font
            }
            rect.origin.y = 10
            txtGrandLable.frame = rect
            txtGrandLable.text = "Grand Total  = "
            txtGrandLable.textAlignment = .right
            let viewchilde = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 1))
            view.backgroundColor = UIColor.clear
            viewchilde.backgroundColor = colorSeparator
            view.addSubview(viewchilde)
            view.addSubview(txtGrandLable)
            view.addSubview(txtGrandTotal)
        }
        return view
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableViewList == tableView{
            return 0
        }else{
            if(section == lasertitle.count - 1 ){
                return 135
            }
            return 1
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableViewList == tableView{
            return 1
        }else{
            return lasertitle.count
        }        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewList == tableView{
            return result.count
        }else{
            let valse = laserArray.object(at: section) as! NSMutableArray
            if(valse.count <= 1){
                return valse.count
            }
            return valse.count+1
        }
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableViewList == tableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "JewelerCellPDF") as! JewelerCellPDF
            if result.count != 0{
                let dict = result[indexPath.row] as! NSDictionary
                let name = dict["mTableName"] as! String
                let date = dict["mTableName2"] as! String
                cell.lblPdf.text = "\(name) \(date)"
                cell.btnRemove.addTarget(self, action: #selector(OnClickRemove(_:)), for: .touchUpInside)
                cell.btnRemove.tag = indexPath.row
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LaserCell") as! LaserCell
            cell.btnPlus.addTarget(self, action: #selector(OnClickPlus), for: .touchUpInside)
            cell.btnPlus.tag = indexPath.section*lng1 + indexPath.row
            cell.btnDropdown.isHidden = true
            let valse = laserArray.object(at: indexPath.section) as! NSMutableArray
            cell.txtQuntity.isHidden = indexPath.row == valse.count
            cell.btnAddStone.isHidden = true
            cell.btnDropdown.setTitle(arrStonesCount[indexPath.section] as? String, for: .normal)//= "Diamond value drop"
//            print("arrStonesCount:\(arrStonesCount[indexPath.row])")
            cell.btnAddStone.tag = indexPath.section
            cell.btnDropdown.titleLabel?.adjustsFontSizeToFitWidth = true
            cell.btnDropdown.titleLabel?.minimumScaleFactor = 0.1
            cell.btnDropdown.titleLabel?.numberOfLines = 1
            cell.btnDropdown.clipsToBounds = true
            cell.btnDropdown.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
            if(indexPath.row == 0){
                if indexPath.section == 8{
                    
                    cell.btnPlus.setImage(UIImage(named: "multiply.png"), for: .normal)
                }
                cell.btnPlus.setImage(UIImage(named: "plus.png"), for: .normal)
            }else{
                cell.btnPlus.setImage(UIImage(named: "multiply.png"), for: .normal)
                if indexPath.section == 8{
//                    cell.btnDropdown.isHidden = indexPath.row == valse.count
                    cell.btnPlus.setImage(UIImage(named: "multiply.png"), for: .normal)
                }
            }
            cell.btnDropdown.tag = indexPath.section
            cell.btnDropdown.addTarget(self, action: #selector(addDropdown), for: .touchUpInside)
            //            print("row - count ",indexPath.row, valse.count)
            cell.txtRate.isHidden = indexPath.row == valse.count
            cell.txtX.isHidden = indexPath.row == valse.count
            cell.btnPlus.isHidden = indexPath.row == valse.count
            cell.txtLine.isHidden = indexPath.row < valse.count
            cell.txtEqual.isHidden = indexPath.row == valse.count
            cell.txtTitle.text = indexPath.row == 0 ? lasertitle[indexPath.section] as? String : ""
            
            if cell.txtTitle.text == "STONE(S)"{
//                for i in 0..<arrStonesCount.count{
                print("arrStonesCount:\(arrStonesCount)")
//                cell.btnDropdown.setTitle(arrStonesCount[indexPath.section-8] as? String, for: .normal)
//                }
                if indexPath.section == 8{
                cell.txtTitle.frame = CGRect(x: 4, y: 0, width: 100, height: 20)
                }
                cell.btnDropdown.isHidden = false
                cell.btnAddStone.isHidden = false
                cell.btnAddStone.addTarget(self, action: #selector(OnClickPlus1), for: .touchUpInside)
                print("ravi",arrStonesCount.count)
                if arrStonesCount.count != 0{
                    
                    if indexPath.section == 8{
                    
                    cell.txtTitle.isHidden = false
                    cell.btnAddStone.isHidden = false
                    }else{
                    cell.txtTitle.isHidden = true
                    cell.btnAddStone.isHidden = true
                    }
                  
                }
                
                
            }else{
                cell.txtTitle.isHidden = false
            }
           
            if indexPath.row < valse.count{
                let lsrow  = valse.object(at: indexPath.row) as! LaserValues
                cell.txtQuntity.text = lsrow.quntity
                cell.txtQuntity.delegate = self
                cell.txtQuntity.tag = indexPath.section*lng1+(lng1*lng2) + indexPath.row
                
                cell.txtRate.text = lsrow.rate
                cell.txtRate.delegate = self
                cell.txtRate.tag = indexPath.section*lng1+(lng1*2*lng2) + indexPath.row
                
                cell.lblResult.text =  valuecalculate(lsrow.quntity, rate: lsrow.rate)
                cell.txtTitle.tag = -1
                if indexPath.section > rownotedit && indexPath.row == 0 {
                    cell.txtTitle.tag = -indexPath.section
                }
                cell.txtTitle.delegate = self                
            }else{
                var grandtotal = Double(0.00)
                for val in valse {
                    let lsrow = val  as! LaserValues
                    let new = (lsrow.quntity.replacingOccurrences(of: ",", with: "") as NSString).doubleValue
                    let new2 = (lsrow.rate.replacingOccurrences(of: ",", with: "") as NSString).doubleValue
                    grandtotal = grandtotal + (new * new2)
                }
                cell.lblResult.text = clean("\(grandtotal)")
            }
            return cell
        }
    }
    
    @objc func addDropdown(_ sender: UIButton){
        print("addDropdown")
        btnClicked = sender
        cont = sender.tag
        DropdownView.sharedInstance.showView()
    }
    
    var selected_index = 0
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        dismissKeyboard()
        if tableViewList == tableView{
            Open(indexPath.row)
            searchBar.isHidden = true
            selected_index = indexPath.row
            lbl_filename.isHidden = false
            selPfd = indexPath.row
            tableViewList.isHidden = true
            btnOpen1.isHidden = false
            btnSave2.isHidden = false
            self.searchBar.text = ""
            let myTimer = Timer(timeInterval: 0.01, target: self, selector: #selector(self.timerCallBack(_:)), userInfo: nil, repeats: false)
            RunLoop.current.add(myTimer, forMode: RunLoop.Mode.common)
        }
    }
   
    func GrandTotal() -> String {
        var grandtotal = Double(0.00)
        for laser in laserArray{
            let valse = laser as! NSMutableArray
            for val in valse {
                let lsrow = val  as! LaserValues
                let new = (lsrow.quntity.replacingOccurrences(of: ",", with: "") as NSString).doubleValue
                let new2 = (lsrow.rate.replacingOccurrences(of: ",", with: "") as NSString).doubleValue
                grandtotal = grandtotal + (new * new2)
            }
        }
        return clean("\(grandtotal)")
    }
    var cont = Int()
    var plaseCount = Int()
    @IBAction func OnClickPlus1(_ sender: UIButton) {
    print("sender.ta",sender.tag)
    cont = sender.tag
    plaseCount = plaseCount + 1
    //arrStonesCount.add("Diamond")s
    print("arrStonesCoucountncountt:\(arrStonesCount.count)",arrStonesCount.count)
    arrStonesCount.insert("Diamond", at: (sender.tag + plaseCount))
    print("arrStonesCount:\(arrStonesCount)")
    lasertitle.insert("STONE(S)", at: sender.tag)
        
    let valse = NSMutableArray()
    valse.add(LaserValues())
    laserArray.insert(valse, at: laserArray.count - 1)
    let myTimer = Timer(timeInterval: 0.5, target: self, selector: #selector(self.timerCallBack(_:)), userInfo: nil, repeats: false)
    RunLoop.current.add(myTimer, forMode: RunLoop.Mode.common)
    }
    @IBAction func OnClickPlus(_ sender: UIButton) {
        let valse = laserArray.object(at: (sender.tag/lng1)) as! NSMutableArray
        if((sender.tag%lng1) == 0){
            let onemore = LaserValues()
            onemore.quntity = ""
            onemore.rate = "1.00"
            valse.add(onemore)
        }else{
            //            if(sender.tag/lng1 == 8){
            //                let stonesArray = NSMutableArray()
            //                let stonesdict = NSMutableDictionary()
            //                stonesdict.setValue("Yogesh", forKey: "stone_name")
            //                let singleStoneArray = LaserValues()
            //                singleStoneArray.quntity = ""
            //                singleStoneArray.rate = "1.00"
            //
            //                stonesdict.setValue(singleStoneArray, forKey: "Values")
            //                stonesArray.add(stonesdict)
            //                valse.add(stonesArray)
            //
            //                print("working on 8 laser array: \(valse)")
            //                for i in 0..<valse.count{
            //                    let x = valse[i] as! NSDictionary
            //                    print("Stone name: ",x.value(forKey: "stone_name") as! String)
            //                    let innerarr = x.value(forKey: "Values") as! NSArray
            //                    for j in 0..<innerarr.count{
            //                        let y = innerarr[j] as! LaserValues
            //                        print("quantity: ", y.quntity)
            //                        print("rate: ", y.rate)
            //                    }
            ////                    print("values: ")
            //                }
            //            }else{
            valse.removeObject(at: sender.tag%lng1)
            //            }
        }
        tableView.reloadData()
    }

    @IBAction func OnClickRemove(_ sender: UIButton) {
        let dict = result[sender.tag] as! NSDictionary
        let name = dict.value(forKey: "mTableName") as! String
        dismissKeyboard()
        let alertController = UIAlertController(title: "", message: "Do You Want To Delete ?", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction) in
            self.Remove(name:name)
            self.onClickOpen(sender)
        }
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textfield = textField
        let myTimer = Timer(timeInterval: 0.01, target: self, selector: #selector(self.selectText(_:)), userInfo: nil, repeats: false)
        RunLoop.current.add(myTimer, forMode: RunLoop.Mode.common)
        
        if textField.tag == -1 {
            dismissKeyboard()
            return false
        }
        return true
    }    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.tag > 0{
            let val = textField.tag/(lng1*lng2)
            let tag = textField.tag%(lng1*lng2)
            let indexPath = NSIndexPath(row: tag%lng1, section: tag/lng1)
            let lcc =  tableView.cellForRow(at: indexPath as IndexPath)
            if val == 1 && lcc != nil {
                let lc =  lcc as! LaserCell
                if let text = textField.text as NSString? {
                    if lc.txtTitle.tag == -(lasertitle.count - 2) && text.length > 0 && indexPath.row == 0 {
                        lasertitle.insert("MISC (click to edit)", at: lasertitle.count - 1)
                        let valse = NSMutableArray()
                        valse.add(LaserValues())
                        laserArray.insert(valse, at: laserArray.count - 1)
                        let myTimer = Timer(timeInterval: 0.5, target: self, selector: #selector(self.timerCallBack(_:)), userInfo: nil, repeats: false)
                        RunLoop.current.add(myTimer, forMode: RunLoop.Mode.common)
                    }
                }
            }
        }
        return true
    }
    @objc func selectText(_ timer: Timer) {
        textfield.becomeFirstResponder()
        textfield.selectedTextRange = textfield.textRange(from: textfield.beginningOfDocument, to: textfield.endOfDocument)
    }
    @objc func timerCallBack(_ timer: Timer) {
        tableView.reloadData()
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag < -1 {
            let val = textField.tag * (-1)
            var str = lasertitle.object(at: val) as! String
            if let text = textField.text as NSString? {
                str  = text.replacingCharacters(in: range, with: string)
                str = str.uppercased()
                lasertitle.removeObject(at: val)
                lasertitle.insert(str, at: val)
                textfield.text = str
            }            
            return false
        }
        let val = textField.tag/(lng1*lng2)
        let tag = textField.tag%(lng1*lng2)
        let valse = laserArray.object(at: tag/lng1) as! NSMutableArray
        let lsrow  = valse.object(at: tag%lng1) as! LaserValues
        let indexPath = NSIndexPath(row: tag%lng1, section: tag/lng1)
        let lc =  tableView.cellForRow(at: indexPath as IndexPath) as! LaserCell
        if val == 1 {
            if let text = textField.text as NSString? {
                lsrow.quntity  = text.replacingCharacters(in: range, with: string)
            }
        }
        if val == 2 {
            if let text = textField.text as NSString? {
                lsrow.rate  = text.replacingCharacters(in: range, with: string)
            }
        }
        lc.lblResult.text = valuecalculate(lsrow.quntity, rate: lsrow.rate)
        if valse.count > 1{
            let indexPath0 = NSIndexPath(row: valse.count, section: tag/lng1)
            var grandtotal = Double(0.00)
            for val in valse {
                let lsrow = val  as! LaserValues
                let new = (lsrow.quntity.replacingOccurrences(of: ",", with: "") as NSString).doubleValue
                let new2 = (lsrow.rate.replacingOccurrences(of: ",", with: "") as NSString).doubleValue
                grandtotal = grandtotal + (new * new2)
            }
            let lcc =  tableView.cellForRow(at: indexPath0 as IndexPath)
            if lcc != nil{
                let lc0 =  lcc as! LaserCell
                lc0.lblResult.text = clean("\(grandtotal)")
            }
        }
        txtGrandTotal.text = GrandTotal()
        txtGTotal.text = txtGrandTotal.text
        return true
    }
    
    
    func valuecalculate(_ qnt:String, rate rt:String) -> String{
        let new = qnt.replacingOccurrences(of: ",", with: "") as NSString
        let new2 = rt.replacingOccurrences(of: ",", with: "") as NSString
        return clean("\(new.doubleValue * new2.doubleValue)")
    }
    
    func clean(_ value: String?) -> String {
        guard value != nil else { return "$0.00" }
        let doubleValue = Double(value!) ?? 0.0
        let formatter = NumberFormatter()
        formatter.currencyCode = "USD"
        formatter.currencySymbol = "$"
        formatter.minimumFractionDigits = (value!.contains(".00")) ? 0 : 2
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .decimal
        let str = formatter.string(from: NSNumber(value: doubleValue)) ?? "\(doubleValue)"
        return "$"+str
    }
    @objc func timerCallBackMail(_ timer: Timer) {
        pdfDataWithTableView(tableView: tableView)
    }
    func pdfDataWithTableView(tableView: UITableView) {
        self.topView.backgroundColor = UIColor.init(red: 246/255.0, green: 229/255.0, blue: 170/255.0, alpha: 1)
        let priorBounds = tableView.bounds
        let fittedSize = tableView.sizeThatFits(CGSize(width:priorBounds.size.width, height:tableView.contentSize.height + self.topView.frame.size.height))
        tableView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:tableView.frame.width, height:tableView.contentSize.height + self.topView.frame.size.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        var pageOriginY: CGFloat = 0
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        
        UIGraphicsGetCurrentContext()!.saveGState()
        UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: 0)
        
        self.topView.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        pageOriginY = self.topView.frame.size.height;
        while pageOriginY < fittedSize.height {
            UIGraphicsGetCurrentContext()!.saveGState()
            UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: pageOriginY)
            tableView.layer.render(in: UIGraphicsGetCurrentContext()!)
            pageOriginY += pdfPageBounds.size.height
        }
        
        UIGraphicsGetCurrentContext()!.restoreGState()
        UIGraphicsEndPDFContext()
        tableView.bounds = priorBounds
        var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        docURL = docURL.appendingPathComponent("myDocument.pdf")
        pdfData.write(to: docURL as URL, atomically: true)
        self.topView.backgroundColor = UIColor.clear
        let messageBody = "Attached is the Jewelers' Scale App \(String(describing: lbl_filename.text!)) Ledger screen "
        let toRecipents = [""]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject("Jewelers' Scale App \(String(describing: lbl_filename.text!)) Ledger Screen")
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        mc.addAttachmentData(pdfData as Data, mimeType: "application/pdf", fileName:  "\(lbl_filename.text!).pdf")
        
        self.present(mc, animated: true, completion: nil)
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case MFMailComposeResult.cancelled:
            break
        case MFMailComposeResult.saved:
            break
        case MFMailComposeResult.sent:
            DispatchQueue.main.async(execute: {
            })
            break
        case MFMailComposeResult.failed:
            break
        }
        controller.dismiss(animated: true)
    }
    func listFilesFromDocumentsFolder() -> [String]?
    {
        let fileMngr = FileManager.default;
        let docs = fileMngr.urls(for: .documentDirectory, in: .userDomainMask)[0].path
        return try? fileMngr.contentsOfDirectory(atPath:docs)
    }
    @IBAction func OnClick_Save(_ sender: UIButton) {
        if UserDefaults.standard.string(forKey: "login") != nil  {
            if(sender.tag == 0 && selPfd > -1){
                Alert(lbl_filename.text!)
            }else{
                Alert()
            }
        }else{
            alartviewShow(str: "Valued Jewelers'Scale User,\n With our new updates, registration is now required in order to manage user prefrences and provide access to saved ladger files from other iOS devices")
        }
        
    }
    
    func Alert(){
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "Enter File Name", message: "", preferredStyle: UIAlertController.Style.alert)
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Enter File Name"
            }
            let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
                let firstTextField = alertController.textFields![0] as UITextField
                let text = alertController.textFields!.first!.text!
                if !text.isEmpty{
                    let date : Date = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MMM dd-yyyy, HH:mm"
                    let todaysDate = dateFormatter.string(from: date)
                    self.fileName = firstTextField.text!
                    self.fileDate = todaysDate
                    self.savetoapi(fileName: firstTextField.text!)
                }else{
                    let alertController1 = UIAlertController(title: "Please enter name", message: "", preferredStyle: UIAlertController.Style.alert)
                    let cancelAction1 = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                        (action : UIAlertAction!) -> Void in
                        self.Alert()
                    })
                    alertController1.addAction(cancelAction1)
                    self.present(alertController1, animated: true, completion: nil)
                }
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
                (action : UIAlertAction!) -> Void in })
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func Alert(_ str :String){
        DispatchQueue.main.async {
            let split = str.split(separator: "#").map(String.init)
            let alertController = UIAlertController(title: "Enter Name", message: "", preferredStyle: UIAlertController.Style.alert)
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Enter Name"
                textField.text = split[0]
            }
            let saveAsAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
                let text = alertController.textFields!.first!.text!
                let isAvailable = self.checkName(str)
                if !text.isEmpty {
                    let date : Date = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MMM dd-yyyy, HH:mm"
                    let todaysDate = dateFormatter.string(from: date)
                    self.fileDate = todaysDate
                    if isAvailable && text == split[0]{
                        let alertController1 = UIAlertController(title: "Do you want to overwrite", message: "", preferredStyle: UIAlertController.Style.alert)
                        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {
                            (action : UIAlertAction!) -> Void in
                            self.savetoapi(fileName: text)
                        })
                        alertController1.addAction(yesAction)
                        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: {
                            (action : UIAlertAction!) -> Void in
                        })
                        alertController1.addAction(cancelAction)
                        self.present(alertController1, animated: true, completion: nil)
                    }else{
                        self.savetoapi(fileName: text)
                    }
                }else{
                    let alertController1 = UIAlertController(title: "Please enter name", message: "", preferredStyle: UIAlertController.Style.alert)
                    let cancelAction1 = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
                        (action : UIAlertAction!) -> Void in
                        self.Alert(text)
                    })
                    alertController1.addAction(cancelAction1)
                    self.present(alertController1, animated: true, completion: nil)
                }
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
                (action : UIAlertAction!) -> Void in })
            alertController.addAction(saveAsAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func checkName(_ str : String) -> Bool {
        for res in result{
            let valtit = res as! NSDictionary
            let text = valtit.value(forKey: "mTableName") as? String
            if str == text {
                return true
            }
        }
        return false
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: tableViewList))!{
            return false
        }
        return true
    }
    @IBAction func onClickCalc(_ sender: UIButton) {
        let myVC = storyboard?.instantiateViewController(withIdentifier: "ViewController12") as! ViewController12
        self.present(myVC,animated: true,completion:nil)
    }
    @IBAction func onClickEmail(_ sender: UIButton) {
        tableView.setContentOffset(CGPoint.zero, animated:false)
        let myTimer = Timer(timeInterval: 0.1, target: self, selector: #selector(self.timerCallBackMail(_:)), userInfo: nil, repeats: false)
        RunLoop.current.add(myTimer, forMode: RunLoop.Mode.common)
    }
    @IBAction func onClickOpen(_ sender: Any) {
        if let email = UserDefaults.standard.string(forKey: "login")  {
            openLaserFile()
            btnOpen1.isHidden = true
            btnSave2.isHidden = true
            searchBar.isHidden = false
            self.tableViewList.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.tableViewList.alpha = 1
            }, completion:  nil)
        }else{
            alartviewShow(str: "Valued Jewelers'Scale User,\n With our new updates, registration is now required in order to manage user prefrences and provide access to saved ladger files from other iOS devices")
        }
    }
    
    @IBAction func OnClickReset(_ sender: UIButton) {
        lasertitle.removeAllObjects()
        laserArray.removeAllObjects()
        arrStonesCount.removeAllObjects()
        lasertitle = ["CAD DESIGN(S)","WAX DESIGN(S)"
            ,"3D PRINT(S)","MOLD(S)","CASTING(S)","MOUNTING(S)","FINDING(S)","JEWELER","STONE(S)","SETTER","POLISHING","FINISHING","ENGRAVING","MISC (click to edit)",""];
        self.arrStonesCount = ["Diamond", "Diamond", "Diamond", "Diamond", "Diamond", "Diamond", "Diamond", "Diamond", "Diamond","Diamond","Diamond","Diamond","Diamond","Diamond",""];
        plaseCount = 0
        for i in 0..<lasertitle.count{
            let valse = NSMutableArray()
            if i < lasertitle.count - 1{
                let laserVal = LaserValues()
                if i == 4{
                    laserVal.quntity = tatalCasting
                }
                valse.add(laserVal)
            }
            laserArray.add(valse)
        }
        txtGrandTotal.text = "$0.00"
        tableView.reloadData()
    }
    var result0 = NSArray()
    func openLaserFile(){
        guard Reachability.isConnectedToNetwork() == true else{
            let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
            return
        }
        postWithoutImage(url_string: "https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/getJsonData", parameters: "user_id=\(userid)", CompletionHandler: {json, error in
            if json?.value(forKey: "success") as! String == "true"{
                DispatchQueue.main.async {
                    let dict = json?.value(forKey: "result") as! NSDictionary
                    let arr = dict.value(forKey: "json_data") as! NSArray
                    let revArr = arr.reversed() as NSArray
                    self.result = (revArr.mutableCopy() as! NSMutableArray)
                    self.result0 = self.result
                    self.searchBar.text = ""
                    //                    self.result = arr.reversed() as! NSMutableArray
                    self.tableViewList.reloadData()
                }
            }else{
                DispatchQueue.main.async {
                    self.alartview(message: json?.value(forKey: "message") as! String)
                }
            }
        })
    }
    func savetoapi(fileName:String){
        let main_array = NSMutableArray()
        for i in 0..<laserArray.count - 1 {
            let valse = laserArray[i] as! NSMutableArray
            let prize = NSMutableArray()
            let quantity = NSMutableArray()
            for val in valse {
                let lsrow = val  as! LaserValues
                let new = (lsrow.quntity as String)
                let new2 = (lsrow.rate as String)
                quantity.add(new)
                prize.add(new2)
            }
            let name = NSMutableDictionary()
            name.setValue(quantity, forKey: "Prize")
            name.setValue(prize, forKey: "Quantity")
            if i < lasertitle.count{
                name.setValue(lasertitle[i], forKey: "Name")
                main_array.add(name)
            }
        }
        let dict = NSMutableDictionary()
        dict.setValue(main_array, forKey: "mMain1")
        dict.setValue(fileName, forKey: "mTableName")
        dict.setValue(fileDate, forKey: "mTableName2")        
        let jsonData = try! JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
        guard Reachability.isConnectedToNetwork() == true else{
            let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
            return
        } //https://adarshtated.com/dev/jeweller_app/test_new_index.php?fun=get_prices https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/getNewTrialtime (edited) 
        postWithImage(filename: fileName, url_string: "https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/insertJSONFile", parameters: ["user_id":"\(userid)"], imageParameterName:"doc_file" , selected_Image: jsonData as NSData, CompletionHandler: {json, error in
            if json?.value(forKey: "success") as! String == "true"{
                self.alartview(message: json?.value(forKey: "message") as! String)
            }else{
                self.alartview(message: json?.value(forKey: "message") as! String)
            }
        })
    }
    func alartview(message:String){
        let alertController1 = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        let cancelAction1 = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        alertController1.addAction(cancelAction1)
        self.present(alertController1, animated: true, completion: nil)
    }
}
public func postWithImage(filename:String,url_string: String,parameters: [String:String],imageParameterName:String,selected_Image: NSData,CompletionHandler: @escaping (NSDictionary?, String?) -> Void){
    let url = URL(string: url_string)
    var request = URLRequest(url: url!)
    request.httpMethod = "POST"
    let boundary = "Boundary-\(NSUUID().uuidString)"
    request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
    request.httpBody = createBodyWithParametersImage(filename: filename, parameters: parameters, filePathKey1: imageParameterName, imageDataKey1: selected_Image as NSData, boundary: boundary) as Data
    let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
        guard error == nil else {
            return
        }
        do {
            if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary {
                CompletionHandler(json, nil)
            }
        } catch let error {
            print(error.localizedDescription)
        }
    })
    task.resume()
}
public func createBodyWithParametersImage(filename:String,parameters: [String: String]?,filePathKey1:String?,imageDataKey1: NSData, boundary: String) -> NSData {
    let body = NSMutableData();
    if parameters != nil {
        for (key, value) in parameters! {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
    }
    let filename1 = "\(filename).doc"
    let mimetype1 = "doc"
    body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
    body.append("Content-Disposition: form-data; name=\"\(filePathKey1!)\"; filename=\"\(filename1)\"\r\n".data(using: String.Encoding.utf8)!)
    body.append("Content-Type: \(mimetype1)\r\n\r\n".data(using: String.Encoding.utf8)!)
    body.append(imageDataKey1 as Data)
    body.append("\r\n".data(using: String.Encoding.utf8)!)
    body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
    return body
}
public func randomString(length: Int) -> String {
    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    return String((0...length-1).map{ _ in letters.randomElement()! })
}
