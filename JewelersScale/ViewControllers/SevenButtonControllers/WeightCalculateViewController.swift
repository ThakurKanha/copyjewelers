//
//  WeightCalculateViewController.swift
//  JewelersScale
//
//  Created by Apple on 19/05/18.
//  Copyright © 2018 MacOS High. All rights reserved.
//

import UIKit
import GoogleMobileAds
import SwiftKeychainWrapper
class TableViewCell1:UITableViewCell{
    
  
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var first_lbl: UILabel!
    override func awakeFromNib() {
        super .awakeFromNib()
    view1.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
    view1.layer.borderWidth = 1
    view1.layer.cornerRadius = 3
    }
}
class TableViewCell2:UITableViewCell{
    
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var second_lbl: UILabel!
    override func awakeFromNib() {
        super .awakeFromNib()
    view2.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
    view2.layer.borderWidth = 1
    view2.layer.cornerRadius = 3
    }
}
class WeightCalculateViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,GADBannerViewDelegate, GADInterstitialDelegate {
    var arraylist = NSMutableArray()
    var array = NSMutableArray()
    var glob: Int = 0
    var glob1: Int = 3
  
    var  carat: Double             = 1.000000
    var  pennyweight: Double       = 0.128600
    var  ounceAvoir: Double        = 0.007055
    var  ounceTroy: Double         = 0.006430
    var  gram: Double              = 0.200000
    var  kilogram: Double          = 0.000200
    var  pound: Double             = 0.000441
   
    var  carat1: Double             = 7.775869
    var  pennyweight1: Double       = 1.000000
    var  ounceAvoir1: Double        = 0.054857
    var  ounceTroy1: Double         = 0.050000
    var  gram1: Double              = 1.555174
    var  kilogram1: Double          = 0.001555
    var  pound1: Double             = 0.003429
    
    var  carat2: Double             = 141.747610
    var  pennyweight2: Double       = 18.229167
    var  ounceAvoir2: Double        = 1.000000
    var  ounceTroy2: Double         = 0.911458
    var  gram2: Double              = 28.349523
    var  kilogram2: Double          = 0.028350
    var  pound2: Double             = 0.062500
    
    var  carat3: Double             = 155.517350
    var  pennyweight3: Double       = 19.999996
    var  ounceAvoir3: Double        = 1.097143
    var  ounceTroy3: Double         = 1.000000
    var  gram3: Double              = 31.103471
    var  kilogram3: Double          = 0.031103
    var  pound3: Double             = 0.068571
    
    var  carat4: Double             = 5.000000
    var  pennyweight4: Double       = 0.643015
    var  ounceAvoir4: Double        = 0.035274
    var  ounceTroy4: Double         = 0.032151
    var  gram4: Double              = 1.000000
    var  kilogram4: Double          = 0.001000
    var  pound4: Double             = 0.002205
    
    var  carat5: Double             = 5000.000000
    var  pennyweight5: Double       = 643.014930
    var  ounceAvoir5: Double        = 35.273962
    var  ounceTroy5: Double         = 32.150747
    var  gram5: Double              = 1000.000000
    var  kilogram5: Double          = 1.000000
    var  pound5: Double             = 2.204623
    var textfield:UITextField!
    var  carat6: Double             = 2267.961900
    var  pennyweight6: Double       = 291.666666
    var  ounceAvoir6: Double        = 16.000000
    var  ounceTroy6: Double         = 14.583333
    var  gram6: Double              = 453.592370
    var  kilogram6: Double          = 0.453592
    var  pound6: Double             = 1.000000
    var lastdate = String()
    var currentdate = Date()
    var signViewShow = UIView()
    var signCompleteLogin = UIView()
    
    @IBOutlet weak var Banner_view: GADBannerView!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var pennyweight_btn: UIButton!
    @IBOutlet weak var ounce_btn: UIButton!
    @IBOutlet weak var table_view_second: UITableView!
    @IBOutlet weak var txt_num: UITextField!
    @IBOutlet weak var txt_value: UILabel!
    @IBOutlet weak var result_txt: UILabel!
    @IBOutlet weak var secont_unit: UILabel!
    @IBOutlet weak var first_unit: UILabel!
    
    @IBOutlet weak var back_btn: UIButton!
    @IBAction func Menu_btn(_ sender: UIButton) {
        if let email = UserDefaults.standard.string(forKey: "login")  {
           self.signCompleteLogin.isHidden = false
        }else{
           self.signViewShow.isHidden = false
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentdate = Date.changeDaysBy(days: 0)               
        self.signViewShow = showViewLogIn()
        self.signCompleteLogin = showViewLogInComplete()
        self.signViewShow.isHidden = true
        self.signCompleteLogin.isHidden = true
        pennyweight_btn.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        pennyweight_btn.layer.borderWidth = 1
        pennyweight_btn.layer.cornerRadius = 3
        
        ounce_btn.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        ounce_btn.layer.borderWidth = 1
        ounce_btn.layer.cornerRadius = 3
        
        txt_num.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        txt_num.layer.borderWidth = 1
        txt_num.layer.cornerRadius = 3
        
        back_btn.imageView?.contentMode = .scaleAspectFit
        back_btn.setImage(UIImage(named: "back.png"), for: .normal)
        pennyweight_btn.setTitle("Gram(s)", for:UIControl.State.normal)
        ounce_btn.setTitle("Ounce,Troy(s)", for:UIControl.State.normal)
         txt_num.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        let padding: CGFloat = 5.0
        pennyweight_btn.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: padding, bottom: 0.0, right: -padding)
        pennyweight_btn.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: padding)
        ounce_btn.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: padding, bottom: 0.0, right: -padding)
        ounce_btn.contentEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: padding)
        table_view_second.isHidden = true
        table_view.isHidden = true
        table_view.delegate = self
        table_view.dataSource = self
        table_view_second.delegate = self
        table_view_second.dataSource = self
        txt_num.delegate = self
        array = ["Gram(s)","Pennyweight(s)","Ounce,Avoir(s)","Ounce,Troy(s)","Carat(s)","Kilogram(s)","Pound(s)"]
        arraylist = ["Gram(s)","Pennyweight(s)","Ounce,Avoir(s)","Ounce,Troy(s)","Carat(s)","Kilogram(s)","Pound(s)"]
        
        let boolen =  UserDefaults.standard.bool(forKey: "BOOL")
        if boolen == false{
            Banner_view.adUnitID = "ca-app-pub-6572892411343856/7270604337"
            Banner_view.rootViewController = self
            Banner_view.load(GADRequest())
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textfield = textField
        let myTimer = Timer(timeInterval: 0.01, target: self, selector: #selector(self.selectText(_:)), userInfo: nil, repeats: false)
        RunLoop.current.add(myTimer, forMode: RunLoop.Mode.common)
        return true
    }
    @objc func selectText(_ timer: Timer) {
        
        textfield.becomeFirstResponder()
        textfield.selectedTextRange = textfield.textRange(from: textfield.beginningOfDocument, to: textfield.endOfDocument)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        resultvalue()
        
        
    }
    func clean(_ value: String?) -> String {
        guard value != nil else { return "$0.00" }
        let doubleValue = Double(value!) ?? 0.0
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.maximumFractionDigits = 2
        currencyFormatter.minimumFractionDigits = 2
        currencyFormatter.numberStyle = .decimal
        currencyFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale?
        let priceString = currencyFormatter.string(from: NSNumber(value: doubleValue))!
        return priceString
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.table_view.isHidden = true
        self.table_view_second.isHidden = true
        self.signViewShow.isHidden = true
        self.signCompleteLogin.isHidden = true
    }
  
    @IBAction func convert_click(_ sender: Any) {
        txt_num.text = "1.00"
        txt_value.text = "1.00"
        result_txt.text = "0.03"
        first_unit.text = "Gram(s)"
        secont_unit.text = "Ounce,Troy(s)"
        pennyweight_btn.setTitle("Gram(s)", for:UIControl.State.normal)
        ounce_btn.setTitle("Ounce,Troy(s)", for:UIControl.State.normal)
    }
    
    func resultvalue(){
        let myfloat : Double = 1.00
        txt_value.text = NSString(format: "%.2f", myfloat) as String
        txt_value.text = txt_num.text
        let str = txt_num.text!
        let new = (str as NSString).doubleValue
        let st = result_txt.text!
        var res = (st as NSString).doubleValue

        if glob == 0 && glob1 == 0{
             res = new * gram4
             result_txt.text = clean(String(res))
        }
        if glob == 0 && glob1 == 1{
            res = new * pennyweight4
            result_txt.text = clean(String(res))
        }
        if glob == 0 && glob1 == 2{
            let res = new * ounceAvoir4
            result_txt.text = clean(String(res))
        }
        if glob == 0 && glob1 == 3{
            let res = new * ounceTroy4
            result_txt.text = clean(String(res))
        }
        if glob == 0 && glob1 == 4{
            let res = new * carat4
            result_txt.text = clean(String(res))
        }
        if glob == 0 && glob1 == 5{
            let res = new * kilogram4
            result_txt.text = clean(String(res))
        }
        if glob == 0 && glob1 == 6{
            let res = new * pound4
            result_txt.text = clean(String(res))
        }
        if glob == 1 && glob1 == 0{
            let res = new * gram1
            result_txt.text = clean(String(res))
        }
        if glob == 1 && glob1 == 1{
            let res = new * pennyweight1
            result_txt.text = clean(String(res))
        }
        if glob == 1 && glob1 == 2{
            let res = new * ounceAvoir1
            result_txt.text = clean(String(res))
        }
        if glob == 1 && glob1 == 3{
            let res = new * ounceTroy1
            result_txt.text = clean(String(res))
        }
        if glob == 1 && glob1 == 4{
            let res = new * carat1
            result_txt.text = clean(String(res))
        }
        if glob == 1 && glob1 == 5{
            let res = new * kilogram1
            result_txt.text = clean(String(res))
        }
        if glob == 1 && glob1 == 6{
            let res = new * pound1
            result_txt.text = clean(String(res))
        }
        if glob == 2 && glob1 == 0{
            let res = new * gram2
            result_txt.text = clean(String(res))
        }
        if glob == 2 && glob1 == 1{
            let res = new * pennyweight2
            result_txt.text = clean(String(res))
        }
        if glob == 2 && glob1 == 2{
            let res = new * ounceAvoir2
            result_txt.text = clean(String(res))
        }
        if glob == 2 && glob1 == 3{
            let res = new * ounceTroy2
            result_txt.text = clean(String(res))
        }
        if glob == 2 && glob1 == 4{
            let res = new * carat2
            result_txt.text = clean(String(res))
        }
        if glob == 2 && glob1 == 5{
            let res = new * kilogram2
            result_txt.text = clean(String(res))
        }
        if glob == 2 && glob1 == 6{
            let res = new * pound2
            result_txt.text = clean(String(res))
        }
        if glob == 3 && glob1 == 0{
            let res = new * gram3
            result_txt.text = clean(String(res))
        }
        if glob == 3 && glob1 == 1{
            let res = new * pennyweight3
            result_txt.text = clean(String(res))
        }
        if glob == 3 && glob1 == 2{
            let res = new * ounceAvoir3
            result_txt.text = clean(String(res))
        }
        if glob == 3 && glob1 == 3{
            let res = new * ounceTroy3
            result_txt.text = clean(String(res))
        }
        if glob == 3 && glob1 == 4{
            let res = new * carat3
            result_txt.text = clean(String(res))
        }
        if glob == 3 && glob1 == 5{
            let res = new * kilogram3
            result_txt.text = clean(String(res))
        }
        if glob == 3 && glob1 == 6{
            let res = new * pound3
            result_txt.text = clean(String(res))
        }
        if glob == 4 && glob1 == 0{
            let res = new * gram
            result_txt.text = clean(String(res))
        }
        if glob == 4 && glob1 == 1{
            let res = new * pennyweight
            result_txt.text = clean(String(res))
        }
        if glob == 4 && glob1 == 2{
            let res = new * ounceAvoir
            result_txt.text = clean(String(res))
        }
        if glob == 4 && glob1 == 3{
            let res = new * ounceTroy
            result_txt.text = clean(String(res))
        }
        if glob == 4 && glob1 == 4{
            let res = new * carat
            result_txt.text = clean(String(res))
        }
        if glob == 4 && glob1 == 5{
            let res = new * kilogram
            result_txt.text = clean(String(res))
        }
        if glob == 4 && glob1 == 6{
            let res = new * pound
            result_txt.text = clean(String(res))
        }
        if glob == 5 && glob1 == 0{
            let res = new * gram5
            result_txt.text = clean(String(res))
        }
        if glob == 5 && glob1 == 1{
            let res = new * pennyweight5
            result_txt.text = clean(String(res))
        }
        if glob == 5 && glob1 == 2{
            let res = new * ounceAvoir5
            result_txt.text = clean(String(res))
        }
        if glob == 5 && glob1 == 3{
            let res = new * ounceTroy5
            result_txt.text = clean(String(res))
        }
        if glob == 5 && glob1 == 4{
            let res = new * carat5
            result_txt.text = clean(String(res))
        }
        if glob == 5 && glob1 == 5{
            let res = new * kilogram5
            result_txt.text = clean(String(res))
        }
        if glob == 5 && glob1 == 6{
            let res = new * pound5
            result_txt.text = clean(String(res))
        }
        if glob == 6 && glob1 == 0{
            let res = new * gram6
            result_txt.text = clean(String(res))
        }
        if glob == 6 && glob1 == 1{
            let res = new * pennyweight6
            result_txt.text = clean(String(res))
        }
        if glob == 6 && glob1 == 2{
            let res = new * ounceAvoir6
            result_txt.text = clean(String(res))
        }
        if glob == 6 && glob1 == 3{
           let res = new * ounceTroy6
           result_txt.text = clean(String(res))
        }
        if glob == 6 && glob1 == 4{
             let res = new * carat6
             result_txt.text = clean(String(res))
        }
        if glob == 6 && glob1 == 5{
            let res = new * kilogram6
            result_txt.text = clean(String(res))
        }
        if glob == 6 && glob1 == 6{
            let res = new * pound6
            result_txt.text = clean(String(res))
        }
        }
    @IBAction func back(_ sender: Any) {
         dismiss(animated: false, completion: nil)
    }
    
    @IBAction func penny_weight_action(_ sender: Any) {
        if table_view.isHidden == true{
            table_view.isHidden = false
        }
        else{
            table_view.isHidden = true
        }
    }
    
    @IBAction func ounce_action(_ sender: Any) {
        if table_view_second.isHidden == true{
            table_view_second.isHidden = false
        }
        else{
            table_view_second.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if tableView == self.table_view {
        return array.count
    }
    else {
        return arraylist.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
          if tableView == self.table_view {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell1
        cell.first_lbl.text = array[indexPath.row] as? String

             return cell
        }
          else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TableViewCell2
            cell.second_lbl.text = arraylist[indexPath.row] as? String

             return cell
        }
       
    }
    var selectedValue = ""
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath)
        
        if tableView == self.table_view {
            selectedValue = (array[indexPath.row] as? String)!
        pennyweight_btn.setTitle(selectedValue, for: .normal)
        first_unit.text = array[indexPath.row] as? String
            table_view.isHidden = true
            if indexPath.row == 0{
                glob = indexPath.row
            }
            if indexPath.row == 1{
                glob = indexPath.row
            }
            if indexPath.row == 2{
                glob = indexPath.row
            }
            if indexPath.row == 3{
                glob = indexPath.row
            }
            if indexPath.row == 4{
                glob = indexPath.row
            }
            if indexPath.row == 5{
                glob = indexPath.row
            }
            if indexPath.row == 6{
                glob = indexPath.row
            }
            if indexPath.row == 7{
                glob = indexPath.row
            }
            if indexPath.row == 8{
                glob = indexPath.row
                
            }
        }
        else{
            selectedValue = (arraylist[indexPath.row] as? String)!
            ounce_btn.setTitle(selectedValue, for: .normal)
            table_view_second.isHidden = true
            secont_unit.text = arraylist[indexPath.row] as? String
            if indexPath.row == 0{
                glob1 = indexPath.row
            }
            if indexPath.row == 1{
                glob1 = indexPath.row
            }
            if indexPath.row == 2{
                glob1 = indexPath.row
            }
            if indexPath.row == 3{
                glob1 = indexPath.row
            }
            if indexPath.row == 4{
                glob1 = indexPath.row
            }
            if indexPath.row == 5{
                glob1 = indexPath.row
            }
            if indexPath.row == 6{
                glob1 = indexPath.row
            }
            if indexPath.row == 7{
                glob1 = indexPath.row
            }
            if indexPath.row == 8{
                glob1 = indexPath.row
            }
        }
        self.resultvalue()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 35
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
