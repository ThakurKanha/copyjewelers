//
//  AppDelegate.swift
//  JewelersScale
//
//  Created by MacOS High on 06/04/18.
//  Copyright © 2018 MacOS High. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftKeychainWrapper
import SwiftyDropbox
import GoogleSignIn
import Siren
var KAppDelegate = UIApplication.shared.delegate as! AppDelegate
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate {
    var gvalue = gmailvalue()
     //com.scientificwebs.CoupleCouponsBook
    var navigationController  = UINavigationController()
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {           
            let fullName = user.profile.name            
            let email = user.profile.email
            gvalue.name = fullName!
            gvalue.gmailid = email!
            print("names - -",email!,user.profile.email)
            print("gvalue - gvalue-",gvalue.name,gvalue.gmailid)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            
            postWithoutImage(url_string: "https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/loginBygmail", parameters: "email=\(email!)&username=\(fullName!)", CompletionHandler: { json, error in
                if json?.value(forKey: "success") as! String == "true"{
                    DispatchQueue.main.async {
                        UserDefaults.standard.set("\(email!)", forKey: "login")
                        UserDefaults.standard.removeObject(forKey: "userlogin")
                    let res = json?.value(forKey: "result") as! NSDictionary
                        UserDefaults.standard.set(res, forKey: "result")
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let objMainViewController: CustomViewController = mainStoryboard.instantiateViewController(withIdentifier: "CustomViewController") as! CustomViewController
                    self.navigationController = UINavigationController(rootViewController: objMainViewController)
                self.window?.rootViewController = self.navigationController
                self.window?.makeKeyAndVisible()
                    }
                      }
            })
        }
    }    
    var window: UIWindow?
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        UserDefaults.standard.set(true, forKey: "CheckAnim")
        return true
    }
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        print("didFinishLaunchingWithOptions")
        Siren.shared.wail()
        DispatchQueue.main.async {
        UserDefaults.standard.set(true, forKey: "CheckAnim")
        if UserDefaults.standard.value(forKey: "remember") == nil{
            UserDefaults.standard.removeObject(forKey: "login")
        }                
        IQKeyboardManager.shared.enable = true
        GIDSignIn.sharedInstance().clientID = "481713880445-1psqse5kmrn78gi5b9v4sbbvoljgg1u5.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self        
        }
            return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func applicationWillTerminate(_ application: UIApplication) {
        comman().remove(key: "selection_nil")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        DispatchQueue.main.async {
        if let authResult = DropboxClientsManager.handleRedirectURL(url) {
            switch authResult {
            case .success:
                let client = DropboxClientsManager.authorizedClient
                let fileManager = FileManager.default
                let directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
                let destURL = directoryURL.appendingPathComponent("myTestFile")
                let destination: (URL, HTTPURLResponse) -> URL = { temporaryURL, response in
                    return destURL
                }
                client?.files.download(path: "/test/path/in/Dropbox/account", overwrite: true, destination: destination)
                    .response { response, error in
                        if let response = response {
                            print(response)
                        } else if let error = error {
                            print(error)
                        }
                    }
                    .progress { progressData in
                        print(progressData)
                }
                client?.files.download(path: "/test/path/in/Dropbox/account")
                    .response { response, error in
                        if let response = response {
                            let responseMetadata = response.0
                            print(responseMetadata)
                            let fileContents = response.1
                            print(fileContents)
                        } else if let error = error {
                            print(error)
                        }
                    }
                    .progress { progressData in
                        print(progressData)
                }
            case .cancel:
                print("Authorization flow was manually canceled by user!")
            case .error(_, let description):
                print("Error: \(description)")
            }
          }
        }
        return GIDSignIn.sharedInstance().handle(url as URL?,
                                                 sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplication.OpenURLOptionsKey.annotation])

    }
}

