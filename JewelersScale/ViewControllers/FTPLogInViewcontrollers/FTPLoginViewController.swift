//
//  FTPLoginViewController.swift
//  JewelersScale
//
//  Created by Apple on 02/03/19.
//  Copyright © 2019 MacOS High. All rights reserved.
//

import UIKit
import rebekka
class cellShowLogIn: UITableViewCell {
    @IBOutlet weak var title_Button: UIButton!
    @IBOutlet weak var setting_Button: UIButton!
}
class shareFTPLogInData{
    var host = String()
    var userName = String()
    var password = String()
}
class FTPLoginViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    var loginList = NSMutableArray()
    var dict = NSMutableDictionary()
    var shareData = shareFTPLogInData()
    var tag = Int()
    var saveFTP = ""
    var host:String?
    var name:String?
    var password:String?
    @IBOutlet weak var txtf_HostName: UITextField!
    @IBOutlet weak var txtf_UserName: UITextField!
    @IBOutlet weak var txtf_Password: UITextField!
    @IBOutlet weak var image_Check: UIImageView!
    @IBOutlet weak var table_VIew: UITableView!
    @IBOutlet weak var view_Setting: roundView!
    @IBOutlet weak var settingTxtf_Host: UITextField!
    @IBOutlet weak var seetingTxtf_User: UITextField!
    @IBOutlet weak var settingTxtf_Password: UITextField!
    @IBOutlet weak var activator: UIActivityIndicatorView!
    
    
    
    var flagValue = true
    var arr = [ResourceItem]()
    override func viewDidLoad() {
        super.viewDidLoad()
        view_Setting.isHidden = true
        self.activator.isHidden = true
        self.activator.stopAnimating()
        image_Check.image = UIImage(named: "empty_check_box.png")
//        UserDefaults.standard.removeObject(forKey: "logindata")
        
    }
    override func viewDidAppear(_ animated: Bool) {
//        self.activator.isHidden = true
//        self.activator.stopAnimating()
        if UserDefaults.standard.value(forKey: "logindata") != nil{
            let val = (UserDefaults.standard.value(forKey: "logindata") as! NSArray).mutableCopy() as! NSMutableArray
                self.loginList = val.mutableCopy() as! NSMutableArray
//                print("login arr:\(self.loginList)")
        }
        if image_Check.image == UIImage(named: "empty_check_box.png"){
            self.saveFTP = ""
        }else{
            self.saveFTP = "save"
        }
        table_VIew.reloadData()
    }
    @IBAction func SettingButton_Edit(_ sender: Any) {
        view_Setting.isHidden = true
    }
    @IBAction func settingDelete_Button(_ sender: UIButton) {
        self.loginList.removeObject(at: tag)
        table_VIew.reloadData()
        UserDefaults.standard.set(self.loginList, forKey: "logindata")
        self.view_Setting.isHidden = true
    }
    @IBAction func btn_SaveFtpCon(_ sender: UIButton) {
        if image_Check.image == UIImage(named: "empty_check_box.png"){
            image_Check.image = UIImage(named: "check_box.png")
            self.saveFTP = "save"
        }else{
            image_Check.image = UIImage(named: "empty_check_box.png")
            self.saveFTP = ""
        }
        
    }
    @IBAction func btn_LogIn(_ sender: UIButton) {
        guard !(txtf_HostName.text!).isEmpty else {
            self.alartviewShow(str: "Enter HostName!")
            return
        }
        guard !(txtf_UserName.text!).isEmpty else {
            self.alartviewShow(str: "Enter UserName!")
            return
        }
        guard !(txtf_Password.text!).isEmpty else {
            self.alartviewShow(str: "Enter Password!")
            return
        }
        self.host = txtf_HostName.text!
        self.name = txtf_UserName.text!
        self.password = txtf_Password.text!
        self.logInFTPServer()
    }
    func logInFTPServer(){
        print("start")
        self.activator.isHidden = false
        self.activator.startAnimating()
        var configuration = SessionConfiguration()
        configuration.host = self.host!//"junnelmoments.com"
        configuration.username = self.name!//"stlviewerapp"
        configuration.password = self.password!//"Stlftp355!"
        configuration.encoding = String.Encoding.utf8
      
        let session = Session(configuration: configuration)
        session.list("") {
            (resources, error) -> Void in
            if error != nil{
                self.activator.isHidden = true
                self.activator.stopAnimating()
                self.alartviewShow(str: "Somthing Went Wrong!")
                return
            }
            self.dict =  ["Host":self.host!,"Name":self.name!,"Password":self.password!]
            self.arr = resources!
            print("self.saveFTP:\(self.saveFTP)")
            if self.saveFTP != "" {
            if UserDefaults.standard.value(forKey: "logindata") != nil{
                if let val = (UserDefaults.standard.value(forKey: "logindata") as! NSArray).mutableCopy() as? NSMutableArray{
                    for i in 0..<val.count{
                        let dict1 = val[i] as? NSMutableDictionary
                        let name = dict1?.value(forKey: "Name") as? String
                        if name == self.name!{
                            self.flagValue = false
                        }
                    }
                    if self.flagValue == true{
                        print("true")
                        self.loginList.add(self.dict)
                        UserDefaults.standard.set(self.loginList, forKey: "logindata")
                    }
                }
            }else{
                self.activator.isHidden = true
                self.activator.stopAnimating()
                self.loginList.add(self.dict)//
                UserDefaults.standard.set(self.loginList, forKey: "logindata")
            }
        }
            self.activator.isHidden = true
            self.activator.stopAnimating()
            let next = self.storyboard?.instantiateViewController(withIdentifier: "FtpDataListViewController") as! FtpDataListViewController
            next.arr = self.arr
            next.dict = self.dict
            next.configuration = configuration
            self.present(next, animated: true, completion: nil)
        }
       
    }
    @IBAction func btn_Back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return loginList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! cellShowLogIn
        let dict = loginList[indexPath.row] as? NSMutableDictionary
        cell.title_Button.setTitle("\(String(dict?["Name"] as? String ?? ""))", for: .normal)     ///.text = dict?["Name"] as? String
        cell.title_Button.tag = indexPath.row
        cell.setting_Button.tag = indexPath.row
        //        cell.title_Button.titleLabel?.numberOfLines = 0
        cell.title_Button.titleLabel?.adjustsFontSizeToFitWidth = true
        cell.title_Button.addTarget(self, action: #selector(clickLogIn), for: .touchUpInside)
        cell.setting_Button.addTarget(self, action: #selector(settingAction), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    @objc func settingAction(sender: UIButton!){
        print("click setting",sender.tag)
        self.tag = sender.tag
        let dict = self.loginList[sender.tag] as? NSDictionary
        settingTxtf_Host.text = dict!["Host"] as? String
        seetingTxtf_User.text = dict!["Name"] as? String
        settingTxtf_Password.text = dict!["Password"] as? String
        view_Setting.isHidden = false
    }
    @objc func clickLogIn(sender: UIButton!){
        print("click clickLogIN")
        let dict = self.loginList[sender.tag] as? NSDictionary
        self.host = dict!["Host"] as? String
        self.name = dict!["Name"] as? String
        self.password = dict!["Password"] as? String
        logInFTPServer()
    }
    
}
extension UIViewController{
    func alartviewShow(str:String){
        let alert = UIAlertController(title: "", message: str, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
}
