//
//  FtpDataListViewController.swift
//  JewelersScale
//
//  Created by Apple on 05/03/19.
//  Copyright © 2019 MacOS High. All rights reserved.
//

import UIKit
import rebekka
class FtpDataListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var configuration = SessionConfiguration()
    var arr = [ResourceItem]()
    var shareData = shareFTPLogInData()
    var sceneUrl = [URL]()
    var host = String()
    var name = String()
    var password = String()
    var arrPath = [String]()
    var newPath = String()
    var dict = NSMutableDictionary()
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var activator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activator.isHidden = true
        self.activator.stopAnimating()
        host = dict.value(forKey: "Host") as? String ?? ""
        name = dict.value(forKey: "Name") as? String ?? ""
        password = dict.value(forKey: "Password") as? String ?? ""
        print("arr:\(dict)",host,name,password)
    }
    
    @IBAction func back(_ sender: UIButton) {
        if self.arrPath.count == 0{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.arrPath.removeLast()
            loadNewPath()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let removeText = self.arr[indexPath.row]
        var text = removeText.path
        text.remove(at: text.startIndex)
        if removeText.type == .Directory{
        cell?.imageView?.image = UIImage(named: "folder.png")
        }else{
        cell?.imageView?.image = UIImage(named: "ftp.png")
        }
        cell?.textLabel?.text = text
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let downText = self.arr[indexPath.row]
        let removeText = self.arr[indexPath.row]
        print("removeText:\(removeText)")
        var text = removeText.path
        text.remove(at: text.startIndex)
        let path = (text as NSString).pathExtension
        print("path:\(path)")
        if path == "stl"{
            let alert = UIAlertController(title: "", message: "Download file", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                print("downText:\(downText)")
                self.downloadSTLFile(downText.path)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else if removeText.type == .Directory{
            if self.arrPath.count == 0{
                arrPath.append("/\(text)")
            }else{
                arrPath = ["/\(text)"]
            }
            loadNewPath()
        }else{
            //let dict = arr[indexPath.row] as? NSDictionary
            //print("dict:\(String(describing: dict))")
            
            self.alartviewShow(str: "Somthing Went Wrong!")
        }
    }
    
    fileprivate func loadNewPath(){
        newPath = ""
        for i in 0..<arrPath.count{
            newPath.append(arrPath[i])
        }
        print("new path: \(newPath)")
        let session = Session(configuration: configuration)
        session.list(newPath) {
            (resources, error) -> Void in
            if error != nil{
                self.activator.isHidden = true
                self.activator.stopAnimating()
                self.alartviewShow(str: "Somthing Went Wrong! Error: \(String(describing: error))")
                self.arrPath.removeLast()
                return
            }
            self.arr = resources!
            self.table_View.reloadData()
            self.activator.isHidden = true
            self.activator.stopAnimating()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func downloadSTLFile(_ filePath:String){
        self.activator.isHidden = false
        self.activator.startAnimating()
        configuration.encoding = String.Encoding.utf8
        print("shareData.host:\(host)")
        configuration.host = host
        configuration.username = name
        configuration.password = password
        let session = Session(configuration: configuration)
        print("filename :\(filePath)")
                session.download(filePath, completionHandler: { (url, error) in
                     print("Url download :\(url)" )
                    if error != nil{
                        self.activator.isHidden = true
                        self.activator.stopAnimating()
                        self.alartviewShow(str: "Something Went Wrong!")
                        return
                    }
                    self.sceneUrl.append((url)!)
                    var strings : [String] = []
                    if let array:[URL] = self.sceneUrl{
                        for url in array {
                            let string = url.absoluteString
                            strings.append(string)
                        }
                    }
                    print("strings :\(strings)")
                    self.activator.isHidden = true
                    self.activator.stopAnimating()
                    UserDefaults.standard.set(strings, forKey: "url1")
                    let next = self.storyboard?.instantiateViewController(withIdentifier: "ViewController1") as! ViewController1
                    //next.
                    self.present(next, animated: true, completion: nil)
                })
    }
}
