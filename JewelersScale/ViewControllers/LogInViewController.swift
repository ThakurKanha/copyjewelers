//
//  LogInViewController.swift
//  JewelersScale
//
//  Created by Virendra on 24/12/18.
//  Copyright © 2018 MacOS High. All rights reserved.
//

import UIKit
import GoogleSignIn
import Foundation
import SystemConfiguration
class TableGold:UITableViewCell{
    
    @IBOutlet weak var ui_view: UIView!
    @IBOutlet weak var cell_lbl: UILabel!
    override func awakeFromNib() {
        super .awakeFromNib()
        ui_view.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        
    }    
}
public func postWithoutImage(url_string: String, parameters : String, CompletionHandler: @escaping (NSDictionary?, String?) -> Void){
         let url = URL(string: url_string)!
         var request = URLRequest(url: url)
         request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
         request.httpMethod = "POST"
    if parameters != nil {
         request.httpBody = parameters.data(using: .utf8)
    }
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
         guard let data = data, error == nil else {
            return
          }
    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            return
        }
    guard error == nil else {return}
    do {
    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                CompletionHandler(json, nil)
            }
        } catch let error {
            print(error.localizedDescription)
        }
    }
    task.resume()
}
public func createBodyWithParameters(parameters: [String: Any]) -> NSData {
    let body = NSMutableData();
    let boundary = "Boundary-\(NSUUID().uuidString)"
    for (key, value) in parameters {
        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
        body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
    }
    return body
}
public func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {}
    }
    return nil
}

class LogInViewController: UIViewController,GIDSignInUIDelegate {
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_pass: UITextField!
    var flag = false
    var values = gmailvalue()
    @IBOutlet weak var activator: UIActivityIndicatorView!
    @IBOutlet weak var check_box: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     check_box.image = UIImage(named: "check_box.png")
     UserDefaults.standard.set("login", forKey: "remember")
        UserDefaults.standard.set(false, forKey: "CheckAnim")
     activator.isHidden = true
     textfieldpedding(txtfield: txt_email)
     textfieldpedding(txtfield: txt_pass)
     GIDSignIn.sharedInstance().uiDelegate = self
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func alartview(str:String){
        let alert = UIAlertController(title: "", message: str, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func Google_signIn(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    @IBAction func Sign_up(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "RagistrationViewController") as! RagistrationViewController
        self.present(next, animated: true, completion: nil)
    }
    @IBAction func Sign_In(_ sender: UIButton) {
        
        let emailRegEx: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        if txt_email.text == ""{
            alartview(str: "Please enter Email Address")
            return
        }else if emailTest.evaluate(with: txt_email.text) != true {
           alartview(str: "Please enter valid email address")
           return
     } else if txt_pass.text == ""{
            alartview(str: "Please enter Password")
            return
        }else{
            activator.isHidden = false
            activator.startAnimating()
            guard Reachability.isConnectedToNetwork() == true else{
                let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    self.activator.stopAnimating()
                    self.activator.isHidden = true
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                return
            }//https://adarshtated.com/dev/jeweller_app/api/index.php/api/V2/signUp
        postWithoutImage(url_string: "https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/logIn", parameters: "email=\(txt_email.text!)&password=\(txt_pass.text!)", CompletionHandler: {json, error in
            let message = json?.value(forKey: "message") as! String
            if json?.value(forKey: "success") as! String == "true"{
                print("json :=",json)
                DispatchQueue.main.async {
                self.activator.isHidden = true
                self.activator.stopAnimating()
                let res = json?.value(forKey: "result") as! NSDictionary
                let scrapgold = res.value(forKey: "scrap_data") as! String                
                let wwcdata = res.value(forKey: "wcc_data") as! String
                let dvc_token = res.value(forKey: "dvc_token") as! String
                UserDefaults.standard.set(dvc_token, forKey: "dvc_token")
                let scrapgoldarr = convertToDictionary(text: scrapgold)
                let wccArr = convertToDictionary(text: wwcdata)
                let colorValues = convertToDictionary(text: res.value(forKey: "color_data") as! String)
                 if let parentId = colorValues?["left_color"] as? NSArray {
                    UserDefaults.standard.set(parentId, forKey: "left_color")
                }
                if let parentId = colorValues?["right_color"] as? NSArray {
                    UserDefaults.standard.set(parentId, forKey: "right_color")
                }               
                let bgColorArr = colorValues?["bg_color"] as! NSArray
                UserDefaults.standard.set(bgColorArr, forKey: "bg_color")
                let arrscrap = (scrapgoldarr?["ScrapJson"] as? NSArray ?? []) as! [Bool]
                let wccjson = (wccArr?["wcc"] as? NSArray ?? []) as! [Bool]
                let email0 = res.value(forKey: "email") as! String
                UserDefaults.standard.set(res, forKey: "result")
                UserDefaults.standard.set(arrscrap, forKey: "scrapgold")
                UserDefaults.standard.set(wccjson, forKey: "wcc")
                UserDefaults.standard.set("\(email0)", forKey: "login")
                UserDefaults.standard.set("login", forKey: "userlogin")
                    self.activator.isHidden = true
                    self.activator.stopAnimating()
                    UserDefaults.standard.set(false, forKey: "CheckAnim")
                let next = self.storyboard?.instantiateViewController(withIdentifier: "CustomViewController") as! CustomViewController
              
                self.present(next, animated: true, completion: nil)
                }
            }else{
                DispatchQueue.main.async {
                self.activator.isHidden = true
                self.activator.stopAnimating()
                self.alartview(str: message)
                }
            }            
        })
      }
    }   
    override func viewWillDisappear(_ animated: Bool) {
        self.activator.isHidden = true
        self.activator.stopAnimating()
    }
    @IBAction func Back(_ sender: UIButton) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "CustomViewController") as! CustomViewController
        self.present(next, animated: true, completion: nil)
    }
    @IBAction func Remember_password(_ sender: UIButton) {
        if check_box.image == UIImage(named: "check_box.png"){
            check_box.image = UIImage(named: "empty_check_box.png")
            UserDefaults.standard.removeObject(forKey: "remember")
        }else{
            check_box.image = UIImage(named: "check_box.png")
            UserDefaults.standard.set("login", forKey: "remember")
        }
    }
    @IBAction func Forgot_password(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Forgot Password", message: "", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Submit", style: .default, handler: { alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            textField.keyboardType = .emailAddress
            if textField.text?.isEmpty == true {
                self.alartview(str: "Please enter Email")
            }else{
                let email_text = textField.text!
                self.forgotPassword(email:email_text)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Enter Email"
        })
        self.present(alertController, animated: true, completion: nil)
    }
    func forgotPassword(email:String){
        guard Reachability.isConnectedToNetwork() == true else{
                self.activator.stopAnimating()
                self.activator.isHidden = true
            alartviewShow(str: "No Internet Connection. Please try Again")
            return
        }
        postWithoutImage(url_string: "https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/forgotPassword", parameters: "email=\(email)", CompletionHandler: {json, error in
            let message = json?.value(forKey: "message") as! String
            if json?.value(forKey: "success") as! String == "true"{
                DispatchQueue.main.async {
                self.activator.isHidden = true
                self.activator.stopAnimating()
                self.alartview(str: message)
                }
            }else{
                DispatchQueue.main.async {
                self.activator.isHidden = true
                self.activator.stopAnimating()
                self.alartview(str: message)
                }
            }
        })
    } 
}
public class Reachability {
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                zeroSockAddress in SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)}
        } ) else {
            return false
        }
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) == false {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        return ret
    }
}
