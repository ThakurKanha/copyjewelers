//  comman.swift
//  swiftStlViewer
//  Created by user on 10/16/18.
//  Copyright © 2018 Rajat. All rights reserved.
import UIKit
import Foundation
extension UIViewController{
    
    func showViewLogIn()->UIView{
        
        let height = self.view.frame.height
        let width = self.view.frame.width
        let logInView = UIView()
        let signButton = UIButton()
        let aboutUsButton = UIButton()
        let stlButton = UIButton()
        let privcyButton = UIButton()
        let line = UILabel()
        let line2 = UILabel()
        let line3 = UILabel()
        var fontSize = CGFloat()
        if UI_USER_INTERFACE_IDIOM() == .pad{
            fontSize = 22
        }else{
            fontSize = 15
        }
            
        logInView.frame = CGRect(x: width-width*0.55, y: 35, width: width*0.55, height: height*0.25) //height*0.17
        logInView.backgroundColor = UIColor(displayP3Red: 254/255, green: 242/255, blue: 195/255, alpha: 1)
        logInView.layer.cornerRadius = 10
        logInView.layer.masksToBounds = true
        logInView.layer.borderWidth = 5
        logInView.layer.borderColor = UIColor(red: 151/255, green: 140/255, blue: 85/255, alpha: 1).cgColor
        self.view.addSubview(logInView)
        
        signButton.frame = CGRect(x: 15, y: 5, width: logInView.frame.width, height:logInView.frame.height/4.5)
        signButton.setTitleColor(UIColor.black, for: .normal)
        signButton.setTitle("Sign In", for: .normal)
        signButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        signButton.addTarget(self, action: #selector(signINAction), for: .touchUpInside)
        signButton.titleLabel?.font =  UIFont.systemFont(ofSize: fontSize)
        logInView.addSubview(signButton)
        
        line.frame = CGRect(x: 15, y: signButton.frame.maxY, width: logInView.frame.width-30, height: 4)
        line.backgroundColor = UIColor(red: 151/255, green: 140/255, blue: 85/255, alpha: 1)
        logInView.addSubview(line)
        
        aboutUsButton.frame = CGRect(x: 15, y: line.frame.maxY, width: logInView.frame.width, height:logInView.frame.height/4.5)
        aboutUsButton.setTitle("Services", for: .normal)
        aboutUsButton.setTitleColor(UIColor.black, for: .normal)
        aboutUsButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        aboutUsButton.titleLabel?.font =  UIFont.systemFont(ofSize: fontSize)
        aboutUsButton.addTarget(self, action: #selector(aboutAction), for: .touchUpInside)
        logInView.addSubview(aboutUsButton)
        
        line2.frame = CGRect(x: 15, y: aboutUsButton.frame.maxY, width: logInView.frame.width-30, height: 4)
        line2.backgroundColor = UIColor(red: 151/255, green: 140/255, blue: 85/255, alpha: 1)
        logInView.addSubview(line2)
        
        
        stlButton.frame = CGRect(x: 15, y: line2.frame.maxY, width: logInView.frame.width-30, height:logInView.frame.height/4.5)
        stlButton.setTitle("Subscription Terms of Use", for: .normal)
        stlButton.titleLabel?.adjustsFontSizeToFitWidth = true
        stlButton.setTitleColor(UIColor.black, for: .normal)
        stlButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        stlButton.titleLabel?.font =  UIFont.systemFont(ofSize: fontSize)
        stlButton.addTarget(self, action: #selector(stlDiscriptionAction), for: .touchUpInside)
        logInView.addSubview(stlButton)
        
        line3.frame = CGRect(x: 15, y: stlButton.frame.maxY, width: logInView.frame.width-30, height: 4)
        line3.backgroundColor = UIColor(red: 151/255, green: 140/255, blue: 85/255, alpha: 1)
        logInView.addSubview(line3)
        
        privcyButton.frame = CGRect(x: 15, y: line3.frame.maxY, width: logInView.frame.width-30, height:logInView.frame.height/4.5)
        privcyButton.setTitle("Privacy Policy", for: .normal)
        privcyButton.titleLabel?.adjustsFontSizeToFitWidth = true
        privcyButton.setTitleColor(UIColor.black, for: .normal)
        privcyButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        privcyButton.titleLabel?.font =  UIFont.systemFont(ofSize: fontSize)
        privcyButton.addTarget(self, action: #selector(PrivcyButtonAction), for: .touchUpInside)
        logInView.addSubview(privcyButton)
        
        return logInView
        
    }
    @objc func signINAction(sender:UIButton){
        print("clickSignin")
        sender.superview?.isHidden = true
        let next = self.storyboard?.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
        self.present(next, animated: true, completion: nil)
    }
    @objc func aboutAction(sender:UIButton){
        print("click aboutAction")
        sender.superview?.isHidden = true
        let next = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
        next.value = "1"
        self.present(next, animated: true, completion: nil)
    }
    @objc func stlDiscriptionAction(sender:UIButton){
        print("click stlDiscriptionAction")
        sender.superview?.isHidden = true
        let next = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
//        next.value = "1"
        self.present(next, animated: true, completion: nil)
    }
    @objc func PrivcyButtonAction(sender:UIButton){
        sender.superview?.isHidden = true
        let next = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
        next.value = "2"
        self.present(next, animated: true, completion: nil)
    }
    @objc func aboutUsAction(sender:UIButton){
        print("click Signin")
        sender.superview?.isHidden = true
        let next = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
        next.value = "1"
        self.present(next, animated: true, completion: nil)
    }
    
    func showViewLogInComplete()->UIView{
        let height = self.view.frame.height
        let width = self.view.frame.width
        let logInView = UIView()
        let lblemail = UILabel()
        let UserPrefrences = UIButton()
        let aboutUsButton = UIButton()
        let stlButton = UIButton()
        let PrivcyButton = UIButton()
        let subsciptoin = UIButton()
        let line0 = UILabel()
        let line = UILabel()
        let line2 = UILabel()
        let line3 = UILabel()
        let line4 = UILabel()
        var fontSize = CGFloat()
        if UI_USER_INTERFACE_IDIOM() == .pad{
            fontSize = 22
        }else{
            fontSize = 15
        }
        
        logInView.frame = CGRect(x: width-width*0.55, y: 35, width: width*0.55, height: height*0.35) //height*0.17
        logInView.backgroundColor = UIColor(displayP3Red: 254/255, green: 242/255, blue: 195/255, alpha: 1)
        logInView.layer.cornerRadius = 10
        logInView.layer.masksToBounds = true
        logInView.layer.borderWidth = 5
        logInView.layer.borderColor = UIColor(red: 151/255, green: 140/255, blue: 85/255, alpha: 1).cgColor
        self.view.addSubview(logInView)
        
        lblemail.frame = CGRect(x: 15, y: 5, width: logInView.frame.width, height:logInView.frame.height/6)
        lblemail.text = UserDefaults.standard.string(forKey: "login")
        lblemail.textColor = UIColor.red
        lblemail.textAlignment = .left
        lblemail.font =  UIFont.systemFont(ofSize: fontSize)
        logInView.addSubview(lblemail)
        
        line0.frame = CGRect(x: 15, y: lblemail.frame.maxY, width: logInView.frame.width-30, height: 4)
        line0.backgroundColor = UIColor(red: 151/255, green: 140/255, blue: 85/255, alpha: 1)
        logInView.addSubview(line0)
        
        aboutUsButton.frame = CGRect(x: 15, y: line0.frame.maxY, width: logInView.frame.width, height:logInView.frame.height/7)
        aboutUsButton.setTitleColor(UIColor.black, for: .normal)
        aboutUsButton.setTitle("Services", for: .normal)
        aboutUsButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        aboutUsButton.addTarget(self, action: #selector(aboutUsAction), for: .touchUpInside)
        aboutUsButton.titleLabel?.font =  UIFont.systemFont(ofSize: fontSize)
        logInView.addSubview(aboutUsButton)
        
        
        
        line.frame = CGRect(x: 15, y: aboutUsButton.frame.maxY, width: logInView.frame.width-30, height: 4)
        line.backgroundColor = UIColor(red: 151/255, green: 140/255, blue: 85/255, alpha: 1)
        logInView.addSubview(line)
        
        
        
        UserPrefrences.frame = CGRect(x: 15, y: line.frame.maxY, width: logInView.frame.width, height:logInView.frame.height/7)
        UserPrefrences.setTitle("User Prefrences", for: .normal)
        UserPrefrences.setTitleColor(UIColor.black, for: .normal)
        UserPrefrences.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        UserPrefrences.titleLabel?.font =  UIFont.systemFont(ofSize: fontSize)
        UserPrefrences.addTarget(self, action: #selector(userPrefrences), for: .touchUpInside)
        logInView.addSubview(UserPrefrences)
        
        line3.frame = CGRect(x: 15, y: UserPrefrences.frame.maxY, width: logInView.frame.width-30, height: 4)
        line3.backgroundColor = UIColor(red: 151/255, green: 140/255, blue: 85/255, alpha: 1)
        logInView.addSubview(line3)
        
        
        subsciptoin.frame = CGRect(x: 15, y: line3.frame.maxY, width: logInView.frame.width-30, height:logInView.frame.height/7)
        subsciptoin.setTitle("Subscription Terms of Use", for: .normal)
        subsciptoin.titleLabel?.adjustsFontSizeToFitWidth = true
        subsciptoin.setTitleColor(UIColor.black, for: .normal)
        subsciptoin.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        subsciptoin.titleLabel?.font =  UIFont.systemFont(ofSize: fontSize)
        subsciptoin.addTarget(self, action: #selector(stlDiscriptionAction), for: .touchUpInside)
        logInView.addSubview(subsciptoin)
        
        line4.frame = CGRect(x: 15, y: subsciptoin.frame.maxY, width: logInView.frame.width-30, height: 4)
        line4.backgroundColor = UIColor(red: 151/255, green: 140/255, blue: 85/255, alpha: 1)
        logInView.addSubview(line4)
        
        
        PrivcyButton.frame = CGRect(x: 15, y: line4.frame.maxY, width: logInView.frame.width-20, height:logInView.frame.height/7)
        PrivcyButton.setTitle("Privacy Policy", for: .normal)
        PrivcyButton.titleLabel?.adjustsFontSizeToFitWidth = true
        PrivcyButton.setTitleColor(UIColor.black, for: .normal)
        PrivcyButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        PrivcyButton.titleLabel?.font =  UIFont.systemFont(ofSize: fontSize)
        PrivcyButton.addTarget(self, action: #selector(PrivcyButtonAction), for: .touchUpInside)
        logInView.addSubview(PrivcyButton)
        
        line2.frame = CGRect(x: 15, y: PrivcyButton.frame.maxY, width: logInView.frame.width-30, height: 4)
        line2.backgroundColor = UIColor(red: 151/255, green: 140/255, blue: 85/255, alpha: 1)
        logInView.addSubview(line2)
        
        
        stlButton.frame = CGRect(x: 15, y: line2.frame.maxY, width: logInView.frame.width-20, height:logInView.frame.height/7)
        stlButton.setTitle("Log Out", for: .normal)
        stlButton.titleLabel?.adjustsFontSizeToFitWidth = true
        stlButton.setTitleColor(UIColor.black, for: .normal)
        stlButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        stlButton.titleLabel?.font =  UIFont.systemFont(ofSize: fontSize)
        stlButton.addTarget(self, action: #selector(logOut), for: .touchUpInside)
        logInView.addSubview(stlButton)
        
        
        return logInView
        
    }
  
    @objc func userPrefrences(sender:UIButton){
        print("click UserPrefrences")
        sender.superview?.isHidden = true
        let alert = UIAlertController(title: "", message: "User Preferences", preferredStyle: UIAlertController.Style.alert)
        if UserDefaults.standard.string(forKey: "userlogin") != nil {
            alert.addAction(UIAlertAction(title: "Change Password", style: UIAlertAction.Style.default, handler: { action in
                let next = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
                self.present(next, animated: true, completion: nil)
            }))
        }
        alert.addAction(UIAlertAction(title: "Scrap Metal Calculator Setting", style: UIAlertAction.Style.default, handler: { action in
            let next = self.storyboard?.instantiateViewController(withIdentifier: "ScrapsettingViewController") as! ScrapsettingViewController
            self.present(next, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Weight & Casting Cost Calculator Setting", style: UIAlertAction.Style.default, handler: { action in
            let next = self.storyboard?.instantiateViewController(withIdentifier: "weightcastingsettingViewController") as! weightcastingsettingViewController
            self.present(next, animated: true, completion: nil)
        }))
        //        alert.addAction(UIAlertAction(title: "Ledger Stone Setting", style: UIAlertAction.Style.default, handler: { action in
        //            let next = self.storyboard?.instantiateViewController(withIdentifier: "SettingStoneViewController") as! SettingStoneViewController
        //            self.present(next, animated: true, completion: nil)
        //        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
     
    }
    @objc func logOut(sender:UIButton){
        print("click logOut")
        sender.superview?.isHidden = true
        let alert = UIAlertController(title: "", message: "Do you want SignOut", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
            UserDefaults.standard.removeObject(forKey: "login")
            UserDefaults.standard.removeObject(forKey: "remember")
            UserDefaults.standard.removeObject(forKey: "result")
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
}

class comman: NSObject {
    var userid = String()
    var colorview = UIColor()
    var colorblue = UIColor()
    var colorwhite = UIColor()
    func userdefultSET(value:String,key:String) {
       UserDefaults.standard.setValue(value, forKey: key)
       UserDefaults.standard.synchronize()
    }
    func remove(key:String) {
     UserDefaults.standard.removeObject(forKey: key)
     UserDefaults.standard.synchronize()
    }
    func getVALUES(key:String) -> Any {
        let str = UserDefaults.standard.value(forKey: key) as Any
        UserDefaults.standard.synchronize()
        return str
    }
}


