//
//  ForgotPasswordViewController.swift
//  JewelersScale
//
//  Created by Virendra on 02/01/19.
//  Copyright © 2019 MacOS High. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    @IBOutlet weak var txt_oldpass: UITextField!
    @IBOutlet weak var txt_newpass: UITextField!
    @IBOutlet weak var txt_confirmpass: UITextField!
    @IBOutlet weak var activator: UIActivityIndicatorView!
    var common = comman()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activator.isHidden = true
        textfieldpedding(txtfield: txt_oldpass)
        textfieldpedding(txtfield: txt_newpass)
        textfieldpedding(txtfield: txt_confirmpass)
    }
    func alartview(str:String){
        let alert = UIAlertController(title: "", message: str, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func Back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func Submit(_ sender: UIButton) {
        if txt_oldpass.text == ""{
            alartview(str: "Enter Old Password")
        }else if txt_newpass.text == ""{
            alartview(str: "Enter New Password")
        }else if txt_newpass.text != txt_confirmpass.text{
            alartview(str: "Don't Match Password")
        }else{
            self.activator.isHidden = false
            self.activator.startAnimating()
            self.passwordchange()
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func passwordchange(){
        var userid = ""
        if let res = UserDefaults.standard.value(forKey: "result"){
            userid = (res as AnyObject).value(forKey: "user_id") as! String
        }
        guard Reachability.isConnectedToNetwork() == true else{
            let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                self.activator.stopAnimating()
                self.activator.isHidden = true
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
            return
        }
        postWithoutImage(url_string: "https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/changePassword", parameters: "user_id=\(userid)&old_password=\(txt_oldpass.text!)&new_password=\(txt_newpass.text!)", CompletionHandler: {json, error in
            let message = json?.value(forKey: "message") as! String
            if json?.value(forKey: "success") as! String == "true"{
                DispatchQueue.main.async {
                UserDefaults.standard.removeObject(forKey: "login")
                UserDefaults.standard.removeObject(forKey: "remember")
                UserDefaults.standard.removeObject(forKey: "result")
                self.activator.isHidden = true
                self.activator.stopAnimating()
                let next = self.storyboard?.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                self.present(next, animated: true, completion: nil)
                }
            }else{
                DispatchQueue.main.async {
                self.activator.isHidden = true
                self.activator.stopAnimating()
                self.alartview(str: message)
                }
            }
        })
    }
}
