//
//  RagistrationViewController.swift
//  JewelersScale
//
//  Created by Virendra on 29/12/18.
//  Copyright © 2018 MacOS High. All rights reserved.
//

import UIKit

class RagistrationViewController: UIViewController {

    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_confirmemail: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var activator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activator.isHidden = true
        textfieldpedding(txtfield: txt_email)
        textfieldpedding(txtfield: txt_confirmemail)
        textfieldpedding(txtfield: txt_password)
    }
    func alartview(str:String){
        let alert = UIAlertController(title: "", message: str, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func Submit(_ sender: UIButton) {
        let emailRegEx: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        if txt_email.text == ""{
            alartview(str: "Please enter Email Address")
            return
        }else if emailTest.evaluate(with: txt_email.text) != true {
            alartview(str: "Please enter valid email address")
            return
        }else if txt_email.text != txt_confirmemail.text{
            alartview(str: "Don't match email address")
            return
        }else if txt_password.text == "" {
            alartview(str: "Please enter Password")
            return
        }else{
            activator.isHidden = false
            activator.startAnimating()
            guard Reachability.isConnectedToNetwork() == true else{
                let alertController = UIAlertController(title: "", message: "No Internet Connection. Please try Again", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    self.activator.stopAnimating()
                    self.activator.isHidden = true
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                return
            }
            callWebService()
        }
    }
    func callWebService(){
        postWithoutImage(url_string: "https://adarshtated.com/dev/jeweller_app/api/index.php/api/V3/signUp", parameters: "email=\(txt_email.text!)&password=\(txt_password.text!)", CompletionHandler: { json, error in
            let message = json?.value(forKey: "message") as! String
            if json?.value(forKey: "success") as! String == "true"{
                DispatchQueue.main.async {
                let res = json?.value(forKey: "result") as! NSDictionary
                let email0 = res.value(forKey: "email") as! String
                UserDefaults.standard.set(res, forKey: "result")
                UserDefaults.standard.set("\(email0)", forKey: "login")
                self.activator.isHidden = true
                self.activator.stopAnimating()
                UserDefaults.standard.set(false, forKey: "CheckAnim")
                let next = self.storyboard?.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
                self.present(next, animated: true, completion: nil)
                }
            }else{
                DispatchQueue.main.async {
                    self.activator.isHidden = true
                    self.activator.stopAnimating()
                    self.alartview(str: message)
                }
            }
        })
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @IBAction func Back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
