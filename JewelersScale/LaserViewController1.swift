//
//  LaserViewController.swift
//  JewelersScale
//
//  Created by baps on 16/06/18.
//  Copyright © 2018 MacOS High. All rights reserved.
//

import UIKit
import MessageUI
class LaserValues: NSObject {
    var quntity = String("")
    var rate = String("1.00")
    
}
`
class JewelerCellPDF: UITableViewCell {
    
    @IBOutlet weak var lblPdf: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
}
class LaserCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var txtQuntity: UITextField!
    @IBOutlet weak var txtRate: UITextField!
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var txtTitle: UITextField!
    
    @IBOutlet weak var txtLine: UILabel!
    @IBOutlet weak var txtX: UILabel!
    
    @IBOutlet weak var txtEqual: UILabel!

    
    
    
    override func awakeFromNib() {
        super .awakeFromNib()
        txtQuntity.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        txtQuntity.layer.borderWidth = 1
        txtQuntity.layer.cornerRadius = 3
        
        txtRate.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
        txtRate.layer.borderWidth = 1
        txtRate.layer.cornerRadius = 3
        
//        cellView.layer.borderColor = UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1).cgColor
//        cellView.layer.borderWidth = 1
        
    }
}

class LaserViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate {
    var lasertitle = NSMutableArray()
    var laserArray = NSMutableArray()
    var lng1 = Int(10000)
    var lng2 = Int(10000)
    let colorSeparator =  UIColor(displayP3Red: 160/255.0, green: 111/255.0, blue: 1/255.0, alpha: 1)
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewList: UITableView!
    @IBOutlet weak var btnOpen: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnOpen1: UIButton!
    @IBOutlet weak var btnSave2: UIButton!
    
    @IBOutlet weak var btnReset: UIButton!
    
    
    @IBOutlet weak var txtGTotal: UILabel!
    @IBOutlet weak var lblGtotal: UILabel!
    
    
    @IBOutlet weak var btnCalc: UIButton!
    
    @IBOutlet weak var btnMail: UIButton!
    var txtGrandTotal = UILabel()
    var txtGrandLable = UILabel()
    var noOfPdf: [String]!
    var selPfd = Int(-1)
    var isMailSend = Bool(false)
    var tatalCasting = String("")
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true);
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lasertitle = ["CAD DESIGN(S)","WAX DESIGN(S)"
            ,"3D PRINT(S)","MOLD(S)","CASTING(S)","JEWELER","STONE(S)","SETTER","POLISHING","FINISHING","ENGRAVING","MISC",""];
        for i in 0..<lasertitle.count{
            let valse = NSMutableArray()
            if i < lasertitle.count - 1{
                let laserVal = LaserValues()
                if i == 4{
                    laserVal.quntity = tatalCasting
                }
                valse.add(laserVal)
            }
            laserArray.add(valse)
        }
        txtGrandTotal.text = "$0.00"
//        noOfPdf = listFilesFromDocumentsFolder()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        tap.delegate = self
        // Do any additional setup after loading the view.
        tableView.separatorStyle = .none
       // save()
        let no = UserDefaults.standard.integer(forKey: "listsj")
        print(no)
        noOfPdf = [String]()
        for i in 0..<no{
            let str = UserDefaults.standard.object(forKey: "todaysDate\(i)") as! String
            print(str)
            noOfPdf.append(str)
            //noOfPdf.append("11")
        }
        tableViewList.separatorColor = colorSeparator;
        tableViewList.isHidden = true
        
        setGradientBackground(btnSave)
        setGradientBackground(btnReset)
        setGradientBackground(btnOpen)
        setGradientBackground(btnOpen1)
        setGradientBackground(btnSave2)
       selPfd = -1
    }
    func setGradientBackground(_ uibtton:UIButton) {
        let colorTop =  UIColor(red: 230.0/255.0, green: 180.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        let colorMidile = UIColor(red: 255.0/255.0, green: 229.0/255.0, blue: 106.0/255.0, alpha: 1.0).cgColor
        let colorBottom =  UIColor(red: 230.0/255.0, green: 180.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorMidile,colorBottom]
        gradientLayer.locations = [0.0,0.5, 1.0]
        gradientLayer.frame = uibtton.bounds
        
        uibtton.layer.insertSublayer(gradientLayer, at: 0)
        uibtton.layer.borderWidth = 1
        uibtton.layer.borderColor =  UIColor(red: 200.0/255.0, green: 150.0/255.0, blue: 1.0/255.0, alpha: 1.0).cgColor
    }
    @IBAction func Back_action(_ sender: Any) {
        if(self.tableViewList.isHidden == false){
            btnSave2.isHidden = false
            btnOpen1.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.tableViewList.alpha = 0
            }, completion:  {
                (value: Bool) in
                self.tableViewList.isHidden = true
            })
//            self.tableViewList.isHidden = true
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func Remove(_ removeIndex:Int)  {
        let nstit = NSMutableArray()
        let nsval = NSMutableArray()
        let nsName = NSMutableArray()
        var noAll = UserDefaults.standard.integer(forKey: "listsj")
        for no in 0..<noAll{
            let Names = UserDefaults.standard.object(forKey: "todaysDate\(no)") as! String
            let valtit = UserDefaults.standard.object(forKey: "lasertitle\(no)") as! NSArray
            let objArr = NSMutableArray()
            for i in 0..<valtit.count{
                let valse = NSMutableArray()
                let noobj = UserDefaults.standard.integer(forKey: "\(no)_\(i)noobj")
                print(noobj)
                for j in 0..<noobj{
                    let lsrow  = LaserValues()
                    lsrow.quntity = UserDefaults.standard.object(forKey: "\(no)_\(i)q\(j)") as! String
                    lsrow.rate = UserDefaults.standard.object(forKey: "\(no)_\(i)r\(j)") as! String
                    valse.add(lsrow)
                }
                objArr.add(valse)
            }
            nstit.add(valtit)
            nsval.add(objArr)
            nsName.add(Names)
        }
        nstit.removeObject(at: removeIndex)
        nsval.removeObject(at: removeIndex)
        nsName.removeObject(at: removeIndex)
        noAll = noAll - 1
        
        for no in 0..<noAll{
            UserDefaults.standard.set(nsName.object(at: no) as! String, forKey: "todaysDate\(no)")
            let valtit = nstit.object(at: no) as! NSArray
            UserDefaults.standard.set(valtit, forKey: "lasertitle\(no)")
            let valval = nsval.object(at: no) as! NSMutableArray
            for i in 0..<valtit.count{
                let valse = valval.object(at: i) as! NSMutableArray
                UserDefaults.standard.set(valse.count, forKey: "\(no)_\(i)noobj")
                for j in 0..<valse.count{
                    let lsrow  = valse.object(at: j) as! LaserValues
                    UserDefaults.standard.set(lsrow.quntity, forKey: "\(no)_\(i)q\(j)")
                    UserDefaults.standard.set(lsrow.rate, forKey: "\(no)_\(i)r\(j)")
                }
            }
        }
        UserDefaults.standard.set(noAll, forKey: "listsj")
        UserDefaults.standard.synchronize()
        //onClickOpen(NSNull.self)
    }
    
    func save(_ todaysDate:String) {
//        if let appDomain = Bundle.main.bundleIdentifier {
//            UserDefaults.standard.removePersistentDomain(forName: appDomain)
//        }
        var no = UserDefaults.standard.integer(forKey: "listsj")
        //let date : Date = Date()
        //let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss"
        //let todaysDate = dateFormatter.string(from: date)
        UserDefaults.standard.set(todaysDate, forKey: "todaysDate\(no)")
        UserDefaults.standard.set(lasertitle, forKey: "lasertitle\(no)")
        for i in 0..<laserArray.count{
            let valse = laserArray.object(at: i) as! NSMutableArray
            UserDefaults.standard.set(valse.count, forKey: "\(no)_\(i)noobj")
            for j in 0..<valse.count{
                let lsrow  = valse.object(at: j) as! LaserValues
                UserDefaults.standard.set(lsrow.quntity, forKey: "\(no)_\(i)q\(j)")
                UserDefaults.standard.set(lsrow.rate, forKey: "\(no)_\(i)r\(j)")
            }
        }
        selPfd = no
        no = no + 1
        UserDefaults.standard.set(no, forKey: "listsj")
        print("\(no)~~~~~~~~~~~~~~")
        UserDefaults.standard.synchronize()
    }
    
    func Open(_ no: Int) {
        let valtit = UserDefaults.standard.object(forKey: "lasertitle\(no)") as! NSArray
        lasertitle.removeAllObjects()
        for val in valtit{
            lasertitle.add(val as! String)
        }
        print(lasertitle)
        laserArray.removeAllObjects()
        for i in 0..<lasertitle.count{
            let valse = NSMutableArray()
            let noobj = UserDefaults.standard.integer(forKey: "\(no)_\(i)noobj")
            for j in 0..<noobj{
                let lsrow  = LaserValues()
                lsrow.quntity = UserDefaults.standard.object(forKey: "\(no)_\(i)q\(j)") as! String
                lsrow.rate = UserDefaults.standard.object(forKey: "\(no)_\(i)r\(j)") as! String
                valse.add(lsrow)
            }
            laserArray.add(valse)
        }
        print(no)
    }
   
    func dismissKeyboard() {
        self.view.endEditing(true);
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 1))
        view.backgroundColor = colorSeparator
        
       
        
        if(section == lasertitle.count - 1){
            var rect = txtGTotal.frame
            print(rect)
            rect.origin.y = 10
            txtGrandTotal.frame = rect
            txtGrandTotal.text = GrandTotal()
            txtGrandTotal.textAlignment = .right
            txtGrandTotal.font = txtGTotal.font
            txtGTotal.text = txtGrandTotal.text
            rect.size.width = lblGtotal.frame.size.width * 1.4
            rect.origin.x -= rect.size.width + 5
            txtGrandLable.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
            if UI_USER_INTERFACE_IDIOM() == .pad{
                rect.origin.x -= 5
                txtGrandLable.font = txtGTotal.font
            }
            print(rect)
            rect.origin.y = 10
            txtGrandLable.frame = rect
            txtGrandLable.text = "Grand Total  = "
            txtGrandLable.textAlignment = .right
            
            //txtGrandLable.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
           
           // resetbtn.addTarget(self, action: Selector(("pressed")), for: .touchUpInside)
            let viewchilde = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 1))
            view.backgroundColor = UIColor.clear
             viewchilde.backgroundColor = colorSeparator
            view.addSubview(viewchilde)
            view.addSubview(txtGrandLable)
            view.addSubview(txtGrandTotal)
        }
        return view
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableViewList == tableView{
            return 0
        }else{
            if(section == lasertitle.count - 1 ){
                return 100
            }
            return 1
        }
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableViewList == tableView{
            return 1
        }else{
            return lasertitle.count
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewList == tableView{
            return noOfPdf.count
        }else{
            let valse = laserArray.object(at: section) as! NSMutableArray
            if(valse.count <= 1){
                return valse.count
            }
            return valse.count+1
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableViewList == tableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "JewelerCellPDF") as! JewelerCellPDF
//            cell.lblPdf.text = noOfPdf[indexPath.row]
            cell.lblPdf.text = (noOfPdf[indexPath.row].replacingOccurrences(of: "#", with: " "))
            cell.btnRemove.addTarget(self, action: #selector(OnClickRemove(_:)), for: .touchUpInside)
            cell.btnRemove.tag = indexPath.row
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LaserCell") as! LaserCell
            cell.btnPlus.addTarget(self, action: #selector(OnClickPlus), for: .touchUpInside)
            cell.btnPlus.tag = indexPath.section*lng1 + indexPath.row
            if(indexPath.row == 0){
//                cell.btnPlus.setTitle("+", for: UIControlState.normal)
                cell.btnPlus.setImage(UIImage(named: "plus.png"), for: .normal)
            }else{
//                cell.btnPlus.setTitle("-", for: UIControlState.normal)
                cell.btnPlus.setImage(UIImage(named: "multiply.png"), for: .normal)
            }
            
            //cell.btnPlus.isHidden = (indexPath.row != 0)
            
            let valse = laserArray.object(at: indexPath.section) as! NSMutableArray
            
            
            cell.txtQuntity.isHidden = indexPath.row == valse.count
            cell.txtRate.isHidden = indexPath.row == valse.count
            cell.txtX.isHidden = indexPath.row == valse.count
            cell.btnPlus.isHidden = indexPath.row == valse.count
            cell.txtLine.isHidden = indexPath.row < valse.count
            cell.txtEqual.isHidden = indexPath.row == valse.count
            cell.txtTitle.text = indexPath.row == 0 ? lasertitle[indexPath.section] as? String : ""
            if indexPath.row < valse.count {
                let lsrow  = valse.object(at: indexPath.row) as! LaserValues
                cell.txtQuntity.text = lsrow.quntity
                cell.txtQuntity.delegate = self
                cell.txtQuntity.tag = indexPath.section*lng1+(lng1*lng2) + indexPath.row
                
                cell.txtRate.text = lsrow.rate
                cell.txtRate.delegate = self
                cell.txtRate.tag = indexPath.section*lng1+(lng1*2*lng2) + indexPath.row
                
                cell.lblResult.text =  valuecalculate(lsrow.quntity, rate: lsrow.rate)
                
                cell.txtTitle.tag = -1
                if indexPath.section > 10 && indexPath.row == 0 {
                    cell.txtTitle.tag = -indexPath.section
                }
                cell.txtTitle.delegate = self
                
            }else{
                var grandtotal = Double(0.00)
                for val in valse {
                    let lsrow = val  as! LaserValues
                    let new = (lsrow.quntity as NSString).doubleValue
                    let new2 = (lsrow.rate as NSString).doubleValue
                    grandtotal = grandtotal + (new * new2)
                }
                cell.lblResult.text = clean("\(grandtotal)")
                
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if tableViewList == tableView{
            Open(indexPath.row)
            selPfd = indexPath.row
            tableViewList.isHidden = true
            btnOpen1.isHidden = false
            btnSave2.isHidden = false
            let myTimer = Timer(timeInterval: 0.01, target: self, selector: #selector(self.timerCallBack(_:)), userInfo: nil, repeats: false)
            RunLoop.current.add(myTimer, forMode: RunLoopMode.commonModes)
        }
        print("did")
    }
    func GrandTotal() -> String {
        var grandtotal = Double(0.00)
        for laser in laserArray{
            let valse = laser as! NSMutableArray
            for val in valse {
                let lsrow = val  as! LaserValues
                let new = (lsrow.quntity as NSString).doubleValue
                let new2 = (lsrow.rate as NSString).doubleValue
                grandtotal = grandtotal + (new * new2)
            }
        }
        return clean("\(grandtotal)")
    }
    @IBAction func OnClickPlus(_ sender: UIButton) {
        print("\(sender.tag) ~ \(sender.tag/lng1) ~ \(sender.tag%lng1)")
        let valse = laserArray.object(at: (sender.tag/lng1)) as! NSMutableArray
        if((sender.tag%lng1) == 0){
            let onemore = LaserValues()
            onemore.quntity = ""
            onemore.rate = "1.00"
            valse.add(onemore)
        }else{
            valse.removeObject(at: sender.tag%lng1)
        }
        tableView.reloadData()
    }
    
    
    @IBAction func OnClickRemove(_ sender: UIButton) {
        let alertController = UIAlertController(title: "", message: "Do You Want To Delete ?", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction) in
            print("You've pressed default");
            self.Remove(sender.tag)
            self.onClickOpen(sender)
        }
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
            print("You've pressed cancel");
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == -1 {
            return false
        }
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.tag == -(lasertitle.count - 2) {
            lasertitle.insert("MISC", at: lasertitle.count - 1)
            
            let valse = NSMutableArray()
            valse.add(LaserValues())
            laserArray.insert(valse, at: laserArray.count - 1)
            
            let myTimer = Timer(timeInterval: 0.5, target: self, selector: #selector(self.timerCallBack(_:)), userInfo: nil, repeats: false)
            RunLoop.current.add(myTimer, forMode: RunLoopMode.commonModes)
            
            //let range = NSMakeRange(0, 1);
            //tableView.reloadSections(IndexSet(integersIn: 0...1), with: UITableViewRowAnimation.top)
            //tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .automatic)
        }
        return true
    }
    
    // function which is triggered when handleTap is called
    @objc func timerCallBack(_ timer: Timer) {
        tableView.reloadData()
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag < -1 {
            
            let val = textField.tag * (-1)
            var str = lasertitle.object(at: val) as! String
            print(str)
            if let text = textField.text as NSString? {
                str  = text.replacingCharacters(in: range, with: string)
                print(lasertitle.count)
                lasertitle.removeObject(at: val)
                lasertitle.insert(str, at: val)
            }
            return true
        }
        let val = textField.tag/(lng1*lng2)
        let tag = textField.tag%(lng1*lng2)
        let valse = laserArray.object(at: tag/lng1) as! NSMutableArray
        let lsrow  = valse.object(at: tag%lng1) as! LaserValues
        if val == 1 {
            if let text = textField.text as NSString? {
                lsrow.quntity  = text.replacingCharacters(in: range, with: string)
            }
        }
        if val == 2 {
            if let text = textField.text as NSString? {
                lsrow.rate  = text.replacingCharacters(in: range, with: string)
            }
        }
        let indexPath = NSIndexPath(row: tag%lng1, section: tag/lng1)
        
        print("\(indexPath) [lng = \(lng1)] [val = \(val)] [textField = \(textField.tag)] [tag = \(tag)]")
        
        let lc =  tableView.cellForRow(at: indexPath as IndexPath) as! LaserCell
        lc.lblResult.text = valuecalculate(lsrow.quntity, rate: lsrow.rate)
        
        
        if valse.count > 1{
            let indexPath0 = NSIndexPath(row: valse.count, section: tag/lng1)
            var grandtotal = Double(0.00)
            for val in valse {
                let lsrow = val  as! LaserValues
                let new = (lsrow.quntity as NSString).doubleValue
                let new2 = (lsrow.rate as NSString).doubleValue
                grandtotal = grandtotal + (new * new2)
            }
            let lc0 =  tableView.cellForRow(at: indexPath0 as IndexPath) as! LaserCell
            lc0.lblResult.text = clean("\(grandtotal)")
        }
        txtGrandTotal.text = GrandTotal()
        txtGTotal.text = txtGrandTotal.text
        return true
    }
    
   
    
    func valuecalculate(_ qnt:String, rate rt:String) -> String{
        let new = (qnt as NSString).doubleValue
        let new2 = (rt as NSString).doubleValue
        return clean("\(new * new2)")
    }
    
    func clean(_ value: String?) -> String {
        guard value != nil else { return "$0.00" }
        let doubleValue = Double(value!) ?? 0.0
        let formatter = NumberFormatter()
        formatter.currencyCode = "USD"
        formatter.currencySymbol = "$"
        
        formatter.minimumFractionDigits = (value!.contains(".00")) ? 0 : 2
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .decimal
        let str = formatter.string(from: NSNumber(value: doubleValue)) ?? "\(doubleValue)"
        return "$"+str
    }
    
    @objc func timerCallBackMail(_ timer: Timer) {
      
        pdfDataWithTableView(tableView: tableView)
    }
    func pdfDataWithTableView(tableView: UITableView) {
        
        
        let priorBounds = tableView.bounds
        let fittedSize = tableView.sizeThatFits(CGSize(width:priorBounds.size.width, height:tableView.contentSize.height))
        tableView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:tableView.frame.width, height:tableView.contentSize.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        var pageOriginY: CGFloat = 0
        UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
        while pageOriginY < fittedSize.height {
            
            UIGraphicsGetCurrentContext()!.saveGState()
            UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
            tableView.layer.render(in: UIGraphicsGetCurrentContext()!)
            pageOriginY += pdfPageBounds.size.height
        }
        
        UIGraphicsGetCurrentContext()!.restoreGState()
        
        UIGraphicsEndPDFContext()
        tableView.bounds = priorBounds
        var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        docURL = docURL.appendingPathComponent("myDocument.pdf")
        pdfData.write(to: docURL as URL, atomically: true)
        
        
       
        let messageBody = "Attached is the Jewelers' Scale App Ledger screen "
        //let messageBody = "Title-" + type + " \nInstall-" + lbl_installation.text!
        let toRecipents = [""]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        
        mc.mailComposeDelegate = self
        mc.setSubject("Jewelers' Scale App Ledger Screen")
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        
        mc.addAttachmentData(pdfData as Data, mimeType: "application/pdf", fileName:  "fileName.pdf")
        self.present(mc, animated: true, completion: nil)
        
        
        print(listFilesFromDocumentsFolder() as Any);
        
    }
    
    func createPdfFromView(aView: UIView, saveToDocumentsWithFileName fileName: String) {
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, aView.bounds, nil)
        UIGraphicsBeginPDFPage()
        
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return }
        
        aView.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        
        if let documentDirectories = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            let documentsFileName = documentDirectories + "/" + fileName
            debugPrint(documentsFileName)
            pdfData.write(toFile: documentsFileName, atomically: true)
        }
        
        
        
        
        
        
        
        
        let messageBody = "Jewelers'Scale App Leger Screen"
        //let messageBody = "Title-" + type + " \nInstall-" + lbl_installation.text!
        let toRecipents = [""]
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        
        mc.mailComposeDelegate = self
        mc.setSubject("Jewelers'Scale App Leger Screen")
        mc.setMessageBody(messageBody, isHTML: false)
        mc.setToRecipients(toRecipents)
        
        mc.addAttachmentData(pdfData as Data, mimeType: "application/pdf", fileName:  fileName)
        self.present(mc, animated: true, completion: nil)
        
        
        print(listFilesFromDocumentsFolder() as Any);
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        print("default~~~~~~~~~~~~~~~mailComposeController~~~~~~~~~~~~~~~~~~~~")
        switch result {
        case MFMailComposeResult.cancelled:
            print("Mail cancelled")
            break
        case MFMailComposeResult.saved:
            print("Mail saved")
            break
        case MFMailComposeResult.sent:
            print("Mail sent")
            DispatchQueue.main.async(execute: {
                //let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                //let newViewController = storyBoard.instantiateViewController(withIdentifier: "Web_Controller") as! Web_Controller
                //self.present(newViewController, animated: true, completion: nil)
                print("Mail sent")
            })
            break
        case MFMailComposeResult.failed:
            print("Mail sent failure: \(String(describing: error?.localizedDescription))")
            break
            //        default:
            //            print("default")
            //            break
        }
        
        controller.dismiss(animated: true)
    }
    func listFilesFromDocumentsFolder() -> [String]?
    {
        let fileMngr = FileManager.default;
        
        // Full path to documents directory
        let docs = fileMngr.urls(for: .documentDirectory, in: .userDomainMask)[0].path
        
        // List all contents of directory and return as [String] OR nil if failed
        return try? fileMngr.contentsOfDirectory(atPath:docs)
    }
    
    @IBAction func OnClick_Save(_ sender: UIButton) {
        if(sender.tag == 0 && selPfd > -1){
            let str = UserDefaults.standard.object(forKey: "todaysDate\(selPfd)") as! String
            //Remove(selPfd)
            //save(str)
            Alert(str)
        }else{
            Alert()
        }
        
    }
    
    func Alert(){
        let alertController = UIAlertController(title: "Enter File Name", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter File Name"
        }
        let saveAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            print(firstTextField)
            let text = alertController.textFields!.first!.text!
            let isAvailable = self.checkName(text)
            if !text.isEmpty{
                
                let date : Date = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss"
                let todaysDate = dateFormatter.string(from: date)
                
                self.save(firstTextField.text!+"#"+todaysDate)
               /* if isAvailable{
                    let alertController1 = UIAlertController(title: "Name already exist", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    let cancelAction1 = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                        (action : UIAlertAction!) -> Void in
                        self.Alert()
                    })
                    alertController1.addAction(cancelAction1)
                    self.present(alertController1, animated: true, completion: nil)
                }*/
            }else{
                let alertController1 = UIAlertController(title: "Please enter name", message: "", preferredStyle: UIAlertControllerStyle.alert)
                let cancelAction1 = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                    (action : UIAlertAction!) -> Void in
                    self.Alert()
                })
                alertController1.addAction(cancelAction1)
                self.present(alertController1, animated: true, completion: nil)
            }
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
  
    func Alert(_ str :String){
        let split = str.split(separator: "#").map(String.init)
        let alertController = UIAlertController(title: "Enter Name", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Name"
            
            textField.text = split[0]
        }
        let saveAsAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.default, handler: { alert -> Void in
            //let firstTextField = alertController.textFields![0] as UITextField
            //print(firstTextField)
            let text = alertController.textFields!.first!.text!
            let isAvailable = self.checkName(str)
            print("\(isAvailable)   ~~~~~~\(str)")
            if !text.isEmpty {
                let date : Date = Date()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss"
                let todaysDate = dateFormatter.string(from: date)
                
                if isAvailable && text == split[0]{
                    let alertController1 = UIAlertController(title: "Do you want to overwrite", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    let yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {
                        (action : UIAlertAction!) -> Void in
                        self.Remove(self.selPfd)
                        self.save(text+"#"+todaysDate)
                        //self.save(str)
                    })
                    alertController1.addAction(yesAction)
                    let cancelAction = UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: {
                        (action : UIAlertAction!) -> Void in
                        
                    })
                    alertController1.addAction(cancelAction)
                    self.present(alertController1, animated: true, completion: nil)
                }else{
                    self.save(text+"#"+todaysDate)
                }
            }else{
                let alertController1 = UIAlertController(title: "Please enter name", message: "", preferredStyle: UIAlertControllerStyle.alert)
                let cancelAction1 = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
                    (action : UIAlertAction!) -> Void in
                    self.Alert(text)
                })
                alertController1.addAction(cancelAction1)
                self.present(alertController1, animated: true, completion: nil)
            }
            
        })
        let saveAction = UIAlertAction(title: "Save_", style: UIAlertActionStyle.default, handler: { alert -> Void in
            
            self.Remove(self.selPfd)
            self.save(str)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        
        
        
        alertController.addAction(saveAsAction)
       // alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    func checkName(_ str : String) -> Bool {
        let no = UserDefaults.standard.integer(forKey: "listsj")
        for i in 0..<no{
            let name = UserDefaults.standard.object(forKey: "todaysDate\(i)") as! String
            if str == name {
                return true
            }
        }
        return false
    }
    
    
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: tableViewList))!{
            return false
        }
        return true
    }
    
    
    @IBAction func onClickCalc(_ sender: UIButton) {
        print("Secod view controller action CalculatorButton1 ")
        let myVC = storyboard?.instantiateViewController(withIdentifier: "ViewController12") as! ViewController12
        self.present(myVC,animated: true,completion:nil)
    }
    @IBAction func onClickEmail(_ sender: UIButton) {
        //createPdfFromView(aView: self.tableView, saveToDocumentsWithFileName: "Jewelers.pdf")
        //pdfDataWithTableView(tableView: self.tableView)
        
        tableView.setContentOffset(CGPoint.zero, animated:false)
        let myTimer = Timer(timeInterval: 0.1, target: self, selector: #selector(self.timerCallBackMail(_:)), userInfo: nil, repeats: false)
        RunLoop.current.add(myTimer, forMode: RunLoopMode.commonModes)
    }
    @IBAction func onClickOpen(_ sender: Any) {
        btnOpen1.isHidden = true
        btnSave2.isHidden = true
        let no = UserDefaults.standard.integer(forKey: "listsj")
        print(no)
        noOfPdf.removeAll()
        for i in 0..<no{
            let str = UserDefaults.standard.object(forKey: "todaysDate\(i)") as! String
            print(str)
            noOfPdf.append(str)
            //noOfPdf.append("11")
        }
        self.tableViewList.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.tableViewList.alpha = 1
        }, completion:  nil)
//        tableViewList.isHidden = false
        tableViewList.reloadData()
    }
  
    @IBAction func OnClickReset(_ sender: UIButton) {
        lasertitle.removeAllObjects()
        laserArray.removeAllObjects()
        lasertitle = ["CAD DESIGN(S)","WAX DESIGN(S)"
            ,"3D PRINT(S)","MOLD(S)","CASTING(S)","JEWELER","STONE(S)","SETTER","POLISHING","FINISHING","ENGRAVING","MISC",""];
        for i in 0..<lasertitle.count{
            let valse = NSMutableArray()
            if i < lasertitle.count - 1{
                let laserVal = LaserValues()
                if i == 4{
                    laserVal.quntity = tatalCasting
                }
                valse.add(laserVal)
            }
            laserArray.add(valse)
        }
        txtGrandTotal.text = "$0.00"
        tableView.reloadData()
        //
    }
    
    
}

